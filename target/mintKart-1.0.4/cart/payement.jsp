<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="com.ecommerce.data.CookieUtil,com.ecommerce.business.User,com.ecommerce.data.UserDB,com.ecommerce.data.MenuUtil,java.util.ArrayList,java.util.Iterator"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Fashion Hub</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Fashion Club Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- css -->
		<link href="${pageContext.request.contextPath}/css/orderProcessing.css"
			rel="stylesheet" type="text/css" media="all" />
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
			media="all" />
		<link rel="stylesheet" href="css/style.css" type="text/css"
			media="all" />
		<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"
			media="all" /> <!--// css --> <!-- font -->
		<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro"
			rel="stylesheet">
		<link
			href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
			rel='stylesheet' type='text/css'> <!-- //font --> <script
			src="js/jquery-1.11.1.min.js"></script> <script src="js/bootstrap.js"></script>
</head>

<body>
	<div class="header-top-w3layouts">
		<div class="container">
			<div class="col-md-6 logo-w3">
				<a href="index.jsp"><img src="images/Util/logo1.jpg" alt=" " />
					<h1>
						MINT<span>KART</span>
					</h1></a>
			</div>
		</div>
	</div>

<br />
		<!-- another version - flat style with animated hover effect -->
		<div class="breadcrumb flat">
			<a>LOGIN</a> <a>SHIPPING</a> <a class="active">PAYEMENT</a> <a>REVIEW</a>
		</div>


		<div class="payementOption">
			<h4 style="float: left; color: black">PAYMENT INFORMATION</h4>
			<br /> <br />
			<form method="post" action="finalizeOrder">
			<ul class="radio-group">
				<li><input id="choice-a" type="radio" name="payementMethod"  value="CashOndelivery" checked="checked"/> <label
					for='choice-a'> <span><span></span></span> Cash On delivery
				</label></li>
				<li><input id="choice-b" type="radio" name="payementMethod" value="PaytmWallet" /> <label
					for='choice-b'> <span><span></span></span> Paytm Wallet
				</label></li>
				<li><input id="choice-d" type="radio" name="payementMethod" value="payumoney" /> <label
					for='choice-d'> <span><span></span></span> Debit/Credit/Net
						Banking
				</label></li>
			</ul>
			<div>
				 <input class="btn btn-large btn-primary"
					type="submit" name="submit" value="Continue" />
			</div>
			</form>
		</div>