<%-- <%@page import="com.ecommerce.data.OrderDB"%>
<%@page import="com.ecommerce.business.Order"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ecommerce.data.ProductDB"%>
<%@page import="com.ecommerce.data.UserDB,java.text.SimpleDateFormat;  "%> --%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ecommerce.business.Order"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ecommerce.data.OrderDB"%>
<%@page import="com.ecommerce.data.UserDB"%>
<jsp:include page="/admin/include/header.jsp" />
<div id="content" class="col-lg-10 col-sm-10">
	<!-- content starts -->
	<div>
		<ul class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="index.jsp">Dashboard</a></li>
		</ul>
	</div>
	<div class=" row">
		<div class="col-md-3 col-sm-3 col-xs-6">
			<a data-toggle="tooltip" title="6 new members."
				class="well top-block" href="#"> <i
				class="glyphicon glyphicon-user blue"></i>

				<div>Registered User</div> <%
 	int registeredUser = UserDB.getRegisteredUser();
 %>
				<div>507</div>
			</a>
		</div>

		<div class="col-md-3 col-sm-3 col-xs-6">
			<a data-toggle="tooltip" title="4 new pro members."
				class="well top-block" href="#"> <i
				class="glyphicon glyphicon-star green"></i>

				<div>Guest User</div>
				<div>228</div>
			</a>
		</div>

		<div class="col-md-3 col-sm-3 col-xs-6">
			<a data-toggle="tooltip" title="$34 new sales."
				class="well top-block" href="#"> <i
				class="glyphicon glyphicon-shopping-cart yellow"></i>

				<div>Total Revenue</div>
				<div>$13320</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-user"></i> Pending Orders
					</h2>


				</div>
				<div class="box-content">
					<table
						class="table table-striped table-bordered bootstrap-datatable datatable responsive">
						<thead>
							<tr>
								<th>OrderId</th>
								<th>Order Date</th>
								<th>Payment Method</th>
								<th>Order Status</th>
								<th>Tax</th>
								<th>Shipping Charges</th>
								<th>Product Charges</th>
								<th>Total Cost</th>
								<th>Actions</th>

							</tr>
						</thead>
						<tbody>
							<%
								ArrayList<Order> orderList = OrderDB.getPendingOrder();
								for (int i = 0; i < orderList.size(); i++) {
									Order order = orderList.get(i);
							%>
							<tr>
								<td><a href="viewOrderDetails?orderId=<%=order.getOrderId()%>"><%=order.getOrderId()%></a></td>
								<%
									SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
								%>
								<td class="center"><%=formatter.format(order.getOrderDate())%></td>
								<td class="center"><%=order.getPayementMethod()%></td>
								<td class="center"><span
									class="label-success label label-default"><%=order.getOderStatus()%></span></td>
								<td class="center"><%=order.getTax()%></td>
								<td class="center"><%=order.getShippementCharges()%></td>
								<td class="center"><%=order.getProductCost()%></td>
								<td class="center"><%=order.getTotalCost()%></td>
								<td class="center"><a class="btn btn-success"
									href="viewOrderDetails?orderId=<%=order.getOrderId()%>"> <i
										class="glyphicon glyphicon-zoom-in icon-white"></i> View Order
								</a> <a class="btn btn-info" href="#"> <i
										class="glyphicon glyphicon-edit icon-white"></i> Ship
								</a></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!--/span-->

	</div>
	<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-user"></i> Shipped Orders
					</h2>


				</div>
				<div class="box-content">
					<table
						class="table table-striped table-bordered bootstrap-datatable datatable responsive">
						<thead>
							<tr>
								<th>OrderId</th>
								<th>Order Date</th>
								<th>Payment Method</th>
								<th>Order Status</th>
								<th>Tax</th>
								<th>Shipping Charges</th>
								<th>Product Charges</th>
								<th>Total Cost</th>
								<th>Tracking Id</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList<Order> shippedOrderList = OrderDB.getShippedOrder();
								for (int i = 0; i < shippedOrderList.size(); i++) {
									Order order = shippedOrderList.get(i);
							%>
							<tr>
								<td><a href="viewOrderDetails?orderId=<%=order.getOrderId()%>"><%=order.getOrderId()%></a></td>
								<%
									SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
								%>
								<td class="center"><%=formatter.format(order.getOrderDate())%></td>
								<td class="center"><%=order.getPayementMethod()%></td>
								<td class="center"><span
									class="label-success label label-default"><%=order.getOderStatus()%></span></td>
								<td class="center"><%=order.getTax()%></td>
								<td class="center"><%=order.getShippementCharges()%></td>
								<td class="center"><%=order.getProductCost()%></td>
								<td class="center"><%=order.getTotalCost()%></td>
								<td class="center"><%=order.getTrackingId()%></td>
								<td class="center"><a class="btn btn-success"
									href="viewOrderDetails?orderId=<%=order.getOrderId()%>"> <i
										class="glyphicon glyphicon-zoom-in icon-white"></i> View Order
								</a> <a class="btn btn-info" href="#"> <i
										class="glyphicon glyphicon-edit icon-white"></i> Ship
								</a></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!--/span-->

	</div>
	<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-user"></i> Delivered Orders
					</h2>


				</div>
				<div class="box-content">
					<table
						class="table table-striped table-bordered bootstrap-datatable datatable responsive">
						<thead>
							<tr>
								<th>OrderId</th>
								<th>Order Date</th>
								<th>Payment Method</th>
								<th>Order Status</th>
								<th>Tax</th>
								<th>Shipping Charges</th>
								<th>Product Charges</th>
								<th>Total Cost</th>
								<th>Tracking Id</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList<Order> deliveredOrderList = OrderDB.getDeliveredOrder();
								for (int i = 0; i < deliveredOrderList.size(); i++) {
									Order order = deliveredOrderList.get(i);
							%>
							<tr>
								<td><a href="viewOrderDetails?orderId=<%=order.getOrderId()%>"><%=order.getOrderId()%></a></td>
								<%
									SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
								%>
								<td class="center"><%=formatter.format(order.getOrderDate())%></td>
								<td class="center"><%=order.getPayementMethod()%></td>
								<td class="center"><span
									class="label-success label label-default"><%=order.getOderStatus()%></span></td>
								<td class="center"><%=order.getTax()%></td>
								<td class="center"><%=order.getShippementCharges()%></td>
								<td class="center"><%=order.getProductCost()%></td>
								<td class="center"><%=order.getTotalCost()%></td>
								<td class="center"><%=order.getTrackingId()%></td>
								<td class="center"><a class="btn btn-success"
									href="viewOrderDetails?orderId=<%=order.getOrderId()%>"> <i
										class="glyphicon glyphicon-zoom-in icon-white"></i> View Order
								</a></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!--/span-->

	</div>
	<<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-user"></i> Failed delivery Orders
					</h2>


				</div>
				<div class="box-content">
					<table
						class="table table-striped table-bordered bootstrap-datatable datatable responsive">
						<thead>
							<tr>
								<th>OrderId</th>
								<th>Order Date</th>
								<th>Payment Method</th>
								<th>Order Status</th>
								<th>Tax</th>
								<th>Shipping Charges</th>
								<th>Product Charges</th>
								<th>Total Cost</th>
								<th>Tracking Id</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList<Order> unDeliveredOrderList = OrderDB.getUnDeliveredOrder();
								for (int i = 0; i < unDeliveredOrderList.size(); i++) {
									Order order = unDeliveredOrderList.get(i);
							%>
							<tr>
								<td><a href="viewOrderDetails?orderId=<%=order.getOrderId()%>"><%=order.getOrderId()%></a></td>
								<%
									SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
								%>
								<td class="center"><%=formatter.format(order.getOrderDate())%></td>
								<td class="center"><%=order.getPayementMethod()%></td>
								<td class="center"><span
									class="label-success label label-default"><%=order.getOderStatus()%></span></td>
								<td class="center"><%=order.getTax()%></td>
								<td class="center"><%=order.getShippementCharges()%></td>
								<td class="center"><%=order.getProductCost()%></td>
								<td class="center"><%=order.getTotalCost()%></td>
								<td class="center"><%=order.getTrackingId()%></td>
								<td class="center"><a class="btn btn-success"
									href="viewOrderDetails?orderId=<%=order.getOrderId()%>"> <i
										class="glyphicon glyphicon-zoom-in icon-white"></i> View Order
								</a></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!--/span-->

	</div>
	
	<jsp:include page="/admin/include/footer.jsp" />