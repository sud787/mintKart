/**
 *
 */

$(function() {
	var value = $("#selectedSize option:selected").attr('data-value');
	$('#selectedQuantity').empty();
	for(var i = 1; i<= value; i++) {
		$('#selectedQuantity').append($('<option>', {
		    value: i,
		    text: i
		})); 
	}
});

function changeQuantity() {
	var value = $("#selectedSize option:selected").attr('data-value');
	$('#selectedQuantity').empty();
	for(var i = 1; i<= value; i++) {
		$('#selectedQuantity').append($('<option>', {
		    value: i,
		    text: i
		}));
	}
}
