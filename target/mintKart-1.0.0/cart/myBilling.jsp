<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="com.ecommerce.data.CookieUtil,com.ecommerce.business.User,com.ecommerce.data.UserDB,com.ecommerce.data.MenuUtil,java.util.ArrayList,java.util.Iterator,com.ecommerce.business.Address"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Fashion Hub</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Fashion Club Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- css -->
		<link href="${pageContext.request.contextPath}/css/orderProcessing.css"
			rel="stylesheet" type="text/css" media="all" />
		<link href="${pageContext.request.contextPath}/css/bootstrap.css"
			rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet"
			href="${pageContext.request.contextPath}/css/style.css"
			type="text/css" media="all" />
		<link rel="stylesheet"
			href="${pageContext.request.contextPath}/css/font-awesome.min.css"
			type="text/css" media="all" /> <!--// css --> <!-- font -->
		<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro"
			rel="stylesheet">
		<link
			href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
			rel='stylesheet' type='text/css'> <!-- //font --> <script
			src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
		<script type="text/javascript">
function validateEmail() {
	//check email with co
	var email = $("#email").val(); 
	if(email==null || email == ""){
		$("#divCheckEmail").html(" ");
	}else{
	 var atpos = email.indexOf('@');
	    var dotpos = email.lastIndexOf('.');
	    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
	    	 $("#divCheckEmail").html("Email id is not valid");
	    //	 return false;
	    }  else{
	    	$("#divCheckEmail").html("");
	    //	return true;
	    }
	}    
	
}
function validatePostcode(){
	var regPostcode = /^([1-9])([0-9]){5}$/;
	//divCheckPinCode
	var pincode=$("#pincode").val();
	 if(regPostcode.test(pincode) == false)
	    {
		 $("#divCheckPinCode").html("Pin code id is not valid");

	    }else{
	    	 $("#divCheckPinCode").html("");
	    }
}
function validatePhoneNumber(){  
	var phone = $("#number").val(); 
	//alert('phone'+phone);
	if(phone==null || phone==""){
		$("#divCheckPhoneNumber").html("");
	}else{
		var phoneno = /^\d{10}$/;  
	  		if(phone.match(phoneno)){
		  		$("#divCheckPhoneNumber").html("");
		//  return true;
	  		}else{  
		  $("#divCheckPhoneNumber").html("Mobile number is not valid");
		 // return false;
	  		} 
		}		  		
}  
$(document).ready(function () {
   $(" #email").blur(validateEmail);
   $(" #number").blur(validatePhoneNumber);
   $(" #pincode").blur(validatePostcode); 
   
});

function validateSubmission(){
	var isPhoneSet=$("#divCheckPhoneNumber").html();
	var isEmailSet= $("#divCheckEmail").html();
	var isPincodeSet=$("#divCheckPinCode").html();
	if(isEmpty(isPhoneSet) && isEmpty(isEmailSet) && isEmpty(isPincodeSet)){
		return true;
	}else{
		return false;
	}
}
function isEmpty(str) {
	return (!str || 0 === str.length);
}
</script>
</head>

<body>
	<div class="header-top-w3layouts">
		<div class="container">
			<div class="col-md-6 logo-w3">
				<a href="${pageContext.request.contextPath}/index.jsp"><img
					src="${pageContext.request.contextPath}/images/Util/logo1.jpg" alt=" " />
					<h1>
						MINT<span>KART</span>
					</h1></a>
			</div>
		</div>
	</div>
<br/>

		<!-- another version - flat style with animated hover effect -->
		<div class="breadcrumb flat ">
			<a class="loginbreadcrumb">LOGIN</a> <a class="active shippingbreadcrumb">SHIPPING</a> <a class="paymentbreadcrumb">PAYEMENT</a> <a class="reviewbreadcrumb">REVIEW</a>
		</div>
		<%Address address=(Address)session.getAttribute("shippingAddress"); %>
		<form id="contactForm"
			action="${pageContext.request.contextPath}/addressCheckServlet"
			name="contactForm" method="POST" onsubmit="return validateSubmission()">

			<h4 style="float: left;">SHIPPING ADDRESS</h4>
			<br /> <br />
			<h6 style="float: right; margin-right: 200px; color: #CC3300;">*
				Required Fields</h6>
			<br />
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<label for="name">Name*</label> <input id="name" type="text"
							value="<%=(address != null ? address.getName(): "") %>" placeholder="Enter Full Name" name="name" required /><label>Email
							Address*</label><input id="email" type="text" value="<%=(address != null ? address.getEmail(): "") %>" name="email"
							placeholder="Enter Email Address" required />
						<p class="registrationFormAlert" id="divCheckEmail"
							style="color: red;"></p>
						<label>State*</label><input id="state" type="text" value="<%=(address != null ? address.getState(): "") %>"
							name="state" placeholder="Enter State" required />
					</div>
					<div class="col-md-3">
						<label>Telephone Number*</label><input id="number" type="text"
							value="<%=(address != null ? address.getNumber(): "") %>" name="number" placeholder="Enter Telephone Number"
							required />
						<p class="registrationFormAlert" id="divCheckPhoneNumber"
							style="color: red;"></p>
						<label>City*</label><input id="city" type="text" value="<%=(address != null ? address.getCity(): "") %>"
							name="city" placeholder="Enter City" required /> <label>Pin
							Code*</label><input id="pincode" type="text" value="<%=(address != null ? address.getPincode(): "") %>" name="pincode"
							placeholder="Enter Pin Code" required />
						<p class="registrationFormAlert" id="divCheckPinCode"
							style="color: red;"></p>

					</div>
				</div>
				<div>
					<label for="message">Address*</label>
					<textarea id="address" rows="7" name="address"
						placeholder="Enter Address" style="width: 283px;" required><%=(address != null ? address.getAddress(): "") %></textarea>
				</div>
				<br />
				<a
						href="${pageContext.request.contextPath}/cartSummary"><button
							class="btn btn-large btn-primary" type="button" name="submit" value="Back">Back</button></a>
					<input class="btn btn-large btn-primary" type="submit" name="submit"
						value="Continue" /> 
			</div>
		</form>
</body>
</html>