<%@ include file="/include/header.jsp"%>
<%@ page import="com.ecommerce.business.Product"%>
<script src="js/filteringUI.js">
<!--
	
//-->
</script>
<div class="content" style="background: white;">
	<div class="container">
		<div class="col-md-4 w3ls_dresses_grid_left">
			<div class="w3ls_dresses_grid_left_grid">
				<h3>Categories</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_dres-type">
						<ul>
							<!-- store this in request object from controller-->
							<!--fetch from sub catgory Table -->
							<!-- fetch the category list from request object -->
							<%
								ArrayList<String> categoryList = (ArrayList<String>) request
										.getAttribute("categoryList");
								if (categoryList != null) {
									Iterator<String> itrNew = categoryList.iterator();
									while (itrNew.hasNext()) {
										String categoryName = itrNew.next();
							%>
							<li><a href="displayProduct?type=<%=categoryName%>"><%=categoryName%></a></li>
							<%
								}
								}
							%>
						</ul>
					</div>
				</div>
			</div>
			<%
				HashSet<String> colourList = (HashSet<String>) request
						.getAttribute("colourSet");
				if (colourList != null && colourList.size() > 0) {
			%>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>Color</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color">
						<ul>
							<%
								Iterator<String> colorItr = colourList.iterator();
									while (colorItr.hasNext()) {
										String colour = colorItr.next();
							%>
							<li>
								<%-- <li><a href="#"><i style="background:<%=colour%>"></i> <%=colour%></a></li> --%>
								<input type="checkbox" id="colorFilter" class="colorFilter"
								name="colorFilter" value="<%=colour%>"
								onclick="filterProducts();" /> <label for="first_checkbox_btn"><%=colour%></label>
							</li>
							<%
								}
							%>
						</ul>
					</div>
				</div>
			</div>
			<%
				}
			%>
			<%
				HashSet<String> sizeList = (HashSet<String>) request
						.getAttribute("sizeSet");
				if (sizeList != null && sizeList.size() > 0) {
			%>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>Size</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<%
								Iterator<String> sizeItr = sizeList.iterator();
									while (sizeItr.hasNext()) {
										String size = sizeItr.next();
							%>
							<li><input type="checkbox" id="sizeFilter"
								class="sizeFilter" name="sizeFilter" value="<%=size%>"
								onclick="filterProducts();" /><label for="first_checkbox_btn"><%=size%></label>
								<%-- 	<li><a href="#"><i style="background: red"></i><%=size%></a></li> --%>
							</li>
							<%
								}
							%>
						</ul>
					</div>
				</div>
			</div>
			<%
				}
			%>
			<!--  -->
			<%
				HashSet<String> brandList = (HashSet<String>) request
						.getAttribute("brandList");
				if (brandList != null && brandList.size() > 0) {
			%>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>Brand</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<!-- store this in request object from controller-->
							<!--fetch from sub catgory Table -->
							<!-- fetch the category list from request object -->
							<%
								//
									//HashSet<String> colourList = (HashSet<String>) request.getAttribute("colourSet");
									//if (colourList != null && colourList.size() > 0) {

									Iterator<String> brandNew = brandList.iterator();
									while (brandNew.hasNext()) {
										String brandName = brandNew.next();
							%>
							<li>
								<%-- <a href="displayProduct?type=<%=brandName%>"><%=brandName%></a> --%>
								<input type="checkbox" id="brandFilter" class="brandFilter"
								name="brandFilter" value="<%=brandName%>"
								onclick="filterProducts();" /><label for="first_checkbox_btn"><%=brandName%></label>
							</li>
							<%
								}
							%>
						</ul>
					</div>
				</div>
			</div>
			<%
				}
			%>
		</div>
		<!-- this div is the main enclosing -->
		<input type="hidden" name="subtype"
			value="<%=request.getParameter("type")%>" id="subtype">
		<div class="productList" id="productList">
			<div class="col-md-9 col-sm-8 women-dresses">
				<%
					ArrayList<Product> productList = (ArrayList<Product>) request
							.getAttribute("productList");

					if (productList != null) {
						Iterator<Product> itrNew = productList.iterator();
						while (itrNew.hasNext()) {
							Product product = itrNew.next();
				%>
				<div class="col-md-4 women-grids wp1 animated wow slideInUp"
					data-wow-delay=".5s">
					<a href="displaySingleProduct?productId=<%=product.getId()%>"><div
							class="product-img">
							<img src="<%=product.getImageURL()%>" alt="" />

						</div></a> <!-- <i class="fa fa-star yellow-star" aria-hidden="true"></i> <i
						class="fa fa-star yellow-star" aria-hidden="true"></i> <i
						class="fa fa-star yellow-star" aria-hidden="true"></i> <i
						class="fa fa-star yellow-star" aria-hidden="true"></i> <i
						class="fa fa-star gray-star" aria-hidden="true"></i> -->
					<h4><%=product.getName()%></h4>
					<%
						if (product.getDiscount() > 0) {
					%>
					<h3 class="w3offDiscount"><%=product.getDiscount()%>%OFF
					</h3>
					<%
						}
					%>
					<h5 class="product-price"><%=product.getSellingPrice()%></h5>
					<br />

				</div>
				<%
					}
					}
				%>
				<br />
			</div>
		</div>
	</div>
	<div class="paginationButton" id="paginationButton">
		<%
			int currentPage = (Integer) request.getAttribute("currentPage");
			if (currentPage != 1) {
		%>
		<a
			href="displayProduct?type=<%=request.getParameter("type")%>&page=<%=currentPage - 1%>"
			class="previous round">&#8249;</a>
		<%
			}
			if (currentPage < (Integer) request.getAttribute("noOfPages")) {
		%>
		<a
			href="displayProduct?type=<%=request.getParameter("type")%>&page=<%=currentPage + 1%>"
			class="next round">&#8250;</a>
		<%
			}
		%>
	</div>
</div>

<jsp:include page="/include/footer.jsp" />