/**
 * 
 */
function editTopCategory() {
	var editedtopCategory = $('#topCategory').val();
	var topCategoryValue = $("#existingTopCategory option:selected").attr('data-value');

	if (isEmpty(editedtopCategory)) {
		alert('Enter Top Category');
		// return false;
	} else {
		request = createRequest();

		request.open("POST", "editMenu", true);
		request.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		request.send("topCategory=" + topCategoryValue+"&updatedTopCategory="+editedtopCategory);
		window.location.reload(true);
		// addMenu
	}

}
function editCategory() {
	var updatedcategory = $('#newCategory').val();
	var categoryValue = $("#category option:selected").attr('value');

	if (isEmpty(updatedcategory)) {
		alert('Enter  Category');
		// return false;
	} else {
		request = createRequest();

		request.open("POST", "editMenu", true);
		request.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		request.send("category=" + categoryValue + "&updatedCategory="
				+ updatedcategory);
		window.location.reload(true);
		// addMenu
	}

}
function editSubCategory() {
	var updatedSubCategory = $('#newSubCategory').val();
	var subCategory = $("#subCategory option:selected").attr('value');

	if (isEmpty(updatedSubCategory)) {
		alert('Enter Sub Category');
		// return false;
	} else {
		request = createRequest();

		request.open("POST", "editMenu", true);
		request.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		request.send("updatedSubCategory=" + updatedSubCategory +"&subCategory="
				+ subCategory);
		window.location.reload(true);

	}
}
function createRequest() {
	try {
		request = new XMLHttpRequest();
	} catch (tryMS) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (otherMS) {
			try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {
				request = null;
			}
		}
	}
	return request;
}
function isEmpty(str) {
	return (!str || 0 === str.length);
}