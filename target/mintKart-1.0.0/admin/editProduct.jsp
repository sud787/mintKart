
<%@page import="com.ecommerce.business.Product"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.ecommerce.data.MenuUtil"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<jsp:include page="/admin/include/header.jsp" />
<div id="content" class="col-lg-10 col-sm-10">
	<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-edit"></i> Edit products
					</h2>
				</div>
				<%
					Product product = (Product) request.getAttribute("product");
					String update = (String) request.getAttribute("updateSucess");
					if (update != null && !update.equals("")) {
				%>
				<input type="hidden" name="update" id="update" value="<%=update%>">
				<%
					}
				%>
				<form role="form"
					action="updateProduct?productId=<%=product.getId()%>" method="POST">
					<input type="hidden" name="subCategoryId"
						value="<%=product.getSubCategoryId()%>">
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Product
							Name</label> <input type="text" class="form-control" id="productName"
							name="productName" value="<%=product.getName()%>
							"
							required="required">
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Product
							Code </label> <input type="text" class="form-control" id="productCode"
							name="productCode" value="<%=product.getProductCode()%>"
							required="required" readonly="readonly">
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Product
							Color </label> <input type="text" class="form-control" id="colour"
							name="colour" value="<%=product.getColour()%>"
							required="required">
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Brand
							Name </label> <input type="text" class="form-control" id="brand"
							name="brand" value="<%=product.getBrand()%>" required="required">
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Cost
							Price </label> <input type="text" class="form-control" id="costPrice"
							name="costPrice" value="<%=product.getCostPrice()%>" required>
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">
							Price(Exclusive Tax) </label> <input type="text" class="form-control"
							id="sellingPrice" name="sellingPrice"
							value="<%=product.getSellingPrice()%>" required>
					</div>

					<div class="sizeQuantity" id="sizeQuantity">
						<%
							HashMap<Integer, Integer> sizeQuantityMap = (HashMap<Integer, Integer>) request.getAttribute("sizes");

							if (sizeQuantityMap.size() > 0) {
								for (Map.Entry<Integer, Integer> entry : sizeQuantityMap.entrySet()) {
									int size = entry.getKey();
									int quantity = entry.getValue();
									if (size == 0) {
						%>

						<div class="form-group has-success col-md-4">
							<label class="control-label" for="inputSuccess1">Size(In
								CMS) </label> <input type="text" class="form-control" id="size"
								name="size" readonly="readonly">
						</div>
						<%
							} else {
						%>
						<div class="form-group has-success col-md-4">
							<label class="control-label" for="inputSuccess1">Size(In
								CMS) </label> <input type="text" class="form-control" id="size"
								value="<%=size%>" name="size">
						</div>
						<%
							}
						%>

						<div class="form-group has-success col-md-4">
							<label class="control-label" for="inputSuccess1">Quantity
							</label> <input type="text" class="form-control" id="quantity"
								value="<%=quantity%>" name="quantity">
						</div>
						<%
							}
							}
						%>
						<div class="form-group has-success col-md-4"
							id="addSizeQuantityElement">
							<br /> <br /> <a href="#" onclick="addSizeQuantityElement()"><i
								class="glyphicon glyphicon-plus-sign"></i></a> <br /> <br />

						</div>

					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Tax </label> <input
							type="text" class="form-control" id="tax" name="tax"
							value="<%=product.getTax()%>" required>
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">HSN
							Number </label> <input type="text" class="form-control" id="hsn"
							value="<%=product.getHSNNumber()%>" name="hsn" required>

					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Selling
							Price Tax Inclusive </label> <input type="text" class="form-control"
							id="sellinPriceTaxInclusive" name="sellingPriceWithTax" required
							readonly="readonly">

					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Discount
						</label> <input type="text" class="form-control" id="discount"
							name="discount" required readonly="readonly">

					</div>


					<div class="box-content">
						<label for="productDescription">Product Description</label> <br />
						<textarea id="productDescription" rows="7"
							name="productDescription" placeholder="Enter product description"
							style="width: 283px;" required><%=product.getProductShortDescription()%></textarea>
						<br /> <label for="productLongDescription">Product Long
							Description</label> <br />
						<textarea id="productLongDescription" rows="7"
							name="productLongDescription"
							placeholder="Enter product Long description"
							style="width: 283px;" required><%=product.getProductLongDescription()%></textarea>
						<br /> <label for="productAdditionalDescription">Product
							Additional Information</label> <br />
						<textarea id="productAdditionalDescription" rows="7"
							name="productAdditionalDescription"
							placeholder="Enter product Additional description"
							style="width: 283px;" required><%=product.getProductAdditionalInformation()%></textarea>
						<br />
						<button type="submit" class="btn btn-default">Submit</button>
					</div>


				</form>
			</div>
		</div>
		<!--/span-->

	</div>
</div>
<script src="js/addProduct.js">
	<jsp:include page="/admin/include/footer.jsp" />
