
<%@page import="java.util.Iterator"%>
<%@page import="com.ecommerce.data.MenuUtil"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<jsp:include page="/admin/include/header.jsp" />
<div id="content" class="col-lg-10 col-sm-10">
	<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-edit"></i> Add products
					</h2>
				</div>
				<%
					String added = (String) request.getAttribute("added");
					if (added != null && !added.equals("")) {
				%>
				<input type="hidden" name="added" id="added" value="<%=added%>">
				<%
					}
				%>

				<form role="form" action="upload2S3" method="POST"
					enctype="multipart/form-data">
					<%
						ArrayList<String> topCategory = MenuUtil.getTopCategory();
						Iterator<String> itr = topCategory.iterator();
					%>
					<div class="form-group has-error col-md-4">
						<label class="control-label" for="inputError1">Top
							Category</label>
						<div class="controls">
							<select id="topCategory" class="topCategory"
								onchange="changeCategory()">
								<%
									while (itr.hasNext()) {
										String value = itr.next();
								%>
								<option data-value="<%=value%>"><%=value%></option>
								<%
									}
								%>
							</select>
						</div>
					</div>
					<div class="form-group has-error col-md-4">
						<label class="control-label" for="selectError">Category </label>
						<div class="controls">
							<select id="category" id="category" onchange="getSubCategory()">

							</select>
						</div>
					</div>
					<div class="form-group has-error col-md-4">
						<label class="control-label" for="selectError">Sub
							Category</label>

						<div class="controls">
							<select id="subCategory" class="subCategory" name="subCategory">
							</select>
						</div>

					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Product
							Name</label> <input type="text" class="form-control" id="productName"
							name="productName" required="required">
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Product
							Code </label> <input type="text" class="form-control" id="productCode"
							name="productCode" required="required">
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Product
							Color </label> <input type="text" class="form-control" id="colour"
							name="colour" required="required">
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Brand
							Name </label> <input type="text" class="form-control" id="brand"
							name="brand" required="required">
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Cost
							Price </label> <input type="text" class="form-control" id="costPrice"
							name="costPrice" required>
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">
							Price(Exclusive Tax) </label> <input type="text" class="form-control"
							id="sellingPrice" name="sellingPrice" required>
					</div>

					<div class="sizeQuantity" id="sizeQuantity">

						<div class="form-group has-success col-md-4">
							<label class="control-label" for="inputSuccess1">Size(In
								CMR) </label> <input type="text" class="form-control" id="size"
								name="size">
						</div>
						<div class="form-group has-success col-md-4">
							<label class="control-label" for="inputSuccess1">Quantity
							</label> <input type="text" class="form-control" id="quantity"
								name="quantity">
						</div>
						<div class="form-group has-success col-md-4"
							id="addSizeQuantityElement">
							<br /> <br /> <a href="#" onclick="addSizeQuantityElement()"><i
								class="glyphicon glyphicon-plus-sign"></i></a> <br /> <br />

						</div>

					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Tax </label> <input
							type="text" class="form-control" id="tax" name="tax" required>
					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">HSN
							Number </label> <input type="text" class="form-control" id="hsn"
							name="hsn" required>

					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Selling
							Price Tax Inclusive </label> <input type="text" class="form-control"
							id="sellinPriceTaxInclusive" name="sellingPriceWithTax" required
							readonly="readonly">

					</div>
					<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">Discount
						</label> <input type="text" class="form-control" id="discount"
							name="discount" required readonly="readonly">

					</div>
					<br />


					<div class="box-content">
						<div class="form-group">

							<label class="abcdefgh " for="exampleInputFile">File
								input</label> <input type="file" id="image1" name="image1">
						</div>
						<div class="form-group">
							<label for="exampleInputFile">File input</label> <input
								type="file" id="image2" name="image2">
						</div>
						<div class="form-group">
							<label for="exampleInputFile">File input</label> <input
								type="file" id="image3" name="image3">
						</div>
						<label for="productDescription">Product Description</label> <br />
						<textarea id="productDescription" rows="7"
							name="productDescription" placeholder="Enter product description"
							style="width: 283px;" required></textarea>
						<br /> <label for="productLongDescription">Product Long
							Description</label> <br />
						<textarea id="productLongDescription" rows="7"
							name="productLongDescription"
							placeholder="Enter product Long description"
							style="width: 283px;" required></textarea>
						<br /> <label for="productAdditionalDescription">Product
							Additional Information</label> <br />
						<textarea id="productAdditionalDescription" rows="7"
							name="productAdditionalDescription"
							placeholder="Enter product Additional description"
							style="width: 283px;" required></textarea>
						<br />
						<button type="submit" class="btn btn-default">Submit</button>
					</div>


				</form>
			</div>
		</div>
		<!--/span-->

	</div>
</div>
<script src="js/addProduct.js">
	<jsp:include page="/admin/include/footer.jsp" />
