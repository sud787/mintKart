<jsp:include page="/include/header.jsp" />
<script>
function checkPasswordMatch() {
    var password = $("#password").val();
    var confirmPassword = $("#confirmPassword").val();
    if((password==null || password=="") || (confirmPassword==null || confirmPassword=="") ){
    	$("#divCheckPasswordMatch").html("");
    }else{
	    if (password != confirmPassword){
	        $("#divCheckPasswordMatch").html("Passwords do not match!");
	    	return false;
	    }
	    else{
	        $("#divCheckPasswordMatch").html("Passwords match.");
	    	return true;
	    }  
    }   
}

$(document).ready(function () {
   $("#password, #confirmPassword").keyup(checkPasswordMatch);
});
</script>
<div class="login">

	<div class="main-agileits">
		<div class="form-w3agile">
		<h5>Password updated sucessfully</h5>
			<div id="scoped-content">
				<style type="text/css" scoped>
h6 {
	color: red;
	margin-top: -40px;
	margin-bottom: 10px;
}
</style>
			</div>
			<%
				if (request.getAttribute("error") != null
						&& !request.getAttribute("error").equals("")) {
			%>

			<h6><%=request.getAttribute("error")%>
			</h6>

			<%
				}
			%>
			<%-- <form action="updatePassword" method="post">
				<input type="hidden" name="token"
					value="<%=request.getAttribute("token")%>">
				<div class="key">
					<i class="fa fa-lock" aria-hidden="true"></i> <input
						type="password" id="password" name="password" required=""
						placeholder="Enter new Password">
					<div class="clearfix"></div>
				</div>
				<div class="registrationFormAlert" id="divCheckPasswordMatch"
					style="color: red;"></div>
				<div class="key">
					<i class="fa fa-lock" aria-hidden="true"></i> <input
						type="password" id="confirmPassword" name="confirmPassword"
						placeholder="ReEnter new Password">
					<div class="clearfix"></div>
				</div>
				<input type="submit" value="Submit" style="align: center;">
			</form> --%>
		</div>
	</div>
</div>
<jsp:include page="/include/footer.jsp" />