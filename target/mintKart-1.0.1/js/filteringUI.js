/**
 * 
 */

function onPagination(page) {
	filterProducts(page);
}
function filterProducts(){
	filterProducts('');
}
function filterProducts(page) {
	// var filterBrand=document.getElementsByName("brandFilter");
	// var checkedValue = $('.brandFilter:checked').val();
	// for brand
//	alert(page);
	var currentPage = 1;
	if (!isEmpty(page)) {
	//	alert(page);
		currentPage = page;
	}
	/*
	 * var currentPageHTML = document.getElementById('currentPage'); if
	 * (!isEmpty(currentPageHTML)) { currentPage = currentPageHTML.value; }
	 */
	var brandInputElements = document.getElementsByClassName('brandFilter');
	var colorInputElements = document.getElementsByClassName('colorFilter');
	var sizeInputElements = document.getElementsByClassName('sizeFilter');
	var subtype = document.getElementById("subtype").value;

	var brandFilter = '';
	var sizeFilter = '';
	var colorFilter = '';
	for (var i = 0; brandInputElements[i]; ++i) {
		if (brandInputElements[i].checked) {
			if (isEmpty(brandFilter)) {
				brandFilter = brandInputElements[i].value.trim();

			} else {
				brandFilter = brandFilter + ','
						+ brandInputElements[i].value.trim();
			}
		//	alert(brandFilter);
		}
	}
	for (var i = 0; colorInputElements[i]; ++i) {
		if (colorInputElements[i].checked) {
			if (isEmpty(colorFilter)) {
				colorFilter = colorInputElements[i].value.trim();

			} else {
				colorFilter = colorFilter + ','
						+ colorInputElements[i].value.trim();
			}
			//alert(colorFilter);
		}
	}
	for (var i = 0; sizeInputElements[i]; ++i) {
		if (sizeInputElements[i].checked) {
			if (isEmpty(sizeFilter)) {
				sizeFilter = sizeInputElements[i].value.trim();

			} else {
				sizeFilter = sizeFilter + ','
						+ sizeInputElements[i].value.trim();
			}
			//alert(sizeFilter);
		}
	}

	request = createRequest();
	request.responseText = 'application/json';
	request.open("GET", "productFiltering?brandFilter=" + brandFilter
			+ "&sizeFilter=" + sizeFilter + "&colorFilter=" + colorFilter
			+ "&subtype=" + subtype + "&page=" + currentPage, true);
	$('#loader').show();
	request.send(null);
	request.onreadystatechange = function() {
		/*setTimeout(function() {
			$('#loader').hide();
		}, 1000);*/
		if (request.readyState == 4 && request.status == 200) {
			$('#loader').hide();
			// <input type="hidden" name="subtype"
			// value="<%=request.getParameter("type")%>" id="subtype">
			var htmplContent = '<input type="hidden" name="currentPage" id="currentPage" value="'
					+ currentPage
					+ '">'
					+ '<div class="col-md-8 col-sm-8 women-dresses">';
			var data = request.responseText;

			var dataParsed = JSON.parse(data);
			var noOfpages = dataParsed.noOfPages;
			//alert(noOfpages);
			var productInfo = dataParsed.productInfo;
			// alert(dataParsed);
			// var header='<div class="col-md-4 women-grids wp1 animated wow
			// slideInUp" data-wow-delay=".5s">';
			for (var i = 0; i < productInfo.length; i++) {
				var obj = productInfo[i];

				htmplContent = htmplContent
						+ '<div class="col-md-4 women-grids wp1 animated wow slideInUp" data-wow-delay=".5s">'
						+ '<a href="displaySingleProduct?productId='
						+ obj['id']
						+ '">'
						+ '<div class="product-img">'
						// images/products/" + productCode + "_" + colour +
						// "_01.PNG
						+ '<img src="https://s3.ap-south-1.amazonaws.com/mintkartaws/products/'
						+ obj['productCode']
						+ "_"
						+ obj['colour']
						+ '_01.PNG" alt="" />'
						+ '</div>'
						+ '</a>'
						+ '<i class="fa fa-star yellow-star" aria-hidden="true"></i> '
						+ '<i class="fa fa-star yellow-star" aria-hidden="true"></i> '
						+ '<i class="fa fa-star yellow-star" aria-hidden="true"></i>'
						+ ' <i class="fa fa-star yellow-star" aria-hidden="true"></i>'
						+ ' <i class="fa fa-star gray-star" aria-hidden="true"></i>'
						+ '<h4>' + obj['name'] + '</h4>';
				if (obj['discount'] > 0) {
					htmplContent = htmplContent + ' <h3 class="w3offDiscount">'
							+ obj['discount'] + '%OFF </h3>';
				}
				htmplContent = htmplContent + '<h5 class="product-price">'
						+ obj['sellingPrice'] + '</h5> <br /> </div>';

				/*
				 * for ( var key in obj) { var attrName = key; var attrValue =
				 * obj[key]; }
				 */
			}
			htmplContent = htmplContent
					+ '<br /> <div class="clearfix"></div></div>';
			$('#productList').html(htmplContent);
			var pagination = '';
			if (currentPage != 1) {
				pagination ='<a href="#" onclick="onPagination('+ (currentPage- 1)+ '); return false;" class="previous round">'+'&#8249;'+'</a> ';
			}
			if (currentPage < noOfpages) {
				pagination = pagination + '<a href="#" onclick="onPagination('+ (currentPage + 1) + '); return false;" class="next round">'+'&#8250;'+'</a>';
			}

			// alert(pagination);
			 $('#paginationButton').html(pagination);

		}

		/*
		 * var inputElements = document.getElementsByClassName('colourFilter');
		 * for (var i = 0; inputElements[i]; ++i) { if
		 * (inputElements[i].checked) { checkedValue = inputElements[i].value;
		 * alert(checkedValue); } } var inputElements =
		 * document.getElementsByClassName('sizeFilter'); for (var i = 0;
		 * inputElements[i]; ++i) { if (inputElements[i].checked) { checkedValue =
		 * inputElements[i].value; alert(checkedValue); } }
		 */
	}
}
function isEmpty(str) {
	return (!str || 0 === str.length);
}
function createRequest() {
	try {
		request = new XMLHttpRequest();
	} catch (tryMS) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (otherMS) {
			try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {
				request = null;
			}
		}
	}
	return request;
}
