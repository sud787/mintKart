/* Set rates + misc */
var taxRate = 0.05;
var shippingRate = 15.00;
var fadeTime = 100;

/* Assign actions */
$('.productCart-quantity input').change(function() {
	updateQuantity(this);
});

$('.productCart-removal button').click(function() {
	removeItem(this);
});
function isErrorMessageSet() {
	var errorMessgae = $('.productCart-errorMessage').html();
	if (!isEmpty(errorMessgae)) {
		return false;
	} else {
		return true;
	}
}
/* Recalculate cart */
function recalculateCart() {
	var subtotal = 0;

	/* Sum up row totals */
	$('.productCart').each(
			function() {
				subtotal += parseFloat($(this).children(
						'.productCart-line-price').text());
			});

	/* Calculate totals */
	var tax = subtotal * taxRate;
	var shipping = (subtotal > 0 ? shippingRate : 0);
	var total = subtotal + tax + shipping;

	/* Update totals display */
	$('.totals-value').fadeOut(fadeTime, function() {
		$('#cart-subtotal').html(subtotal.toFixed(2));
		$('#cart-tax').html(tax.toFixed(2));
		$('#cart-shipping').html(shipping.toFixed(2));
		$('#cart-total').html(total.toFixed(2));
		if (total == 0) {
			$('.checkout').fadeOut(fadeTime);
		} else {
			$('.checkout').fadeIn(fadeTime);
		}
		$('.totals-value').fadeIn(fadeTime);
	});
}

/* Update quantity */
function updateQuantity(quantityInput) {
	/* Calculate line price */
	var productCartRow = $(quantityInput).parent().parent();
	var price = parseFloat(productCartRow.children('.productCart-price').text());
	var size = productCartRow.children('.productCart-size').text();
	var quantity = $(quantityInput).val();
	var productId = productCartRow.children().val();

	var linePrice = price * quantity;
	var errorMessgae = $('.productCart-errorMessage').html();
	if (isEmpty(errorMessgae)) {
		if (quantity > 0) {
			var request = createRequest();

			request.open("GET", "ajaxHandleServlet?productId=" + productId
					+ "&quantity=" + quantity + "&size=" + size, true);
			$('#loader').show();
			request.send(null);
			request.onreadystatechange = function() {
				/*
				 * setTimeout(function() { $('#loader').hide(); }, 1000);
				 */
				if (request.readyState == 4 && request.status == 200) {
					$('#loader').hide();
					var data = request.responseText;
					if (isEmpty(data)) {
						/* Update line price display and recalc cart totals */
						productCartRow.children('.productCart-line-price')
								.each(function() {
									$(this).fadeOut(fadeTime, function() {
										$(this).text(linePrice.toFixed(2));
										recalculateCart();
										$(this).fadeIn(fadeTime);
									});
								});
					} else {
						$(quantityInput).val(data);
						productCartRow.children('.productCart-details')
								.children('.productCart-quantityMessage').html(
										'Only ' + data
												+ ' units can be ordered');
						;

						// $('.productCart-quantityMessage').html(
						// 'Only ' + data + ' units can be ordered');
						linePrice = price * data;
						productCartRow.children('.productCart-line-price')
								.each(function() {
									$(this).fadeOut(fadeTime, function() {
										$(this).text(linePrice.toFixed(2));
										recalculateCart();
										$(this).fadeIn(fadeTime);
									});
								});
					}
				}
			}
		} else {
			$(quantityInput).val(1);
		}
	} else {
		alert('Product is out of stock');
		$(quantityInput).val(0);
	}
}

/* Remove item from cart */
function removeItem(removeButton) {
	/* Remove row from DOM and recalc cart total */
	// make the ajaxCall
	var productCartRow = $(removeButton).parent().parent().parent();
	var productId = productCartRow.children().val();
	var size = productCartRow.children('.productCart-size').text();

	request = createRequest();

	request.open("GET", "ajaxHandleServlet?productId=" + productId + "&size="
			+ size, true);
	$('#loader').show();
	request.send(null);
	request.onreadystatechange = function() {
		/*
		 * setTimeout(function() { $('#loader').hide(); }, 1000);
		 */
		if (request.readyState == 4 && request.status == 200) {
			$('#loader').hide();
			var data = request.responseText;
			if (!isEmpty(data)) {
				var emptyBagHtml='<div class="emptyCart" id="emptyCart">'
			+'<h3'
			+'	style="color: #7c795d; font-family: \'Trocchi\', serif; font-size: 35px; font-weight: normal; line-height: 48px; margin: 0;">Your'
			+'	Cart Is Empty</h3> '
			+ '<a href="index.jsp">'
			+	'<button class="checkout123" '
			+	'style="text-align: center; margin-left: 590px;">Continue'
			+	'	Shopping</button>'
			+'</a>'
			+'</div>';
				
				$('#bagSize').html('My Bag(0)');
				$('#productDivider').remove();
				$('#nonemptyCart').html(emptyBagHtml);
				$('#column-labels').remove();
			}
			productCartRow.slideUp(fadeTime, function() {
				productCartRow.remove();
				recalculateCart();
			});
		}
	}
}

function createRequest() {
	try {
		request = new XMLHttpRequest();
	} catch (tryMS) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (otherMS) {
			try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {
				request = null;
			}
		}
	}
	return request;
}
function isEmpty(str) {
	return (!str || 0 === str.length);
}