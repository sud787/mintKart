<jsp:include page="/include/header.jsp" />
<div class="scoped">
	<style scoped>

.loginbutton {
	background-color: #F0F0F0;
	border-radius: 5px/5px;
	-webkit-border-radius: 5px/5px;
	-moz-border-radius: 5px/5px;
	color: #333;
	font-family: "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans",
		"DejaVu Sans", Verdana, sans-serif;
	font-size: 18px;
	font-weight: bold;
	width: 270px;
	text-align: center;
	line-height: 50px;
	text-decoration: none;
	height: 50px;
	margin-top: 20px;
	margin-bottom: 20px;
	border: none;
	outline: 0;
	cursor: pointer;
	margin-left: 60px;
	
}

.loginbutton:active {
	position: relative;
	top: 1px;
}

.loginbutton:hover {
	background-color: #eea29a;
}
.userCheckoutOptions {
	margin: 20px auto;
	width: 40%;
	padding: 30px 25px;
	padding-left: 50px;
	background: white;
	border: 0.5px solid #F0F0F0;
	border-radius: 5px;
	height: 350px;
}
@media screen and (max-width: 800px) {
	.userCheckoutOptions {
	margin: 20px auto;
    width: 80%;
    padding: 30px 25px;
    padding-left: 50px;
    background: white;
    border: 0.5px solid #F0F0F0;
    border-radius: 5px;
    height: 350px;
		
	}
}

</style>


	<div class="userCheckoutOptions">
		<h3 class="topHeader" style="margin-top: 10px;margin-left: -20px;">New to Mint Kart ?</h3>
		<hr />
		<a href="cart/myBilling.jsp" ><h3 class="btn btn-primary" style="display:block">Continue as
			Guest</h3></a>
			<br />
	<a href="login.jsp?source=checkoutProcessing"><h3 class="btn btn-primary" style="display:block">Already Registered? Login</h3>
	</a>
	<br />
	<a href="register.jsp?source=checkoutProcessing">	<h3 class="btn btn-primary" style="display:block">Sign UP</h3></a>
	</div>
</div>

<jsp:include page="/include/footer.jsp" />