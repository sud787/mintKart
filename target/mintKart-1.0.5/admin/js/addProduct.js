$(function() {
	var topCategoryValue = $("#topCategory option:selected").attr('data-value');
	
	var added=$('#added').attr('value');
	if(added!=null){
		alert('Product Added Sucessfully');
	}
	// make an ajax call to fetch the
	if (!isEmpty(topCategoryValue)) {
		$('#category').empty();
		var categorySelect = document.getElementById("category");
		/*
		 * for (var i = 1; i <= 4; i++) { $('#category').append($('<option>', {
		 * value : i, text : i })); }
		 */
		request = createRequest();
		request.open("GET", "menuCategory?topCategory=" + topCategoryValue,
				true);
		request.send(null);
		request.onreadystatechange = function() {
			if (request.readyState == 4 && request.status == 200) {
				var data = request.responseText;
				var dataParsed = JSON.parse(data);
				var categoryMenu = dataParsed.categoryMenu;
				// alert(categoryMenu);
				for (var i = 0; i < categoryMenu.length; i++) {
					var obj = categoryMenu[i];
					$('#category').append($('<option>', {
						value : obj,
						text : obj
					}));
				}
				getSubCategory();
			}
		}
	}else{
		var update=$('#update').attr('value');
		if(update!=null){
			alert('Product Updated Sucessfully');
		}
	}
});

function changeCategory() {
	var topCategoryValue = $("#topCategory option:selected").attr('data-value');

	// make an ajax call to fetch the
	$('#category').empty();
	request = createRequest();
	request.open("GET", "menuCategory?topCategory=" + topCategoryValue, true);
	request.send(null);
	request.onreadystatechange = function() {
		if (request.readyState == 4 && request.status == 200) {
			var data = request.responseText;
			var dataParsed = JSON.parse(data);
			var categoryMenu = dataParsed.categoryMenu;
			// alert(categoryMenu);
			for (var i = 0; i < categoryMenu.length; i++) {
				var obj = categoryMenu[i];
				$('#category').append($('<option>', {
					value : obj,
					text : obj
				}));
			}
			getSubCategory();
		}
	}
}
function getSubCategory() {

	var categoryValue = $("#category option:selected").attr('value');
	// make an ajax call to fetch the
	$('#subCategory').empty();
	request = createRequest();
	request.open("GET", "menuCategory?category=" + categoryValue, true);
	request.send(null);
	request.onreadystatechange = function() {
		if (request.readyState == 4 && request.status == 200) {

			var data = request.responseText;
			var dataParsed = JSON.parse(data);
			var categoryMenu = dataParsed.subCategoryMenu;
			// alert(categoryMenu);
			for (var i = 0; i < categoryMenu.length; i++) {
				var obj = categoryMenu[i];
				$('#subCategory').append($('<option>', {
					value : obj,
					text : obj
				}));
			}
		}
	}
}
function addSizeQuantityElement() {
	// $('#addSizeQuantityElement').remove();

	var addButtonHtml = '<div class="form-group has-success col-md-4">'
			+ '<label class="control-label" for="inputSuccess1">Size </label> <input '
			+ 'type="text" class="form-control" id="size" name="size" >'
			+ '</div>'
			+ '<div class="form-group has-success col-md-4">'
			+ '<label class="control-label" for="inputSuccess1">Quantity'
			+ '</label> <input type="text" class="form-control" id="quantity"'
			+ 'name="quantity" >'
			+ '</div>'
			+ '<div class="form-group has-success col-md-4" id="addSizeQuantityElement">'
			+ '<br /> <br /> <a href="#" onclick="addSizeQuantityElement()"><i class="glyphicon glyphicon-plus-sign"></i></a>'
			+ '<br /> <br />' + '</div>';
	// alert('new elements ' + addButtonHtml);
	$('#sizeQuantity').append(addButtonHtml);
}
function createRequest() {
	try {
		request = new XMLHttpRequest();
	} catch (tryMS) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (otherMS) {
			try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {
				request = null;
			}
		}
	}
	return request;
}
function isEmpty(str) {
	return (!str || 0 === str.length);
}
$(document).ready(function() {
	$("#tax").blur(calculateTaxInclusiveSellingPrice);
	$("#sellingPrice").blur(calculateTaxInclusiveSellingPrice);
	// $("#costPrice").blur(calculateDiscount);
	$("#sellinPriceTaxInclusive").blur(calculateDiscount);
	// $("#pincode").blur(validatePostcode);

});

function calculateTaxInclusiveSellingPrice() {
	// $('#topCategory')
	var tax = $("#tax").val();
	var sellingPrice = $("#sellingPrice").val();

	if (!isEmpty(sellingPrice) && !isEmpty(tax)) {
		var taxInclusiveSellingPrice = Math.round((parseFloat(sellingPrice))
				+ (parseFloat((sellingPrice * tax) / 100)));
		$("#sellinPriceTaxInclusive").val(taxInclusiveSellingPrice);
	} else {
		$("#sellinPriceTaxInclusive").val('');
	}
	calculateDiscount();
}
function calculateDiscount() {
	var costPrice = $("#costPrice").val();
	var sellingPrice = $("#sellinPriceTaxInclusive").val();
	if (!isEmpty(costPrice) && !isEmpty(sellingPrice)) {
		if (parseFloat(costPrice) > parseFloat(sellingPrice)) {
			var percentage = Math
					.round((((parseFloat(costPrice) - parseFloat(sellingPrice)) / parseFloat(costPrice)) * 100));

			$("#discount").val(percentage + '%');
		} else {
			$("#discount").val('no Discount');
		}

	}

}
$(function() {
	calculateTaxInclusiveSellingPrice();

});