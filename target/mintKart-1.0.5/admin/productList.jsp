<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.ecommerce.data.MenuUtil"%>
<%@page import="java.util.HashMap"%>
<jsp:include page="/admin/include/header.jsp" />

<div id="content" class="col-lg-10 col-sm-10">
	<div>
		<ul class="breadcrumb">
			<li><a href="index.jsp">Home</a></li>
			<li><a href="#">ProductList</a></li>
		</ul>
	</div>
	<%
		HashMap<Integer, String> subCategoryList = MenuUtil.getSubcategoryList();

		if (subCategoryList == null) {
	%>
	<div>
		<ul class="breadcrumb">
			<li>No Products</li>
		</ul>
	</div>
	<%
		} else {
	%>
	<div class=" row">
		<%
			for (Map.Entry<Integer, String> entry : subCategoryList.entrySet()) {
					int id = entry.getKey();
					String subCategoryName = entry.getValue();
		%>


		<div class="col-md-3 col-sm-3 col-xs-6">
			<a data-toggle="tooltip" 
				class="well top-block" href="displayproductList?subCategoryId=<%=id %>"> <i
				class="glyphicon glyphicon-user blue"></i>
				<div><%=subCategoryName %></div>
			</a>
		</div>


		<%
			}
		%>
	</div>
	<%
		}
	%>

	<jsp:include page="/admin/include/footer.jsp" />