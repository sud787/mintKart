<%@ include file="/include/header.jsp"%>
<%@ page
	import="com.ecommerce.business.Product,java.util.StringTokenizer,java.util.HashMap,java.util.Map"%>
<script defer src="js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="css/flexslider.css" type="text/css"
	media="screen" />
<script>
	// Can also be used with $(document).ready()
	$(window).load(function() {
		$('.flexslider').flexslider({
			animation : "slide",
			controlNav : "thumbnails"
		});
	});
</script>
<script src="js/displayProduct.js"></script>
<!--flex slider-->

<script src="js/imagezoom.js"></script>
<!-- //js -->
<div class="products" style="background: white; color: black;">
	<%
		Product product = (Product) request.getAttribute("product");
	%>
	<div class="container">
		<div class="single-page">
			<div class="single-page-row" id="detail-21">
				<div class="col-md-6 single-top-left">
					<div class="flexslider">
						<ul class="slides">
							<%
								ArrayList<String> imageURLs = product.getImagesURLS();
							%>
							<li data-thumb="<%=imageURLs.get(0)%>" style="width: 50px;">
								<div class="thumb-image detail_images">

									<img src="<%=imageURLs.get(0)%>" data-imagezoom="true"
										class="img-responsive" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal">
								</div>
							</li>
							<li data-thumb="<%=imageURLs.get(1)%>">
								<div class="thumb-image">
									<img src="<%=imageURLs.get(1)%>" data-imagezoom="true"
										class="img-responsive" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal">
								</div>
							</li>
							<li data-thumb="<%=imageURLs.get(2)%>">
								<div class="thumb-image">
									<img src="<%=imageURLs.get(2)%>" data-imagezoom="true"
										class="img-responsive" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal">
								</div>
							</li>
						</ul>
					</div>
				</div>
				<form action="cartSummary" method="post">
					<div class="col-md-6 single-top-right">
						<h3
							style="border-bottom: 1px solid #eaeaea; padding-bottom: 10px;"
							class="item_name"><%=product.getName()%></h3>

						<p>Processing Time: Item will be shipped out within 2-3
							working days.</p>
						<div class="single-price">
							<ul>
								<!--
							if product has discount,then compute the new price otherwise hide discount and new price
							 -->
								<li style="font-weight: bold;" class="product-price"><%=(int) product.getSellingPrice()%></li>
								<%
									if (product.getDiscount() > 0) {
								%>
								<li class="product-price"><del><%=product.getCostPrice()%></del></li>
								<li><span class="w3off"><%=product.getDiscount()%>%
										OFF</span></li>
								<%
									}
								%>
							</ul>
						</div>
						<label>Color :</label><span
							style="margin-left:10px; color: <%=product.getColour()%>;text-transform: capitalize;"><%=product.getColour()%></span><br />
						<%
							HashMap<Integer, Integer> sizeQuantityMap = (HashMap<Integer, Integer>) request.getAttribute("sizes");

							if (sizeQuantityMap.size() > 0) {

								if (sizeQuantityMap.get(0) == null) {
						%>
						<div>
							<br /> <label>Size :</label> <select id="selectedSize"
								name="selectedSize" onchange="changeQuantity()">
								<%
									for (Map.Entry<Integer, Integer> entry : sizeQuantityMap.entrySet()) {
												int key = entry.getKey();
												int value = entry.getValue();
												//for (Integer key : sizeQuantityMap.keySet()) {
												//Integer value = sizeQuantityMap.get(key);
								%>
								<option data-value="<%=value%>" value="<%=key%>"><%=key%></option>
								<%
									}
								%>
							</select> <label style="margin-left: 10px;">Quantity :</label> <select
								id="selectedQuantity" name="selectedQuantity">

							</select>
							</div>

							<%
							} else {
									int quantity = sizeQuantityMap.get(0);
									%>
									<div>
									<br />
									<label style="margin-left: 10px;">Quantity :</label> <select
											id="selectedQuantitySingle" name="selectedQuantity">
									<% for(int i=1;i<=quantity && i<=5;i++){%>

							           <option value="<%=i%>"><%=i%></option>


							<%}%>
							</select>
							</div>
							<%
									}
								
						%>

						
						<%
							} else {
						%>
						<p style="color: red">Product is currently out of stock</p>
						<%
							}
						%>
						<br />
						<%
							HashMap<String, String> availabeColours = (HashMap<String, String>) request.getAttribute("availabeColours");
							if (availabeColours != null && availabeColours.size() > 0) {
						%>
						<label>Available Colors :</label> <br />
						<%
							for (Map.Entry<String, String> entry : availabeColours.entrySet()) {
									String key = entry.getKey();
									String value = entry.getValue();
						%>

						<a href="displaySingleProduct?productId=<%=value%>"
							class="availableColors" style="background:<%=key%> "
							target="_blank"></a>

						<%
							}
							}
						%>
						<div class="discriptionUL">
							<ul style="list-style-type: circle;">
								<%
									String productDescription = product.getProductShortDescription();
									StringTokenizer st = new StringTokenizer(productDescription, ",");

									while (st.hasMoreTokens()) {
								%>
								<li style="display: block;"><%=st.nextToken()%></li>
								<%
									}
									//split the string basedon comas's and dots.
								%>
							</ul>
						</div>

						<input type="hidden" name="productId" value="<%=product.getId()%>">
						<%
							session.setAttribute("noRefesh", "noRefesh");
											if (sizeQuantityMap.size() > 0) {
						%>
						<button type="submit" class="w3ls-cart">
							<i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart
						</button>
						<%
							}
						%>
					
				</form>

			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<!-- collapse-tabs -->
	<div class="collpse tabs">
		<h3 class="w3ls-title">About this item</h3>
		<div class="panel-group collpse" id="accordion" role="tablist"
			aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a class="pa_italic" role="button" data-toggle="collapse"
							data-parent="#accordion" href="#collapseOne" aria-expanded="true"
							aria-controls="collapseOne"> <i
							class="fa fa-file-text-o fa-icon" aria-hidden="true"></i>
							Description <span class="fa fa-angle-down fa-arrow"
							aria-hidden="true"></span> <i class="fa fa-angle-up fa-arrow"
							aria-hidden="true"></i>
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in"
					role="tabpanel" aria-labelledby="headingOne">
					<div class="panel-body"><%=product.getProductLongDescription()%></div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingTwo">
					<h4 class="panel-title">
						<a class="collapsed pa_italic" role="button"
							data-toggle="collapse" data-parent="#accordion"
							href="#collapseTwo" aria-expanded="false"
							aria-controls="collapseTwo"> <i
							class="fa fa-info-circle fa-icon" aria-hidden="true"></i>
							additional information <span class="fa fa-angle-down fa-arrow"
							aria-hidden="true"></span> <i class="fa fa-angle-up fa-arrow"
							aria-hidden="true"></i>
						</a>
					</h4>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse"
					role="tabpanel" aria-labelledby="headingTwo">
					<div class="panel-body"><%=product.getProductAdditionalInformation()%></div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a class="collapsed pa_italic" role="button"
							data-toggle="collapse" data-parent="#accordion"
							href="#collapseThree" aria-expanded="false"
							aria-controls="collapseThree"> <i
							class="fa fa-check-square-o fa-icon" aria-hidden="true"></i>
							reviews <span class="fa fa-angle-down fa-arrow"
							aria-hidden="true"></span> <i class="fa fa-angle-up fa-arrow"
							aria-hidden="true"></i>
						</a>
					</h4>
				</div>
				<div id="collapseThree" class="panel-collapse collapse"
					role="tabpanel" aria-labelledby="headingThree">
					<div class="panel-body">
						<%
							ArrayList<String> productReview = (ArrayList) request.getAttribute("productReview");
							if (productReview == null) {
						%>
						No Reviews.
						<%
							} else {
								Iterator<String> reviewItr = productReview.iterator();
								while (reviewItr.hasNext()) {
						%>
						<%=reviewItr.next()%>
						<%
							if (reviewItr.hasNext()) {
						%>
						<hr />
						<%
							}
								}
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br /> <br />
	<h3 class="w3ls-title">Related Products</h3>
	<br /> <br />
	<div class="col-md-3 top-product-grids tp2" data-wow-delay=".5s">
		<a href="displayProduct.jsp"><div class="product-img">
				<img src="images/tp1.jpg" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal" />
			</div></a> <i class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star gray-star" aria-hidden="true"></i> <i
			class="fa fa-star gray-star" aria-hidden="true"></i>
		<h4>Formal shoes</h4>
		<h5 class="product-price">220.00</h5>
	</div>
	<div class="col-md-3 top-product-grids tp2" data-wow-delay=".5s">
		<a href="displayProduct.jsp"><div class="product-img">
				<img src="images/tp1.jpg" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal" />
				<!-- <div class="p-mask">
					<form action="#" method="post">
						<input type="hidden" name="cmd" value="_cart" /> <input
							type="hidden" name="add" value="1" /> <input type="hidden"
							name="w3ls1_item" value="Formal shoes" /> <input type="hidden"
							name="amount" value="220.00" />
						<button type="submit" class="w3ls-cart pw3ls-cart">
							<i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart
						</button>
					</form>
				</div> -->
			</div></a> <i class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star gray-star" aria-hidden="true"></i> <i
			class="fa fa-star gray-star" aria-hidden="true"></i>
		<h4>Formal shoes</h4>
		<h5 class="product-price">220.00</h5>
	</div>
	<div class="col-md-3 top-product-grids tp2" data-wow-delay=".5s">
		<a href="displayProduct.jsp"><div class="product-img">
				<img src="images/tp1.jpg" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal" />
				<!--
				<div class="p-mask">
					<form action="#" method="post">
						<input type="hidden" name="cmd" value="_cart" /> <input
							type="hidden" name="add" value="1" /> <input type="hidden"
							name="w3ls1_item" value="Formal shoes" /> <input type="hidden"
							name="amount" value="220.00" />
						<button type="submit" class="w3ls-cart pw3ls-cart">
							<i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart
						</button>
					</form>
				</div>-->
			</div></a> <i class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star gray-star" aria-hidden="true"></i> <i
			class="fa fa-star gray-star" aria-hidden="true"></i>
		<h4>Formal shoes</h4>
		<h5 class="product-price">220.00</h5>
	</div>
	<div class="col-md-3 top-product-grids tp2" data-wow-delay=".5s">
		<a href="displayProduct.jsp"><div class="product-img">
				<img src="images/tp1.jpg" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal" />
				<!--
				<div class="p-mask">
					<form action="#" method="post">
						<input type="hidden" name="cmd" value="_cart" /> <input
							type="hidden" name="add" value="1" /> <input type="hidden"
							name="w3ls1_item" value="Formal shoes" /> <input type="hidden"
							name="amount" value="220.00" />
						<button type="submit" class="w3ls-cart pw3ls-cart">
							<i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart
						</button>
					</form>
				</div>-->
			</div></a> <i class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star yellow-star" aria-hidden="true"></i> <i
			class="fa fa-star gray-star" aria-hidden="true"></i> <i
			class="fa fa-star gray-star" aria-hidden="true"></i>
		<h4>Formal shoes</h4>
		<h5 class="product-price">220.00</h5>
	</div>
	<%-- <div class="col-md-8 col-sm-8 women-dresses">
		<%
			//	ArrayList<Product> productList = (ArrayList<Product>) request.getAttribute("productList");

			//	if (productList != null) {
			//	Iterator<Product> itrNew = productList.iterator();
			//	while (itrNew.hasNext()) {
			//		Product product = itrNew.next();
		%>
		<div class="col-md-4 women-grids wp1 animated wow slideInUp"
			data-wow-delay=".5s">
			<a href="displaySingleProduct?productId=<%=product.getCode()%>"><div
					class="product-img">
					<img src="<%=product.getImageURL()%>" alt="" />
					<!-- <div class="p-mask">
							<form action="#" method="post">
								<input type="hidden" name="cmd" value="_cart" /> <input
									type="hidden" name="add" value="1" /> <input type="hidden"
									name="w3ls1_item" value="Casual shirt" /> <input type="hidden"
									name="amount" value="50.00" />
								<button type="submit" class="w3ls-cart pw3ls-cart">
									<i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart
								</button>
							</form>
						</div> -->
				</div></a> <i class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star gray-star" aria-hidden="true"></i>
			<h4><%=product.getName()%></h4>
			<h5><%=product.getPrice()%></h5>
		</div>
		<div class="col-md-4 women-grids wp1 animated wow slideInUp"
			data-wow-delay=".5s">
			<a href="displaySingleProduct?productId=<%=product.getCode()%>"><div
					class="product-img">
					<img src="<%=product.getImageURL()%>" alt="" />
					<!-- <div class="p-mask">
							<form action="#" method="post">
								<input type="hidden" name="cmd" value="_cart" /> <input
									type="hidden" name="add" value="1" /> <input type="hidden"
									name="w3ls1_item" value="Casual shirt" /> <input type="hidden"
									name="amount" value="50.00" />
								<button type="submit" class="w3ls-cart pw3ls-cart">
									<i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart
								</button>
							</form>
						</div> -->
				</div></a> <i class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star gray-star" aria-hidden="true"></i>
			<h4><%=product.getName()%></h4>
			<h5><%=product.getPrice()%></h5>
		</div>
		<div class="col-md-4 women-grids wp1 animated wow slideInUp"
			data-wow-delay=".5s">
			<a href="displaySingleProduct?productId=<%=product.getCode()%>"><div
					class="product-img">
					<img src="<%=product.getImageURL()%>" alt="" />
					<!-- <div class="p-mask">
							<form action="#" method="post">
								<input type="hidden" name="cmd" value="_cart" /> <input
									type="hidden" name="add" value="1" /> <input type="hidden"
									name="w3ls1_item" value="Casual shirt" /> <input type="hidden"
									name="amount" value="50.00" />
								<button type="submit" class="w3ls-cart pw3ls-cart">
									<i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart
								</button>
							</form>
						</div> -->
				</div></a> <i class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star gray-star" aria-hidden="true"></i>
			<h4><%=product.getName()%></h4>
			<h5><%=product.getPrice()%></h5>
		</div>
		<div class="col-md-4 women-grids wp1 animated wow slideInUp"
			data-wow-delay=".5s" style="clear: both; float: right;">
			<a href="displaySingleProduct?productId=<%=product.getCode()%>"><div
					class="product-img">
					<img src="<%=product.getImageURL()%>" alt="" />
				</div></a> <i class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star yellow-star" aria-hidden="true"></i> <i
				class="fa fa-star gray-star" aria-hidden="true"></i>
			<h4><%=product.getName()%></h4>
			<h5><%=product.getPrice()%></h5>
		</div>

		<%
			//}
			//	}
		%>
		<div class="clearfix"></div>
	</div> --%>
	<!-- //collapse -->
</div>
</div>
<jsp:include page="/include/footer.jsp" />
