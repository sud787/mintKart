/**
 * 
 */
$('.product-reviewSubmit a').click(function() {
	submitReview(this);
});
// Validating Empty Field
function submitReview(reviewButton) {
	var orderRow=$(reviewButton).parent().parent().parent().parent().parent();
	var orderpopupRow = $(reviewButton).parent().parent();
	var orderId = orderpopupRow.children('.orderId').val();
	var productId = orderpopupRow.children('.productId').val();
	var reviewMessage = orderpopupRow.children('.reviewMessage').val();
	
	// orderId = $(reviewButton).siblings('.orderId').val()
	if (reviewMessage == "") {
		alert("Fill All Fields !");
		return false;
	} else {
		request = createRequest();

		request.open("POST", "productReviewSubmit?productId=" + productId
				+ "&orderId=" + orderId, true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.send("reviewMessage="+reviewMessage);
		$(orderRow.children('.reviewButton')).remove();
		div_hide();
	}
}


// Function To Display Popup
function div_show() {
	document.getElementById('reviewPopUP').style.display = "block";
}
// Function to Hide Popup
function div_hide() {
	document.getElementById('reviewPopUP').style.display = "none";
}

function createRequest() {
	try {
		request = new XMLHttpRequest();
	} catch (tryMS) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (otherMS) {
			try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {
				request = null;
			}
		}
	}
	return request;
}
