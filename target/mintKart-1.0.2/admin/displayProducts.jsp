<%@page import="java.util.Iterator"%>
<%@page import="com.ecommerce.business.Product"%>
<%@page import="java.util.ArrayList"%>
<jsp:include page="/admin/include/header.jsp" />
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" />
<div id="content" class="col-lg-10 col-sm-10">
	<div class="content" style="background: white;">
		<%
			ArrayList<Product> productList = (ArrayList<Product>) request.getAttribute("productList");

			if (productList != null) {
				Iterator<Product> itrNew = productList.iterator();
				while (itrNew.hasNext()) {
					Product product = itrNew.next();
		%>
		<div class="col-md-4 women-grids wp1 animated wow slideInUp"
			data-wow-delay=".5s">
			<a href="displayProductHandler?productId=<%=product.getId()%>"><div
					class="product-img"
					style="position: relative; box-shadow: 0 0 1px #bbb; padding: 10px; width: 100%; vertical-align: middle; border: 0; box-sizing: border-box;">
					<img width="193" height="258" src="<%=product.getImageURL()%>"
						alt="" />

				</div></a>
			<h4><%=product.getName()%></h4>
			<h5 class="product-price"><%=product.getSellingPrice()%></h5>
			<br />

		</div>
		<%
			}
			}
		%>
		<br />



		<div class="clearfix"></div>
	</div>

	<jsp:include page="/admin/include/footer.jsp" />