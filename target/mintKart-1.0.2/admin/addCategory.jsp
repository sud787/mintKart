<%@page import="java.util.Iterator"%>
<%@page import="com.ecommerce.data.MenuUtil"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<jsp:include page="/admin/include/header.jsp" />
<div id="content" class="col-lg-10 col-sm-10">
	<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-edit"></i> Add Top Category
					</h2>
				</div>
				<%
					ArrayList<String> topCategory = MenuUtil.getTopCategory();
					Iterator<String> itr = topCategory.iterator();
				%>
				<div class="form-group has-success col-md-4">
					<label class="control-label" for="inputError1">Top Category</label>
					<div class="controls">
						<select id="topCategory" class="topCategory"
							onchange="changeCategory()">
							<%
								while (itr.hasNext()) {
									String value = itr.next();
							%>
							<option data-value="<%=value%>"><%=value%></option>
							<%
								}
							%>
						</select>
					</div>
				</div>
				<div class="form-group has-success col-md-4">
					<label class="control-label" for="selectError"> Existing Category </label>
					<div class="controls">
						<select id="category" id="category" onchange="getSubCategory()">

						</select>
					</div>
					
				</div>
				<div class="form-group has-success col-md-4">
						<label class="control-label" for="inputSuccess1">New Category
							 </label> <input type="text" class="form-control" id="newCategory" class="newCategory"
							name="newCategory" required>

					</div>
				
			</div>
		</div>
		<div class="box-content">
		
		<button type="submit" class="btn btn-default" onclick="updateCategory()">Submit</button>
		</div>
	</div>
</div>
<script src="js/addProduct.js"></script>
<script src="js/menuAddition.js"></script>
<jsp:include page="/admin/include/footer.jsp" />