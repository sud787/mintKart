<%@page import="com.ecommerce.data.PayuMoneyUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="com.ecommerce.data.CookieUtil,com.ecommerce.business.User,com.ecommerce.data.UserDB,com.ecommerce.data.MenuUtil,java.util.ArrayList,java.util.Iterator"%>
<%@page
	import="java.util.ArrayList,com.ecommerce.business.Product,com.ecommerce.business.LineItem,com.ecommerce.business.Cart,com.ecommerce.business.Address"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/Util/logo1.jpg" type="image/png">
<title>Mint Kart-An online shopping store in Manipur| E-commerce store in Manipur  </title>
<meta name="description" content="Online Shopping in Manipur. E-commerce in Manipur.Manipuri dress online shopping">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal" />


<!-- css -->
		<link href="${pageContext.request.contextPath}/css/orderProcessing.css"
			rel="stylesheet" type="text/css" media="all" />
			<link href="${pageContext.request.contextPath}/css/styleCheckout.css"
			rel="stylesheet" type="text/css" media="all" />
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
			media="all" />
		<link rel="stylesheet" href="css/style.css" type="text/css"
			media="all" />
		<link rel="stylesheet" href="css/loader.css" type="text/css"
			media="all" />
		<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"
			media="all" /> <!--// css --> <!-- font -->
		<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro"
			rel="stylesheet">
		<link
			href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
			rel='stylesheet' type='text/css'> <!-- //font --> <script
			src="js/jquery-1.11.1.min.js"></script> <script src="js/bootstrap.js"></script>
		<script src="js/finalizeOrder.js"></script>
</head>

<body>
	<div class="header-top-w3layouts">
		<div class="container">
			<div class="col-md-6 logo-w3">
				<a href="index.jsp"><img src="images/Util/logo1.jpg" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal " />
					<h1>
						MINT<span>KART</span>
					</h1></a>
			</div>
		</div>
	</div>

		<!-- another version - flat style with animated hover effect -->
		<div class="breadcrumb flat">
			<a>LOGIN</a> <a>SHIPPING</a> <a>PAYEMENT</a> <a class="active">REVIEW</a>
		</div>
		<%
			Address address = (Address) session.getAttribute("shippingAddress");
			String paymentMode = (String) session.getAttribute("payementMethod");
			if (paymentMode.equals("payumoney")) {
				response.setHeader("Authorization", "Basic " + "lFN9jDZI2mhi6YIulb/f9CymhPlnvVARLV6CjXyK8z4=");
				PayuMoneyUtil payuAcess = (PayuMoneyUtil) request.getAttribute("payuMoney");
		%><form action="<%=payuAcess.getAction1()%>" method="post"
			name="payuForm">
			<input type="hidden" name="key"
				value="<%=PayuMoneyUtil.getMerchantKey()%>" /> <input type="hidden"
				name="hash" value="<%=payuAcess.getHashString()%>" /> <input
				type="hidden" name="txnid" value="<%=payuAcess.getTxnid()%>" /> <input
				type="hidden" name="service_provider" value="payu_paisa" /> <input
				type="hidden" name="amount" value="<%=payuAcess.getAmount()%>" />
			<%--  --%>
			<input type="hidden" name="firstname" value="<%=address.getName()%>" />
			<input type="hidden" name="email" value="<%=address.getEmail()%>" />
			<input type="hidden" name="phone" value="<%=address.getNumber()%>" />
			<input type="hidden" name="productinfo" value="online payment" /> <input
				type="hidden" name="surl" value="<%=PayuMoneyUtil.getSurl()%>" /> <input
				type="hidden" name="furl" value="<%=PayuMoneyUtil.getFurl()%>" />
			<%
				} else {
			%>
			<form action="placeOrder" method="post" id="placeOrderForm">
				<%
					}
				%>
				<div class="shopping-cart">
					<br />
					<h3>Finalize Order</h3>
					<br /> <br />
					<%
						session.setAttribute("sendOrderConfirmationMail", "yes");
						Cart cart = (Cart) session.getAttribute("cart");
					%>
					<div class="embeddedCart">
						<div class="row">
							<div class="col-sm-4" style="font-weight: bold;padding-top:10px;padding-left: 25px;">Shipping Address</div>
							<div class="col-sm-4" style="font-weight: bold;padding-top:10px;">Payment Method</div>
							<div class="col-sm-4" style="font-weight: bold;padding-top:10px;">Order Summary</div>
						</div>
						<br />
						 <div class="row">
							<div class="col-sm-4">
								<ul class="shippingAddressDisplay">
									<li><%=address.getName()%></li>
									<li><%=address.getAddress()%></li>
									<li><%=address.getCity() + " ," + address.getState()%></li>
									<li><%=address.getPincode() + ", P.H:" + address.getNumber()%></li>
								</ul>
							</div>
							<div class="col-sm-4">
								<ul class="payementSelected">
									<li><%=session.getAttribute("payementMethod")%></li>
								</ul>
							</div>

						</div> 
						<!-- <div class="column-labels" style="width: 100%">
							<label class="productCart-ShippingAddress">Shipping
								Address</label> <label class="productCart-Payment">Payment
								Method</label> <label class="productCart-Order">Order Summary</label>
						</div> -->
						<%-- <div class="column-values" style="width: 100%">
							<ul class="shippingAddressDisplay">
								<li><%=address.getName()%></li>
								<li><%=address.getAddress()%></li>
								<li><%=address.getCity() + " ," + address.getState()%></li>
								<li><%=address.getPincode() + ", P.H:" + address.getNumber()%></li>
							</ul>

							<ul class="payementSelected">
								<li><%=session.getAttribute("payementMethod")%></li>
							</ul>
						</div> --%>
						<%-- <ul class="orderSummeryDisplay">
					<li>Item(s) Subtotal: <%=order.getProductCost()%></li>
					<li>Shipping:<%=order.getShippementCharges()%></li>
					<li>Tax:<%=order.getTax()%></li>
					<li>Grand Total:<%=order.getTotalCost()%></li>
				</ul> --%>




					</div>
					<br />
					<div class="column-labels" style="margin-bottom: -5px;">
						<label class="productCart-image">Image</label> <label
							class="productCart-details">Product</label> <label
							class="productCart-price">Price</label> <label
							class="productCart-quantity">Qty</label> <label
							class="productCart-size">Size</label> <label
							class="productCart-line-price">Total</label>

					</div>
					<hr />

					<%
						ArrayList<LineItem> items = cart.getItems();
						Iterator<LineItem> itrCartItems = items.iterator();
						double totalPrice = 0;
						while (itrCartItems.hasNext()) {
							LineItem item = itrCartItems.next();
					%>
					<div class="productCart">
						<input class="hidden_id" type="hidden"
							value="<%=item.getProduct().getId()%>" />
						<div class="productCart-image">
							<a
								href="displaySingleProduct?productId=<%=item.getProduct().getId()%>"><img
								src="<%=item.getProduct().getImageURL()%>" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal"></a>
						</div>
						<div class="productCart-details">
							<a
								href="displaySingleProduct?productId=<%=item.getProduct().getId()%>"
								target="_blank">
								<div class="productCart-title"
									style="text-decoration: underline;"><%=item.getProduct().getName()%></div>
							</a>
							<p class="productCart-description"><%=item.getProduct().getProductShortDescription()%></p>

							<!-- <div class="productCart-removal">
						<button class="remove-productCart">Remove</button>
					</div> -->
						</div>
						<div class="productCart-price">
							<%=item.getProduct().getSellingPrice()%></div>
						<div class="productCart-quantity">
							<input type="number" value="<%=item.getQuantity()%>" min="1"
								readonly>
						</div>
						<div class="productCart-size">
							<%
								if (item.getSize() > 0) {
							%>
							<%=item.getSize()%>
							<%
								} else {
							%>
							-
							<%
								}
							%>
						</div>
						<div></div>
						<div class="productCart-line-price">
							<%=item.getProduct().getSellingPrice() * item.getQuantity()%></div>
					</div>

					<%
						totalPrice += item.getProduct().getSellingPrice() * item.getQuantity();
						}
					%>

					<div class="totals">
						<div class="totals-item">
							<label>Subtotal</label>
							<div class="totals-value" id="cart-subtotal">
								<%=totalPrice%></div>
						</div>
						<div class="totals-item">
							<label>Shipping</label>
							<div class="totals-value" id="cart-shipping">15.00</div>
						</div>
						<div class="totals-item totals-item-total">
							<label>Grand Total</label>
							<div class="totals-value" id="cart-total">
								<%=totalPrice + 15.00%></div>
						</div>
					</div>
					<button class="checkout" type="submit" value="Submit"
						onclick="showLoader()">Place Order</button>

				</div>
			</form>
			<!-- <a href="placeOrder"> -->
			<!-- 	<button class="checkout"  type ="submit" form="placeOrderForm" value="Submit" onclick="showLoader()">Place Order</button> -->
			<!-- </a>
 -->
	<div id="loader" class="loading">Loading&#8230;</div>
</body>
</html>