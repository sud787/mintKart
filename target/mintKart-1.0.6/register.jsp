<jsp:include page="/include/header.jsp" />

<script>
function validateSubmission(){
	var isPhoneSet=$("#divCheckPhoneNumber").html();
	var isEmailSet= $("#divCheckEmail").html();
	var isPasswordMatch=$("#divCheckPasswordUnMatch").html();
	
	if(isEmpty(isPhoneSet) && isEmpty(isEmailSet) && isEmpty(isPasswordMatch)){
		return true;
	}else{
		return false;
	}
}
function isEmpty(str) {
	return (!str || 0 === str.length);
}
	function validateEmail() {
		//check email with co
		var email = $("#Email").val(); 
		if(email==null || email == ""){
			$("#divCheckEmail").html("");
		}else{
		 var atpos = email.indexOf('@');
		    var dotpos = email.lastIndexOf('.');
		    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
		    	 $("#divCheckEmail").html("Email id is not valid");
		    //	 return false;
		    }  else{
		    	$("#divCheckEmail").html("");
		    //	return true;
		    }
		}    
		
	}
	function validatePhoneNumber()  
	{  
		var phone = $("#PhoneNumber").val(); 
		//alert('phone'+phone);
		if(phone==null || phone==""){
			$("#divCheckPhoneNumber").html("");
		}else{
			var phoneno = /^\d{10}$/;  
		  		if(phone.match(phoneno)){
			  		$("#divCheckPhoneNumber").html("");
			//  return true;
		  		}else{  
			  $("#divCheckPhoneNumber").html("Mobile number is not valid");
			 // return false;
		  		} 
			}		  		
	}  
	function checkPasswordMatch() {
	    var password = $("#Password").val();
	    var confirmPassword = $("#ConfirmPassword").val();
	    if((password==null || password=="") || (confirmPassword==null || confirmPassword=="") ){
	    	$("#divCheckPasswordMatch").html("");
	    	$("#divCheckPasswordUnMatch").html("");
	    }else{
		    if (password != confirmPassword){
		        $("#divCheckPasswordUnMatch").html("Passwords do not match!");
		        $("#divCheckPasswordMatch").html("");
		    	return false;
		    }
		    else{
		        $("#divCheckPasswordMatch").html("Passwords match.");
		        $("#divCheckPasswordUnMatch").html("");
		    	return true;
		    }  
	    }   
	}

	$(document).ready(function () {
	   $("#Password, #ConfirmPassword").keyup(checkPasswordMatch);
	   $(" #Email").blur(validateEmail);
	   $(" #PhoneNumber").blur(validatePhoneNumber);
	   
	});

</script>
<div class="login">

	<div class="main-agileits">
		<div class="form-w3agile">
			<h3 id='register'>Register</h3>
			<!-- check for already registerd user -->
			<%
				String source = request.getParameter("source");
				if (source != null) {
					session.setAttribute("source", source);
				}
				if (request.getAttribute("error") != null
						&& !request.getAttribute("error").equals("")) {
			%>
			<div id="scoped-content">
				<style type="text/css" scoped>
h6 {
	color: red;
	margin-top: -40px;
	margin-bottom: 10px;
}
</style>
			</div>

			<h6><%=request.getAttribute("error")%>
			</h6>
			<%
				}
			%>
			<form action="registerUser" method="post"
				onsubmit="return validateSubmission();">
				<div class="key">
					<i class="fa fa-user" aria-hidden="true"></i> <input type="text"
						name="FirstName" required="" placeholder="First Name">
					<div class="clearfix"></div>
				</div>
				<div class="key">
					<i class="fa fa-user" aria-hidden="true"></i> <input type="text"
						name="LastName" required="" placeholder="Last Name">
					<div class="clearfix"></div>
				</div>
				<div class="key">
					<i class="fa fa-envelope" aria-hidden="true"></i> <input
						type="text" name="Email" required="" placeholder="Email"
						id="Email">
					<div class="clearfix"></div>
				</div>
				<div class="registrationFormAlert" id="divCheckEmail"
					style="color: red;"></div>
				<div class="key">
					<i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> <input
						type="text" name="PhoneNumber" id="PhoneNumber" required=""
						placeholder="Mobile Number">
					<div class="clearfix"></div>
				</div>
				<div class="registrationFormAlert" id="divCheckPhoneNumber"
					style="color: red;"></div>
				<div class="key">
					<i class="fa fa-lock" aria-hidden="true"></i> <input
						type="password" id="Password" name="Password" required=""
						placeholder="Password">
					<div class="clearfix"></div>
				</div>
				<div class="registrationFormAlert" id="divCheckPasswordMatch"
					style="color: red;"></div>
				<div class="registrationFormAlert" id="divCheckPasswordUnMatch"
					style="color: red;"></div>
				<div class="key">
					<i class="fa fa-lock" aria-hidden="true"></i> <input
						type="password" id="ConfirmPassword" name="ConfirmPassword"
						required="" placeholder="Confirm Password">
					<div class="clearfix"></div>
				</div>
		</div>

		<input type="submit" value="Register" id="submit">
		</form>
	</div>
</div>
</div>
<jsp:include page="/include/footer.jsp" />