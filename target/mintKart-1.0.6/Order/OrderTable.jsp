<jsp:include page="/include/header.jsp" />
<%@ page
	import="java.util.ArrayList,java.util.Iterator,com.ecommerce.business.Order,java.text.SimpleDateFormat,com.ecommerce.business.LineItem"%>
<div id="scoped-content">
	<style type="text/css" scoped>
@import "css/styleCheckout.css";
</style>
	<div class="shopping-cart">
		<br />
		<h3>My Orders</h3>

		<%
			ArrayList<Order> orderList = (ArrayList<Order>) request.getAttribute("orderList");
			if (orderList.size() > 0) {
				Iterator<Order> itr = orderList.iterator();
				while (itr.hasNext()) {
					Order order = itr.next();
		%>
		<div class="embeddedCart">
			<div class="headerInfo">
			<div class="row">
					<div class="col-sm-20" style="color: #aaa;padding-left: 25px;">ORDER PLACED</div>
					<div class="col-sm-20" style="color: #aaa;">TOTAL</div>
					<div class="col-sm-20" style="color: #aaa;">
						ORDER #<%=order.getOrderId()%></div>
					<div class="col-sm-20" style="color: #aaa;">ORDER STATUS</div>
					<div class="col-sm-20" style="color: #aaa;"> <a target="_blank"
						href="invoiceGeneration?orderId=<%=order.getOrderId()%>">Invoice</a></div>
				</div>
				<%
						SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					%>
				<div class="row">
					<div class="col-sm-20" style="padding-left: 25px;"><%=formatter.format(order.getOrderDate())%></div>
					<div class="col-sm-20 productOrder-price"><%=order.getTotalCost()%></div>
					<div class="col-sm-20"><a
						href="orderDetailsServlet?orderId=<%=order.getOrderId()%>">Order
							Details</a></div>
					<div class="col-sm-20"><%=order.getOderStatus()%></div>
				</div>
			</div>
			<br />
			<%
				ArrayList<LineItem> items = order.getItems();
						Iterator<LineItem> itrOrderItems = items.iterator();
						//double totalPrice = 0;
						while (itrOrderItems.hasNext()) {
							LineItem item = itrOrderItems.next();
			%>
			<div class="productCart">
				<input class="hidden_Product_id" type="hidden"
					value="<%=item.getProduct().getId()%>"> <input
					class="hidden_Order_id" type="hidden"
					value="<%=order.getOrderId()%>">
				<div class="productCart-image">
					<a
						href="displaySingleProduct?productId=<%=item.getProduct().getId()%>"><img
						src="<%=item.getProduct().getImageURL()%>" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal"></a>
				</div>
				<div class="productCart-details">
					<a
						href="displaySingleProduct?productId=<%=item.getProduct().getId()%>"
						target="_blank">
						<div class="productCart-title" style="text-decoration: underline;"><%=item.getProduct().getName()%></div>
					</a>
					<p class="productCart-description"><%=item.getProduct().getProductShortDescription()%></p>
					<%
						String isOrderCancelled = item.getProduct().getIsOrderCancelled();

									if (isOrderCancelled != null && !isOrderCancelled.equals("")) {
					%>
					<p class="productCart-quantityMessage"
						style="color: red; display: inline;">
						<i class="glyphicon glyphicon-remove"></i>Product Cancelled
					</p>
					<%
						} else if (!order.getOderStatus().equalsIgnoreCase("Order Placed")) {
										if (order.getOderStatus().equalsIgnoreCase("Order Delivered")
												|| order.getOderStatus().equalsIgnoreCase("Shipped")) {
					%>

					<p class="productCart-quantityMessage"
						style="color: Green; font: bold; display: inline;">
						<i class="glyphicon glyphicon-ok"></i>
						<%=order.getOderStatus()%>
					</p>
					<br />
					<%
						} else if (order.getOderStatus().equalsIgnoreCase("Delivery Attempt Failed")) {
					%>
					<p class="productCart-quantityMessage"
						style="color: red; display: inline;">
						<i class="glyphicon glyphicon-remove"></i><%=order.getOderStatus()%></p>
					<br />
					<%
						}
					%>
					<div class="productOrder-price"
						style=" margin-top: 10px;"><%=item.getProduct().getSellingPrice()%></div>
					<br /> <label
						style="float: left; ">Size
						:</label>
					<div class="productOrder-size"
						>
						<%
							if (item.getSize() > 0) {
						%>
						<%=item.getSize()%>
						<%
							} else {
						%>
						-
						<%
							}
						%>
					</div>
					<br /> <label
						style="float: left;">Quantity
						:</label>
					<div class="productOrder-quantity"
						>
						<%=item.getQuantity()%>
					</div>

					<%
						} else {
					%>
					<div class="productCart-removal">
						<button class="remove-productCart">Cancel</button>
					</div>
					<br />
					<div class="orderSizeQuantityPrice">
						<div class="productOrder-price"
							style=" margin-top: 15px;"><%=item.getProduct().getSellingPrice()%></div>
						<br /> <label
							style="float: left;">Size
							:</label>
						<div class="productOrder-size"
							>
							<%
								if (item.getSize() > 0) {
							%>
							<%=item.getSize()%>
							<%
								} else {
							%>
							-
							<%
								}
							%>
						</div>
						<br /> <label
							style="float: left;">Quantity
							:</label>
						<div class="productOrder-quantity"
							>
							<%=item.getQuantity()%>
						</div>
					</div>
					<%
						}
					%>

				</div>
				<%
					if (order.getOderStatus().equalsIgnoreCase("Order Delivered")
										&& item.getIsProductReviewed() == null) {
				%>
				<div id="reviewPopUP">
					<!-- Popup Div Starts Here -->
					<div id="popupContact">
						<!-- Contact Us Form -->
						<div id="reviewsForm" name="reviewsForm">
							<input type="hidden" name="orderId" id="orderId" class="orderId"
								value="<%=order.getOrderId()%>"> <input type="hidden"
								name="productId" id="productId" class="productId"
								value="<%=item.getProduct().getId()%>"> <img id="close"
								src="images/Util/CancelButton.png" onclick="div_hide()" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal">
							<h2>Your Review</h2>
							<hr>

							<textarea name="message" id="reviewMessage" class="reviewMessage"
								placeholder="Enter your review"></textarea>
							<div class="product-reviewSubmit">
								<!-- <button class="reviewSubmit-product">Submit</button> -->

								<a href="javascript:%20check_empty()" id="submit">Submit</a>
							</div>
						</div>
					</div>
					<!-- Popup Div Ends Here -->
				</div>
				<button type="button" class="reviewButton" id="reviewButton"
					onclick="div_show()">Write a Product Review</button>
				<%
					}
				%>
			</div>
			<%
				}
			%>
		</div>
		<br />
		<%
			}
			} else {
		%>
		<h3
			style="color: #7c795d; font-family: 'Trocchi', serif; font-size: 35px; font-weight: normal; line-height: 48px; margin: 0;">No
			Order History</h3>
		<a href="index.jsp">
			<button class="checkout123"
				style="text-align: center; margin-left: 550px;">Continue
				Shopping</button>
		</a>

		<%
			}
		%>



	</div>
</div>
<script src="js/orderProcessing.js"></script>
<script src="js/reviewPopup.js">
	
</script>
<jsp:include page="/include/footer.jsp" />