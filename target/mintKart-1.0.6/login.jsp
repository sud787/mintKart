<jsp:include page="/include/header.jsp" />
<meta name="google-signin-client_id"
	content="957880320130-mqc1ofsv7m21dbvtrgsila31p40uttfl.apps.googleusercontent.com">
<div class="login">

	<div class="main-agileits">
		<div class="form-w3agile">
			<h3>Login</h3>
			<div id="scoped-content">
				<style type="text/css" scoped>
h6 {
	color: red;
	margin-top: -40px;
	margin-bottom: 10px;
}
</style>
			</div>
			<%
				String source = request.getParameter("source");
				if (source != null) {
					session.setAttribute("source", source);
				}
				if (request.getAttribute("error") != null
						&& !request.getAttribute("error").equals("")) {
			%>

			<h6><%=request.getAttribute("error")%>
			</h6>

			<%
				}
			%>
			<form action="authenticateUser"
				method="post">
				<div class="key">
					<i class="fa fa-envelope" aria-hidden="true"></i> <input
						type="text" name="loginId" required=""
						placeholder="Enter Email or Mobile">
					<div class="clearfix"></div>
				</div>
				<div class="key">
					<i class="fa fa-lock" aria-hidden="true"></i> <input
						type="password" name="Password" required="" placeholder="Password">
					<div class="clearfix"></div>
				</div>
				<div class="forg">
					<a href="forgotPassword.jsp" class="forg-left">Forgot Password</a>
				</div>
				<input type="submit" value="Login">
			</form>
		</div>
		<a
			href="https://accounts.google.com/o/oauth2/auth?scope=email&redirect_uri=http://mintkart.in/oauth2callback&response_type=code&client_id=172685467054-ea3fgs84e63dmadg8ql55n836pe372oq.apps.googleusercontent.com&approval_prompt=force">
			<button class="SocialloginBtn loginBtn--google">Login with
				Google</button> <a
			href="https://www.facebook.com/dialog/oauth?client_id=134675527196481&redirect_uri=http://mintkart.in/oauth2fb&scope=email">
				<button class="SocialloginBtn loginBtn--facebook">Login
					with Facebook</button>
		</a>
	</div>
</div>
<jsp:include page="/include/footer.jsp" />