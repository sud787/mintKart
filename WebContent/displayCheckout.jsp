<%@page import="com.ecommerce.business.LineItem"%>
<%@page import="java.util.Iterator"%>
<%@page
	import="java.util.ArrayList,com.ecommerce.business.Product,com.ecommerce.business.LineItem,com.ecommerce.business.Cart"%>
<jsp:include page="/include/header.jsp" />
</div>

<div id="scoped-content">
	<style type="text/css" scoped>
@import "css/styleCheckout.css";
</style>
	<div class="shopping-cart">
		<br />
		<%
			Cart cart = (Cart) session.getAttribute("cart");
			ArrayList<LineItem> items = cart.getItems();
			int cartSize = items.size();
		%>
		<h3 class="bagSize" id="bagSize">
			My Bag(<%=cartSize%>)
		</h3>
		<%
			if (cartSize > 0) {
		%>
		<div class="column-labels" id="column-labels">
			<label class="productCart-image">Image</label> <label
				class="productCart-details">Product</label> <label
				class="productCart-price">Price</label> <label
				class="productCart-quantity">Qty</label> <label
				class="productCart-size">Size</label> <label
				class="productCart-line-price">Total</label>

		</div>
		<hr style="margin-top: 0px;"/>
		<%
			Iterator<LineItem> itrCartItems = items.iterator();
				double totalPrice = 0;
				while (itrCartItems.hasNext()) {
					LineItem item = itrCartItems.next();
		%>
		<div class="productCart">
			<input class="hidden_id" type="hidden"
				value="<%=item.getProduct().getId()%>" />
			<div class="productCart-image">
				<a
					href="displaySingleProduct?productId=<%=item.getProduct().getId()%>"><img
					src="<%=item.getProduct().getImageURL()%>" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal"></a>
			</div>
			<div class="productCart-details">
				<a
					href="displaySingleProduct?productId=<%=item.getProduct().getId()%>"
					target="_blank">
					<div class="productCart-title" style="text-decoration: underline;"><%=item.getProduct().getName()%></div>
				</a>
				<p class="productCart-description"><%=item.getProduct().getProductShortDescription()%></p>

				<%
					if (item.getErrorMessage() != null && !item.getErrorMessage().equals("")
									|| (item.getMessage() != null && !item.getMessage().equals(""))) {
				%>

				<%
					if (item.getErrorMessage() != null && !item.getErrorMessage().equals("")) {
				%>

				<p class="productCart-errorMessage" id="productCart-errorMessage"
					style="color: red;">
					<%=item.getErrorMessage()%>
				</p>
				<%
					} else {
				%>
				<p class="productCart-quantityMessage" style="color: red;">
					<%=item.getMessage()%>
				</p>
				<%
					}
				%>

				<%
					}
				%>
				<p class="productCart-quantityMessage" style="color: red;"></p>
				<div class="productCart-removal">
					<button class="remove-productCart">Remove</button>
				</div>
			</div>
			<div class="productCart-price">
				<%=item.getProduct().getSellingPrice()%></div>
			<div class="productCart-quantity">
				<input type="number" value="<%=item.getQuantity()%>" min="1">
			</div>
			<div class="productCart-size">
				<%
					if (item.getSize() > 0) {
				%>
				<%=item.getSize()%>
				<%
					} else {
				%>
				-
				<%
					}
				%>
			</div>
			<div class="productCart-line-price">
				<%=item.getProduct().getSellingPrice() * item.getQuantity()%></div>
		</div>
		<%
			totalPrice += item.getProduct().getSellingPrice() * item.getQuantity();
				}
		%>
		<div class="nonemptyCart" id="nonemptyCart">
			<div class="totals">
				<div class="totals-item">
					<label>Subtotal</label>
					<div class="totals-value" id="cart-subtotal">
						<%=totalPrice%></div>
				</div>
				<div class="totals-item">
					<label>Shipping</label>
					<div class="totals-value" id="cart-shipping">15.00</div>
				</div>
				<div class="totals-item totals-item-total">
					<label>Grand Total</label>
					<div class="totals-value" id="cart-total">
						<%=totalPrice + 15.00%></div>
				</div>
			</div>
			<!-- <a href="CheckUserCheckoutServlet"
				onclick="return isErrorMessageSet();">
				<button class="checkout">Checkout</button>
			</a>  -->
			<a class="btn btn-large btn-primary " href="index.jsp"> Continue
				Shopping </a>
			<a class="btn btn-large btn-primary checkOutButton" href="CheckUserCheckoutServlet" onclick="return isErrorMessageSet();"> Checkout </a>
			
		</div>
	</div>
	<%
		} else {
	%>

	<div class="content" id="emptyCart">
		<div class="container"><h3 style="color: #7c795d; font-family: 'Trocchi', serif; font-size: 35px; font-weight: normal; line-height: 48px; margin: 0;">Your
			Cart Is Empty</h3>
			</div>
		<div class="emptyCartButton ">	
			<a class="btn btn-large btn-primary emptyCartPosition" href="index.jsp"> Continue
				Shopping </a>
	</div>

	<%
		}
	%>

</div>
<script
	src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src="js/indexCheckout.js"></script>

<jsp:include page="/include/footer.jsp" />