/**
 * 
 */
function updateTopCategory() {
	var topCategory = $('#topCategory').val();

	if (isEmpty(topCategory)) {
		alert('Enter Top Category');
		// return false;
	} else {
		request = createRequest();

		request.open("POST", "addMenu", true);
		request.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		request.send("topCategory=" + topCategory);
		window.location.reload(true);
		// addMenu
	}

}
function updateCategory() {
	var category = $('#newCategory').val();
	var topCategoryValue = $("#topCategory option:selected").attr('data-value');

	if (isEmpty(category)) {
		alert('Enter Top Category');
		// return false;
	} else {
		request = createRequest();

		request.open("POST", "addMenu", true);
		request.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		request.send("category=" + category + "&topCategory="
				+ topCategoryValue);
		window.location.reload(true);
		// addMenu
	}

}
function updateSubCategory() {
	var subCategory = $('#newSubCategory').val();
	var categoryValue = $("#category option:selected").attr('value');

	if (isEmpty(subCategory)) {
		alert('Enter Top Category');
		// return false;
	} else {
		request = createRequest();

		request.open("POST", "addMenu", true);
		request.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		request.send("category=" + categoryValue + "&subCategory="
				+ subCategory);
		window.location.reload(true);

	}
}
function createRequest() {
	try {
		request = new XMLHttpRequest();
	} catch (tryMS) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (otherMS) {
			try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {
				request = null;
			}
		}
	}
	return request;
}
function isEmpty(str) {
	return (!str || 0 === str.length);
}