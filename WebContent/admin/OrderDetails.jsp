<jsp:include page="/admin/include/header.jsp" />
<%@ page
	import="java.util.ArrayList,java.util.Iterator,com.ecommerce.business.Order,com.ecommerce.business.Address,java.text.SimpleDateFormat,com.ecommerce.business.LineItem"%>

<style type="text/css" scoped>
@import "css/styleCheckout.css";
</style>
<br />
<h3 style="float: left;">Order Details</h3>
<br />
<br />
<%
	Order order = (Order) request.getAttribute("order");
	Address address = order.getAddress();
%>
<div id="content" class="col-lg-10 col-sm-10">
	<div class="row">
		<div class="box col-md-12">

			<div class="box-content">
				<table class="table">
					<thead bgcolor="#F6F6F6">
						<tr>
							<th>Shipping Address</th>
							<th>Payment Method</th>
							<th>Order Summary</th>
							<th>Invoice</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="center"><ul style="list-style-type: none">
									<li><%=address.getName()%></li>
									<li><%=address.getAddress()%></li>
									<li><%=address.getCity() + " ," + address.getState()%></li>
									<li><%=address.getPincode() + ", P.H:" + address.getNumber()%></li>
								</ul></td>
							<td class="center"><%=order.getPayementMethod()%></td>
							<td class="center"><ul style="list-style-type: none">
									<li>Item(s) Subtotal: <%=order.getProductCost()%></li>
									<li>Shipping:<%=order.getShippementCharges()%></li>
									<li>Tax:<%=order.getTax()%></li>
									<li>Grand Total:<%=order.getTotalCost()%></li>
								</ul></td>
							<td class="center">View Invoice</td>
						</tr>
					</tbody>
				</table>
				<div class="embeddedCart">
					<div class="headerInfo" style="background: #F6F6F6; height: 70px;">
						<div class="column-labels" style="margin-bottom: -5px;">
							<label class="productCart-image1">ORDER PLACED</label> <label
								class="productCart-details1">TOTAL</label> <label
								class="productCart-price">ORDER #<%=order.getOrderId()%></label>
							<label class="productCart-price" style="margin-left: 65px;">ORDER
								STATUS</label>
						</div>
						<div style="float: left;" class="columnValues">
							<%
								SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
							%>
							<span style="margin-left: 10px;"><%=formatter.format(order.getOrderDate())%></span>
							<span style="margin-left: 170px;" class="totalOrderCost"><%=order.getTotalCost()%></span>
							<%-- <span style="color: #0066C0; margin-left: 415px;"><a
						href="orderDetailsServlet?orderId=<%=order.getOrderId()%>">Order
							Details</a></span> --%>
							<span style="margin-left: 640px;" class="orderStatus"><%=order.getOderStatus()%></span>
							<input type="hidden" id="hiddenOrderId"
								value="<%=order.getOrderId()%>">
						</div>
					</div>
					<br />
					<%
						ArrayList<LineItem> items = order.getItems();
						Iterator<LineItem> itrOrderItems = items.iterator();
						//double totalPrice = 0;
						while (itrOrderItems.hasNext()) {
							LineItem item = itrOrderItems.next();
					%>
					<div class="productCart">
						<input class="hidden_Product_id" id="hidden_Product_id"
							type="hidden" value="<%=item.getProduct().getId()%>"> <input
							class="hidden_Order_id" type="hidden"
							value="<%=order.getOrderId()%>">
						<div class="productCart-image">
							<a
								href="displaySingleProduct?productId=<%=item.getProduct().getId()%>"><img
								src="<%=item.getProduct().getImageURL()%>"></a>
						</div>
						<div class="productCart-details">
							<a
								href="displaySingleProduct?productId=<%=item.getProduct().getId()%>"
								target="_blank">
								<div class="productCart-title"
									style="text-decoration: underline;"><%=item.getProduct().getName()%></div>
							</a>
							<p class="productCart-description"><%=item.getProduct().getProductShortDescription()%></p>
							<%
								String isOrderCancelled = item.getProduct().getIsOrderCancelled();
									int isReviewAllowed = 0;

									if (isOrderCancelled != null && !isOrderCancelled.equals("")) {
										isReviewAllowed = 1;
							%>
							<p class="productCart-quantityMessage"
								style="color: red; display: inline;">Product Cancelled</p>
							<%
								} else if (!order.getOderStatus().equalsIgnoreCase("Order Placed")) {
							%>
							<p class="productCart-quantityMessage"
								style="color: red; display: inline;">
								<%=order.getOderStatus()%></p>
							<br />

							<div class="productCart-price"
								style="margin-left: -2px; margin-top: 10px;"><%=item.getProduct().getSellingPrice()%></div>
							<br /> <label
								style="float: left; margin-left: -55px; margin-top: 5px;">Size
								:</label>
							<div class="productCart-size"
								style="margin-left: -15px; margin-top: 5px;">
								<%=item.getSize()%>
							</div>
							<br /> <label
								style="float: left; margin-left: -80px; margin-top: 5px;">Quantity
								:</label>
							<div class="productCart-quantity"
								style="margin-left: -5px; margin-top: 5px;">
								<%=item.getQuantity()%>
							</div>

							<%
								} else {
							%>
							<!-- <div class="productCart-removal">
								<button class="remove-productCart">Cancel</button>
							</div> -->
							<br />
							<div class="orderSizeQuantityPrice">
								<div class="productCart-price"
									style="margin-left: -40px; margin-top: 10px;"><%=item.getProduct().getSellingPrice()%></div>
								<br /> <label
									style="float: left; margin-left: -55px; margin-top: 5px;">Size
									:</label>
								<div class="productCart-size"
									style="margin-left: -15px; margin-top: 5px;">
									<%=item.getSize()%>
								</div>
								<br /> <label
									style="float: left; margin-left: -80px; margin-top: 5px;">Quantity
									:</label>
								<div class="productCart-quantity"
									style="margin-left: -5px; margin-top: 5px;">
									<%=item.getQuantity()%>
								</div>
							</div>
							<%
								}
							%>

						</div>
					</div>
					<%
						}
					%>
				</div>
			</div>

		</div>
		<%
			String orderStatus = order.getOderStatus();
			if (orderStatus.equals("Order Placed")) {
		%>
		<div class="form-group has-success col-md-4">
			<label class="control-label" for="inputSuccess1">Enter
				Tracking ID</label> <input type="text" class="form-control"
				id="trackingNumber" /> <br /> <a onclick="shipProduct()"
				class="btn btn-info btn-setting">Ship Order</a>
		</div>
		<%
			} else if (orderStatus.equals("Shipped")) {
		%>
		<div class="control-group">
			<label class="control-label" for="selectError">Shipment
				Status</label>

			<div class="controls">
				<select id="shipementStatus" data-rel="chosen">
					<option>Order Delivered</option>
					<option>Delivery Attempt Failed</option>
				</select>
			</div>
			<br /> <a onclick="shipmentStatus()"
				class="btn btn-info btn-setting">Update Shipement Status</a>
		</div>
		<%
			} else {
		%>
		<div class="control-group">
			<label class="control-label" for="selectError">Shipment
				Status</label>

			<div class="controls">
				<select id="shipementStatus" data-rel="chosen">
					<option>Order Delivered</option>
				</select>
			</div>
			<br /> <a onclick="shipmentStatus()"
				class="btn btn-info btn-setting">Update Shipement Status</a>
		</div>
		<%
			}
		%>

	</div>
</div>
<script src="js/orderStateTrasition.js">
	<jsp:include page="/admin/include/footer.jsp" />
