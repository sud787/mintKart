<%@ include file="/include/header.jsp"%>
<%@ page import="com.ecommerce.business.Product"%>

<div class="content" style="background: white;">
	<div class="container">
		<div class="col-md-4 w3ls_dresses_grid_left">
			<%-- <div class="w3ls_dresses_grid_left_grid">
				<h3>Categories</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_dres-type">
						<ul>
							<!-- store this in request object from controller-->
							<!--fetch from sub catgory Table -->
							<!-- fetch the category list from request object -->
							<%
								ArrayList<String> categoryList = (ArrayList<String>) request.getAttribute("categoryList");
								if (categoryList != null) {
									Iterator<String> itrNew = categoryList.iterator();
									while (itrNew.hasNext()) {
										String categoryName = itrNew.next();
							%>
							<li><a href="displayMenProduct?type=<%=categoryName%>"><%=categoryName%></a></li>
							<%
								}
								}
							%>
						</ul>
					</div>
				</div>
			</div> --%>
			<%
				HashSet<String> colourList = (HashSet<String>) request
						.getAttribute("colourSet");
				if (colourList != null && colourList.size() > 0) {
			%>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>Color</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color">
						<ul>
							<%
								Iterator<String> colorItr = colourList.iterator();
									while (colorItr.hasNext()) {
										String colour = colorItr.next();
							%>
							<li><a href="#"><i style="background:<%=colour%>"></i> <%=colour%></a></li>
							<%
								}
							%>
						</ul>
					</div>
				</div>
			</div>
			<%
				}
			%>
			<!-- <div class="w3ls_dresses_grid_left_grid">
				<h3>Size</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li><a href="#">Medium</a></li>
							<li><a href="#">Large</a></li>
							<li><a href="#">Extra Large</a></li>
							<li><a href="#">Small</a></li>
						</ul>
					</div>
				</div>
			</div> -->
			<!--  -->
			<div class="w3ls_dresses_grid_left_grid">
				<h3>Brand</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<!-- store this in request object from controller-->
							<!--fetch from sub catgory Table -->
							<!-- fetch the category list from request object -->
							<%
								HashSet<String> brandList = (HashSet<String>) request
										.getAttribute("brandList");
								if (brandList != null) {
									Iterator<String> brandNew = brandList.iterator();
									while (brandNew.hasNext()) {
										String brandName = brandNew.next();
							%>
							<li><a href="displayMenProduct?type=<%=brandName%>"><%=brandName%></a></li>
							<%
								}
								}
							%>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- this div is the main enclosing -->
		<div class="col-md-8 col-sm-8 women-dresses">
			<%
				ArrayList<Product> productList = (ArrayList<Product>) request
						.getAttribute("productList");

				if (productList != null) {
					Iterator<Product> itrNew = productList.iterator();
					while (itrNew.hasNext()) {
						Product product = itrNew.next();
			%>
			<div class="col-md-4 women-grids wp1 animated wow slideInUp"
				data-wow-delay=".5s">
				<a href="displaySingleProduct?productId=<%=product.getId()%>"><div
						class="product-img">
						<img src="<%=product.getImageURL()%>" alt="" />
						<!-- <div class="p-mask">
							<form action="#" method="post">
								<input type="hidden" name="cmd" value="_cart" /> <input
									type="hidden" name="add" value="1" /> <input type="hidden"
									name="w3ls1_item" value="Casual shirt" /> <input type="hidden"
									name="amount" value="50.00" />
								<button type="submit" class="w3ls-cart pw3ls-cart">
									<i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart
								</button>
							</form>
						</div> -->
					</div></a> <i class="fa fa-star yellow-star" aria-hidden="true"></i> <i
					class="fa fa-star yellow-star" aria-hidden="true"></i> <i
					class="fa fa-star yellow-star" aria-hidden="true"></i> <i
					class="fa fa-star yellow-star" aria-hidden="true"></i> <i
					class="fa fa-star gray-star" aria-hidden="true"></i>
				<h4><%=product.getName()%></h4>
				<% if (product.getDiscount() > 0) {
				%>
				<h3 class="w3offDiscount"><%=product.getDiscount()%>%OFF</h3>
				<%
					}
				%>
				<h5 class="product-price"><%=product.getSellingPrice()%></h5>
			</div>
			<%
				}
				}
			%>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<jsp:include page="/include/footer.jsp" />