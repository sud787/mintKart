<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="com.ecommerce.data.CookieUtil,com.ecommerce.business.User,com.ecommerce.data.UserDB,com.ecommerce.data.MenuUtil,java.util.ArrayList,java.util.Iterator,java.util.HashSet"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<link rel="shortcut icon" href="images/Util/logo1.jpg" type="image/png">
<title>Mint Kart-An online shopping store in Manipur| E-commerce store in Manipur  </title>
<meta name="description" content="Online Shopping in Manipur. E-commerce in Manipur.Manipuri dress online shopping">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal" />
<script type="application/x-javascript">
	
			
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 


</script>
<!-- css -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="css/loader.css" type="text/css" media="all" />
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"
	media="all" />
<!--// css -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro"
	rel="stylesheet">
<link
	href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
</head>

<body>
	<div class="header-top-w3layouts">
		<div class="container">
			<div class="col-md-6 logo-w3">
				<a href="index.jsp"><img src="images/Util/logo1.jpg" alt="manipuri dress online shopping,online shopping in manipur,online shopping in north east,e commerce in manipur,e commerce in northeast,online shopping in imphal,e commerce shopping in imphal" />
					<h1>
						MINT<span>KART</span>
					</h1></a>
			</div>
			<%
				User user = (User) session.getAttribute("user");
				if (user == null) {
					Cookie[] cookie = request.getCookies();
					String cookieValue = CookieUtil.getCookieValue(cookie, "id");
					if (cookieValue == null || cookieValue.equals("")) {
			%>
			<div align="left" class="col-md-6 phone-w3l">
				<ul>
					<li id=""><a href="login.jsp"
						style="margin-right: 10px; margin-left: 20px;">Login In</a></li>
					<li id=""><a href="register.jsp" style="margin-right: -60px;">Signup</a></li>
				</ul>
			</div>

			<%
				} else {
				  user = UserDB.selectUser(cookieValue);
						if (user == null) {
							%>
							<div align="left" class="col-md-6 phone-w3l">
								<ul>
									<li id=""><a href="login.jsp"
										style="margin-right: 10px; margin-left: 20px;">Login In</a></li>
									<li id=""><a href="register.jsp" style="margin-right: -60px;">Signup</a></li>
								</ul>
							</div>
							<%
								} else {
											session.setAttribute("user", user);
							%>
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button"
									data-toggle="dropdown">
									Account <span class="caret"></span>
								</button>
								<ul class="dropdown-menu ">
									<li><a href="orderServlet">Orders</a></li>
									<li><a href="logout">Log Out</a></li>
								</ul>
							</div>
							<%
								}
					}
				} else {
			%>
			<div class="dropdown dropDownLoggedIn">
				<button class="btn btn-primary dropdown-toggle" type="button"
					data-toggle="dropdown">
					Account <span class="caret"></span>
				</button>
				<ul class="dropdown-menu ">
					<li><a href="orderServlet">Orders</a></li>
					<li><a href="logout">Log Out</a></li>
				</ul>
			</div>

			<%
				}
			%>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="header-bottom-w3ls">
		<div class="container">
			<div class="col-md-7 navigation-agileits">
				<nav class="navbar navbar-default">
				<div class="navbar-header nav_2">
					<button type="button"
						class="navbar-toggle collapsed navbar-toggle1"
						data-toggle="collapse" data-target="#bs-megadropdown-tabs">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<ul class="nav navbar-nav ">
						<li class=" active"><a href="index.jsp" class="hyper "><span>Home</span></a></li>
						<%
							ArrayList<String> topCategory = MenuUtil.getTopCategory();
							Iterator<String> topCategoryItr = topCategory.iterator();
							while (topCategoryItr.hasNext()) {
								String topCategoryName = topCategoryItr.next();
						%>
						<li class="dropdown "><a class="dropdown-toggle  hyper"
							data-toggle="dropdown"><span><%=topCategoryName%> <b
									class="caret"></b> </span></a>
							<ul class="dropdown-menu multi">
								<div class="row">
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<%
												ArrayList<String> subCategoryMen = MenuUtil.getSubcategoryMenu(topCategoryName);
													if (subCategoryMen != null) {
														Iterator<String> itr = subCategoryMen.iterator();
														int sizeCheck = 0;
														while (itr.hasNext()) {
															//itr.next()
															++sizeCheck;
															String subType = itr.next();
															int check = sizeCheck % 4;
															if (sizeCheck > 4 && sizeCheck % 4 == 1) {
											%><!-- close the previous div and ul and create the new one -->
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<%
												}
											%>
											<li><a href="displayProduct?type=<%=subType%>"><i
													class="fa fa-angle-right" aria-hidden="true"></i> <%=subType%>
											</a></li>
											<%
												}
													}
													//make the db call to fetch the subcategory list
											%>
										</ul>
									</div>
									<div class="clearfix"></div>
								</div>
							</ul></li>
						<%
							}
						%>
					</ul>
				</div>
				</nav>
			</div>
			<script>
				$(document).ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).stop(true,
												true).slideDown("fast");
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).stop(true,
												true).slideUp("fast");
										$(this).toggleClass('open');
									});
						});
			</script>
			<div class="col-lg-4 search-agileinfo" id="searchElement">
				<form action="search" method="get">
					<input type="search" name="Search"
						placeholder="Search for a Product..." required="">
					<button type="submit" class="btn btn-default search"
						aria-label="Left Align" onclick="">
						<i class="fa fa-search" aria-hidden="true"> </i>
					</button>
				</form>
			</div>
			<div class="col-md-1 cart-wthree">
				<div class="cart">
					<form action="cartSummary" method="post" class="last">

						<button class="w3view-cart" type="submit" name="submit" value="">
							<i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
						</button>
					</form>
				</div>


				<div class="clearfix"></div>
			</div>
		</div>

		<div id="loader" class="loading">Loading&#8230;</div>