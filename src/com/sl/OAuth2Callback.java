package com.sl;

import com.ecommerce.business.User;
import com.ecommerce.data.CartDB;
import com.ecommerce.data.CookieUtil;
import com.ecommerce.data.UserDB;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/oauth2callback")
public class OAuth2Callback extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static String className = "com.sl.Oauth2callback";

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			String code = request.getParameter("code");
			String source = (String) session.getAttribute("source");
			String urlParameters = "code=" + code + "&client_id="
					+ Setup.CLIENT_ID + "&client_secret=" + Setup.CLIENT_SECRET
					+ "&redirect_uri=" + Setup.REDIRECT_URL
					+ "&grant_type=authorization_code";
			URL url = new URL("https://accounts.google.com/o/oauth2/token");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter writer = new OutputStreamWriter(
					conn.getOutputStream());
			writer.write(urlParameters);
			writer.flush();
			String line1 = "";
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				line1 = line1 + line;
			}
			String s = GsonUtility.getJsonElementString("access_token", line1);

			url = new URL(
					"https://www.googleapis.com/oauth2/v1/userinfo?access_token="
							+ s);
			conn = url.openConnection();
			line1 = "";
			reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			while ((line = reader.readLine()) != null) {
				line1 = line1 + line;
			}
			GooglePojo data = (GooglePojo) new Gson().fromJson(line1,
					GooglePojo.class);
			if (data != null) {
				User user = new User();
				user.setFirstName(data.getName());
				user.setEmailAddress(data.getEmail());
				String sessionId = data.getId();
				user.setSessionId(sessionId);
				String cookieValue = null;
				Cookie[] cookie = request.getCookies();
				cookieValue = CookieUtil.getCookieValue(cookie, "id");
				// if user already exists,create session object and store the
				// user information
				if (!UserDB.selectUserID(sessionId).equals("")) {
					if (cookieValue != null && !cookieValue.equals("")) {
						// update the logged in user db with guest cart
						// check if the user has already cart
						int guestId = CartDB.getCartId(cookieValue);
						int loggenUserCartId = CartDB.getCartId(user
								.getSessionId());
						if (guestId > 0) {
							if (loggenUserCartId == 0) {
								CartDB.insertIntocart(user.getSessionId());
								loggenUserCartId = CartDB.getCartId(user
										.getSessionId());
							}

							// int loggedInUsercartId =
							// CartDB.getCartId(user.getSessionId());
							if (loggenUserCartId > 0) {
								CartDB.updateGuestToLoginProductHandling(
										guestId, loggenUserCartId);
								CartDB.removeCartEntry(guestId);
								UserDB.removeGuestEntry(cookieValue);
							}

						}
					}
					session.setAttribute("user", user);
					Cookie cookieId = new Cookie("id", sessionId);
					cookieId.setMaxAge(60 * 60 * 24 * 365 * 2);
					cookieId.setPath("/");
					response.addCookie(cookieId);

				} else {
					if (UserDB.userRegistered(user.getEmailAddress())) {
						// fetch the loginid of existing user
						String loginId = UserDB.getLoginId(user
								.getEmailAddress());
						user.setSessionId(loginId);
						if (cookieValue != null && !cookieValue.equals("")) {
							// update the logged in user db with guest cart
							int guestId = CartDB.getCartId(cookieValue);
							int loggenUserCartId = CartDB.getCartId(user
									.getSessionId());
							if (guestId > 0) {
								if (loggenUserCartId == 0) {
									CartDB.insertIntocart(user.getSessionId());
									loggenUserCartId = CartDB.getCartId(user
											.getSessionId());
								}
								if (loggenUserCartId > 0) {
									// update the guest cartdetails table,change
									// the
									// cart id
									// of guest to logged in user
									CartDB.updateGuestToLoginProductHandling(
											guestId, loggenUserCartId);
									CartDB.removeCartEntry(guestId);
									UserDB.removeGuestEntry(cookieValue);
									// insert into cart and then updatte

								}
							}
						}
					} else {
						if (UserDB.insertGoogleLoginInfo(user, sessionId) > 0) {
							UserDB.insertGoogleUserRegisterInfo(user, sessionId);
							if (cookieValue != null && !cookieValue.equals("")) {
								// update the logged in user db with guest cart
								int guestId = CartDB.getCartId(cookieValue);

								if (guestId > 0) {
									CartDB.insertIntocart(user.getSessionId());
									int loggedInUsercartId = CartDB
											.getCartId(user.getSessionId());
									if (loggedInUsercartId > 0) {
										CartDB.updateGuestToLoginProductHandling(
												guestId, loggedInUsercartId);
										CartDB.removeCartEntry(guestId);
										UserDB.removeGuestEntry(cookieValue);
										// insert into cart and then updatte

									}

								}
							}

						}
					}
					session.setAttribute("user", user);
					Cookie cookieId = new Cookie("id", user.getSessionId());
					cookieId.setMaxAge(60 * 60 * 24 * 365 * 2);
					cookieId.setPath("/");
					response.addCookie(cookieId);

				}
				// first check whether user is already in database
			}
			writer.close();
			reader.close();
			String path = "";
			if (source != null) {
				path = "/cartSummary";
			} else {
				path = "/index.jsp";
			}
			RequestDispatcher view = request.getRequestDispatcher(path);
			view.forward(request, response);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}
}
