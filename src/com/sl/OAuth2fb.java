package com.sl;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.User;
import com.ecommerce.data.CartDB;
import com.ecommerce.data.CookieUtil;
import com.ecommerce.data.UserDB;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/oauth2fb")
public class OAuth2fb extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			String source = (String) session.getAttribute("source");
			String rid = request.getParameter("request_ids");
			if (rid != null) {
				response.sendRedirect("https://www.facebook.com/dialog/oauth?client_id="
						+ Setup.FB_CLIENT_ID
						+ "&redirect_uri="
						+ Setup.FB_REDIRECT_URL + "");
			} else {
				String code = request.getParameter("code");
				if (code != null) {
					URL url = new URL(
							"https://graph.facebook.com/oauth/access_token?client_id="
									+ Setup.FB_CLIENT_ID + "&redirect_uri="
									+ Setup.FB_REDIRECT_URL + "&client_secret="
									+ Setup.FB_CLIENT_SECRET + "&code=" + code);
					HttpURLConnection conn = (HttpURLConnection) url
							.openConnection();
					conn.setRequestMethod("GET");
					conn.setConnectTimeout(20000);
					String outputString = "";
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(conn.getInputStream()));
					String line;
					while ((line = reader.readLine()) != null) {
						outputString = outputString + line;
					}
					System.out.println(outputString);
					String accessToken = null;
					if (outputString.indexOf("access_token") != -1) {
						int k = outputString.length();
						accessToken = outputString.substring(17,
								outputString.indexOf(",") - 1);
					}
					System.out.println(accessToken);
					url = new URL("https://graph.facebook.com/me?access_token="
							+ accessToken);
					System.out.println(url);
					URLConnection conn1 = url.openConnection();
					conn1.setConnectTimeout(7000);
					outputString = "";
					reader = new BufferedReader(new InputStreamReader(
							conn1.getInputStream()));
					while ((line = reader.readLine()) != null) {
						outputString = outputString + line;
					}
					reader.close();
					System.out.println(outputString);
					FaceBookPojo fbp = (FaceBookPojo) new Gson().fromJson(
							outputString, FaceBookPojo.class);
					if (fbp != null) {
						User user = new User();
						user.setFirstName(fbp.getName());
						String sessionId = fbp.getId();
						user.setSessionId(sessionId);
						String cookieValue = null;
						Cookie[] cookie = request.getCookies();
						cookieValue = CookieUtil.getCookieValue(cookie, "id");
						if (!UserDB.selectUserID(sessionId).equals("")) {
							if (cookieValue != null && !cookieValue.equals("")) {
								// update the logged in user db with guest cart
								int guestId = CartDB.getCartId(cookieValue);
								int loggenUserCartId = CartDB.getCartId(user
										.getSessionId());
								if (guestId > 0) {
									if (loggenUserCartId == 0) {
										CartDB.insertIntocart(user
												.getSessionId());
										loggenUserCartId = CartDB
												.getCartId(user.getSessionId());
									}
									if (loggenUserCartId > 0) {
										CartDB.updateGuestToLoginProductHandling(
												guestId, loggenUserCartId);
										CartDB.removeCartEntry(guestId);
										UserDB.removeGuestEntry(cookieValue);
										// insert into cart and then updatte

									}

								}
							}
							session.setAttribute("user", user);
							Cookie cookieId = new Cookie("id", sessionId);
							cookieId.setMaxAge(60 * 60 * 24 * 365 * 2);
							cookieId.setPath("/");
							response.addCookie(cookieId);

						} else {
							if (UserDB.insertFacebookLoginInfo(sessionId) > 0) {
								UserDB.insertGoogleUserRegisterInfo(user,
										sessionId);
								if (cookieValue != null
										&& !cookieValue.equals("")) {
									// update the logged in user db with guest
									// cart
									int guestId = CartDB.getCartId(cookieValue);
									if (guestId > 0) {
										CartDB.insertIntocart(user
												.getSessionId());
										int loggedInUsercartId = CartDB
												.getCartId(user.getSessionId());
										if (loggedInUsercartId > 0) {
											// update the guest cartdetails
											// table,change
											// the
											// cart id
											// of guest to logged in user
											CartDB.updateGuestToLoginProductHandling(
													guestId, loggedInUsercartId);
											CartDB.removeCartEntry(guestId);
											UserDB.removeGuestEntry(cookieValue);
											// insert into cart and then updatte

										}

									}
								}
								session.setAttribute("user", user);
								Cookie cookieId = new Cookie("id", sessionId);
								cookieId.setMaxAge(60 * 60 * 24 * 365 * 2);
								cookieId.setPath("/");
								response.addCookie(cookieId);
							}
						}
						String path = "";
						if (source != null) {
							path = "/cartSummary";
						} else {
							path = "/index.jsp";
						}
						RequestDispatcher view = request
								.getRequestDispatcher(path);
						view.forward(request, response);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
