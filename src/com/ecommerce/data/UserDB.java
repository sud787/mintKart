package com.ecommerce.data;

import com.ecommerce.business.*;

import java.sql.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class UserDB {
	public static int insertLoginInfo(User user, String password, String sessionId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "INSERT INTO logininfo (loginId, email, phoneNumber, password) " + "VALUES (?, ?, ?, ?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, sessionId);
			ps.setString(2, user.getEmailAddress());
			ps.setLong(3, user.getMobileNumber());
			ps.setString(4, ProductUtil.md5(password));
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int updateLoginInfo(User user, String password, String sessionId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "update   logininfo set email=? , phoneNumber=? ,password=? " + "where loginId = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, user.getEmailAddress());
			ps.setLong(2, user.getMobileNumber());
			ps.setString(3, ProductUtil.md5(password));
			ps.setString(4, sessionId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int insertGuestInfo(String sessionId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "INSERT INTO logininfo (loginId) " + "VALUES (?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, sessionId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int update(User user) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		// This method updates the record with a matching email address.
		// It returns a value of 0 if the email address can't be found.
		String query = "UPDATE User SET " + "FirstName = ?, " + "LastName = ?, " + "CompanyName = ?, "
				+ "Address1 = ?, " + "Address2 = ?, " + "City = ?, " + "State = ?, " + "Zip = ?, " + "Country = ?, "
				+ "CreditCardType = ?, " + "CreditCardNumber = ?, " + "CreditCardExpirationDate = ? "
				+ "WHERE EmailAddress = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, user.getFirstName());
			ps.setString(2, user.getLastName());
			ps.setString(3, user.getCompanyName());
			ps.setString(4, user.getAddress1());
			ps.setString(5, user.getAddress2());
			ps.setString(6, user.getCity());
			ps.setString(7, user.getState());
			ps.setString(8, user.getZip());
			ps.setString(9, user.getCountry());
			ps.setString(10, user.getCreditCardType());
			ps.setString(11, user.getCreditCardNumber());
			ps.setString(12, user.getCreditCardExpirationDate());
			ps.setString(13, user.getEmailAddress());

			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static boolean userExists(String emailAddress, long phoneNumber) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT email FROM logininfo " + "WHERE email = ? or phoneNumber = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, emailAddress);
			ps.setLong(2, phoneNumber);
			rs = ps.executeQuery();
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static String selectUserID(String sessionId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		// This method returns 0 if invoiceID isn't found.
		String query = "SELECT loginId FROM logininfo " + "WHERE loginId = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, sessionId);
			rs = ps.executeQuery();
			if (rs.next())
				return rs.getString("loginId");
			else
				return "";
		} catch (SQLException e) {
			e.printStackTrace();
			return "";
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	// This method returns null if a record isn't found.
	public static User selectUser(String cookieValue) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT userinfo.fname,userinfo.lname,logininfo.email,logininfo.phoneNumber FROM logininfo "
				+" INNER JOIN userinfo on logininfo.loginId=userinfo.loginId "
				+ "WHERE logininfo.loginId = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, cookieValue);
			rs = ps.executeQuery();
			User user = null;
			if (rs.next()) {
				user = new User();
				user.setFirstName(rs.getString("fname"));
				user.setLastName(rs.getString("lname"));
				user.setEmailAddress(rs.getString("email"));
				user.setMobileNumber(rs.getLong("phoneNumber"));
				user.setSessionId(cookieValue);
			}
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static User selectGuestUser(String cookieValue) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// System.out.println("cookieValue---------" + cookieValue);
		String query = "SELECT userinfo.fname,userinfo.lname,logininfo.email,logininfo.phoneNumber  FROM logininfo INNER JOIN userinfo on logininfo.loginId=userinfo.loginId "
				+ "WHERE logininfo.loginId = ?";
		System.out.println("query" + query);
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, cookieValue);
			rs = ps.executeQuery();
			User user = null;
			if (rs.next()) {
				user = new User();
				user.setFirstName(rs.getString("fname"));
				user.setLastName(rs.getString("lname"));
				user.setEmailAddress(rs.getString("email"));
				user.setMobileNumber(rs.getLong("phoneNumber"));
				user.setSessionId(cookieValue);
			}
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int insertUserRegisterInfo(User user, String sessionId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "INSERT INTO userinfo (fname, lname, loginId) " + "VALUES (?, ?, ?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, user.getFirstName());
			ps.setString(2, user.getLastName());
			ps.setString(3, sessionId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static User authenticateUser(String loginId, String password) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = null;
		boolean isEmailid = false;
		if (loginId.indexOf('@') != -1) {
			query = "select loginId from logininfo where email = ? and password = ?";
			isEmailid = true;
		} else {
			query = "select loginId from logininfo where phoneNumber = ? and password = ?";
		}
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, loginId);
			ps.setString(2, ProductUtil.md5(password));
			rs = ps.executeQuery();
			User user = null;
			if (rs.next()) {
				user = selectUser(rs.getString("loginId"));
			}
			if (user != null) {
				user.setSessionId(rs.getString("loginId"));
			}
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int insertGoogleLoginInfo(User user, String sessionId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "INSERT INTO logininfo (loginId, email) " + "VALUES (?, ?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, sessionId);
			ps.setString(2, user.getEmailAddress());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int insertGoogleUserRegisterInfo(User user, String sessionId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "INSERT INTO userinfo (fname,loginId) " + "VALUES (?, ?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, user.getFirstName());
			ps.setString(2, sessionId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int insertFacebookLoginInfo(String sessionId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "INSERT INTO logininfo (loginId) " + "VALUES (?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, sessionId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int getRegisteredUser() {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "select COUNT(*) AS total from userinfo";
		try {
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			rs.next();
			return rs.getInt("total");
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int getAllUser() {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "select COUNT(*) AS total from logininfo";
		try {
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			rs.next();
			return rs.getInt("total");
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static boolean userRegistered(String emailAddress) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT email FROM logininfo " + "WHERE email = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, emailAddress);
			rs = ps.executeQuery();
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static boolean userAlreadyRequestedPassword(String emailAddress) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT USERID FROM password_change_requests " + "WHERE USERID = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, emailAddress);
			rs = ps.executeQuery();
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int updateTokenInfo(String emailId, java.util.Date date, String token) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Timestamp timestamp = new Timestamp(date.getTime());
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "update   password_change_requests  set ID=? , time=? " + "where USERID = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, token);
			ps.setTimestamp(2, timestamp);
			;
			ps.setString(3, emailId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int insertTokenInfo(String emailId, java.util.Date date, String token) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Timestamp timestamp = new Timestamp(date.getTime());
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "INSERT INTO password_change_requests (ID, USERID, time) " + "VALUES (?, ?, ?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, token);
			ps.setString(2, emailId);
			ps.setTimestamp(3, timestamp);
			;
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static Timestamp validateToken(Timestamp timestamp, String token) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		// This method returns 0 if invoiceID isn't found.
		String query = "SELECT time FROM password_change_requests " + "WHERE time >= ? and ID=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setTimestamp(1, timestamp);
			ps.setString(2, token);
			rs = ps.executeQuery();
			if (rs.next())
				return rs.getTimestamp("time");
			else
				return null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static String getEmaildFromToken(String token) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		// This method returns 0 if invoiceID isn't found.
		String query = "SELECT USERID FROM password_change_requests " + "WHERE ID =?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, token);
			rs = ps.executeQuery();
			if (rs.next())
				return rs.getString("USERID");
			else
				return null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int resetPassword(String emailId, String password) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// System.out.println("MD5 in hex: " + md5("hello"));
		// This method adds a new record to the Users table in the database
		// first insert into the parent table(loginInfo) then into userInfo
		String query = "update   logininfo  set password=?" + "where email = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, ProductUtil.md5(password));
			ps.setString(2, emailId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static String getLoginId(String emailAddress) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String loginId = null;
		ArrayList<LineItem> items = new ArrayList();
		String query = "select loginId from  logininfo where email= ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, emailAddress);
			rs = ps.executeQuery();
			if (rs.next()) {
				loginId = rs.getString("loginId");
			}
			return loginId;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int removeGuestEntry(String id) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "delete from logininfo where loginId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, id);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static User authenticateAdmin(String loginId, String password) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = null;
		User user = null;
		String userName = null;
		query = "select username from adminlogin where username = ? and password = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, loginId);
			ps.setString(2, password);
			rs = ps.executeQuery();

			if (rs.next()) {
				userName = rs.getString("username");
			}
			if (userName != null) {
				user = new User();
				user.setSessionId(rs.getString("username"));
			}
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

}