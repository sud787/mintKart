package com.ecommerce.data;

import com.ecommerce.business.*;
import com.mysql.jdbc.Statement;

import java.sql.*;
import java.util.*;

public class ProductDB {
	// This method returns null if a product isn't found.
	public static Product selectProduct(String productCode) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT product.productId,product.subCategoryId,product.productCode,product.name,product.hsn,product.tax,product.costprice,product.sellingPrice,product.discount,product.colour,product.productShortDescription,product.productLongDescription, "
				+ "product.productAdditionalInformation,brand.brandName FROM product "
				+ "INNER JOIN brand on product.brandId=brand.brandId " + "where productId =?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, productCode);
			rs = ps.executeQuery();
			if (rs.next()) {
				Product p = new Product();
				p.setId(rs.getString("productId"));
				p.setName(rs.getString("name"));
				p.setSubCategoryId(rs.getString("subCategoryId"));
				// p.setPrice(rs.getDouble("costprice"));
				p.setCostPrice(rs.getDouble("costprice"));
				p.setSellingPrice(rs.getDouble("sellingPrice"));
				p.setDiscount(rs.getInt("discount"));
				p.setColour(rs.getString("colour"));
				p.setProductShortDescription(rs.getString("productShortDescription"));
				p.setProductLongDescription(rs.getString("productLongDescription"));
				p.setProductAdditionalInformation(rs.getString("productAdditionalInformation"));
				p.setBrand(rs.getString("brandName"));
				p.setHSNNumber(rs.getString("hsn"));
				String tax = rs.getString("tax");
				if (tax != null && !tax.equals(""))
					p.setTax(Double.parseDouble(tax));
				else 
					p.setTax(0);
				
				p.setProductCode(rs.getString("productCode"));
				return p;
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	/*
	 * // This method will return 0 if productID isn't found. public static int
	 * selectProductID(Product product) { ConnectionPool pool =
	 * ConnectionPool.getInstance(); Connection connection = pool.getConnection();
	 * PreparedStatement ps = null; ResultSet rs = null;
	 * 
	 * String query = "SELECT ProductID FROM Product " + "WHERE ProductCode = ?";
	 * try { ps = connection.prepareStatement(query); ps.setString(1,
	 * product.getCode()); rs = ps.executeQuery(); rs.next(); int productID =
	 * rs.getInt("ProductID"); return productID; } catch (SQLException e) {
	 * e.printStackTrace(); return 0; } finally { DBUtil.closeResultSet(rs);
	 * DBUtil.closePreparedStatement(ps); pool.freeConnection(connection); } }
	 * 
	 * // This method returns null if a product isn't found. public static Product
	 * selectProduct(int productID) { ConnectionPool pool =
	 * ConnectionPool.getInstance(); Connection connection = pool.getConnection();
	 * PreparedStatement ps = null; ResultSet rs = null;
	 * 
	 * String query = "SELECT * FROM Product " + "WHERE ProductID = ?"; try { ps =
	 * connection.prepareStatement(query); ps.setInt(1, productID); rs =
	 * ps.executeQuery(); if (rs.next()) { Product p = new Product();
	 * p.setCode(rs.getString("ProductCode"));
	 * p.setDescription(rs.getString("ProductDescription"));
	 * p.setPrice(rs.getDouble("ProductPrice")); return p; } else { return null; } }
	 * catch (SQLException e) { e.printStackTrace(); return null; } finally {
	 * DBUtil.closeResultSet(rs); DBUtil.closePreparedStatement(ps);
	 * pool.freeConnection(connection); } }
	 */

	// This method returns null if a product isn't found.
	public static ArrayList<Product> selectProducts(String subcategory, int startIndex, int recordPerpage
	/* HashSet<String> colourSet, HashSet<String> brandList */) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT product.productId,product.name,product.productCode,product.costprice,product.sellingPrice,product.discount,product.colour FROM product "
				+ "INNER JOIN subcategory on subcategory.subcategoryid=product.subCategoryId "
				+ "where  subcategory.subcategoryName=? LIMIT " + (startIndex) + "," + recordPerpage;
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, subcategory);
			rs = ps.executeQuery();
			ArrayList<Product> products = new ArrayList<Product>();
			while (rs.next()) {
				Product p = new Product();
				p.setId(rs.getString("productId"));
				p.setName(rs.getString("name"));
				p.setCostPrice(rs.getDouble("costprice"));
				p.setSellingPrice(rs.getDouble("sellingPrice"));
				p.setDiscount(rs.getInt("discount"));
				// brandList.add(brand);
				String colour = rs.getString("colour");
				p.setProductCode(rs.getString("productCode"));
				p.setColour(colour);
				// colourSet.add(colour);
				// p.setDescription(rs.getString("ProductDescription"));
				// p.setPrice(rs.getDouble("ProductPrice"));
				products.add(p);
			}
			return products;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	// overloadedMethod for admin Display
	public static ArrayList<Product> selectProducts(int subcategoryID) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT product.productId,product.name,product.productCode,product.costprice,product.sellingPrice,product.discount,product.colour FROM product "
				+ "where  subCategoryId=? ";
		// LIMIT " + (startIndex) + "," + recordPerpage
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, subcategoryID);
			rs = ps.executeQuery();
			ArrayList<Product> products = new ArrayList<Product>();
			while (rs.next()) {
				Product p = new Product();
				p.setId(rs.getString("productId"));
				p.setName(rs.getString("name"));
				p.setCostPrice(rs.getDouble("costprice"));
				p.setSellingPrice(rs.getDouble("sellingPrice"));
				p.setDiscount(rs.getInt("discount"));
				// brandList.add(brand);
				String colour = rs.getString("colour");
				p.setProductCode(rs.getString("productCode"));
				p.setColour(colour);
				// colourSet.add(colour);
				// p.setDescription(rs.getString("ProductDescription"));
				// p.setPrice(rs.getDouble("ProductPrice"));
				products.add(p);
			}
			return products;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static HashMap<Integer, Integer> selectSizeQuantity(String productCode) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT size,quantity FROM sizequantitypivot " + " where productId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, productCode);
			rs = ps.executeQuery();
			HashMap<Integer, Integer> sizeQuantityMap = new HashMap<>();
			while (rs.next()) {
				int size = rs.getInt("size");
				int quantity = rs.getInt("quantity");
				if (quantity >= 1) {
					sizeQuantityMap.put(size, quantity);
				}
				// p.setDescription(rs.getString("ProductDescription"));
				// p.setPrice(rs.getDouble("ProductPrice"));
				// products.add(p);
			}
			return sizeQuantityMap;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int getSizeID(String productCode, int size) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT sizeid from sizequantitypivot where productId=? and size = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, productCode);
			ps.setInt(2, size);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("sizeid");
			} else {
				return -1;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}
	//overloaded Method for handling the products that doesn't has a size;
	public static int getSizeID(String productCode) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT sizeid from sizequantitypivot where productId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, productCode);
		//	ps.setInt(2, size);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("sizeid");
			} else {
				return -1;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int reduceQuantity(int sizeId, int quantity) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "update  sizequantitypivot set quantity=quantity -" + quantity + " where sizeid=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, sizeId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int getQuantity(int size, int productId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "select quantity from  sizequantitypivot where size=? and productId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, size);
			ps.setInt(2, productId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static ArrayList<String> getAllBranads() {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT brandName  FROM brand ";
		try {
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList<String> brands = new ArrayList();
			while (rs.next()) {
				brands.add(rs.getString("brandName"));
			}
			return brands;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static ArrayList<Product> selectProductsForSearch(String whereClause, HashSet<String> colourSet,
			HashSet<String> productBrandSet) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT product.productId,product.name,product.costprice,product.productCode,product.sellingPrice,product.discount,product.colour,brand.brandName FROM product "
				+ "INNER JOIN subcategory on subcategory.subcategoryid=product.subCategoryId "
				+ "INNER JOIN brand on product.brandId=brand.brandId " + whereClause;
		try {
			ps = connection.prepareStatement(query);
			// ps.setString(1, subcategory);
			rs = ps.executeQuery();
			ArrayList<Product> products = new ArrayList<Product>();
			while (rs.next()) {
				Product p = new Product();
				p.setId(rs.getString("productId"));
				p.setName(rs.getString("name"));
				p.setCostPrice(rs.getDouble("costprice"));
				p.setSellingPrice(rs.getDouble("sellingPrice"));
				p.setDiscount(rs.getInt("discount"));
				p.setBrand(rs.getString("brandName"));
				p.setProductCode(rs.getString("productCode"));
				String colour = rs.getString("colour");
				p.setColour(colour);
				colourSet.add(colour);
				productBrandSet.add(rs.getString("brandName"));
				// p.setDescription(rs.getString("ProductDescription"));
				// p.setPrice(rs.getDouble("ProductPrice"));
				products.add(p);
			}
			return products;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static String getBrandID(String brandName) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT brandId from brand where brandName=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, brandName);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("brandId");
			}
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static HashMap<String, String> getAvailableColours(String productCode, String productId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<String, String> availabeColours = new HashMap<>();
		String query = "SELECT productId,colour from product where productCode=? AND productId!=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, productCode);
			ps.setString(2, productId);
			rs = ps.executeQuery();
			while (rs.next()) {
				availabeColours.put(rs.getString("colour"), rs.getString("productId"));
			}
			return availabeColours;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	// //String query =
	// "INSERT INTO cartDetails (cartId,productId,sizeid,quantity) "
	// + "VALUES (?,?,?,?)";
	public static int addProduct(Product product, int subcategoryId, int brandId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int autoId = 0;
		String query = "INSERT INTO product (subCategoryId,name,productShortDescription,productLongDescription,productAdditionalInformation,costprice,discount,colour,sellingPrice,productCode,brandId,tax,hsn) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, subcategoryId);
			ps.setString(2, product.getName());
			ps.setString(3, product.getProductShortDescription());
			ps.setString(4, product.getProductLongDescription());
			ps.setString(5, product.getProductAdditionalInformation());
			ps.setDouble(6, product.getCostPrice());
			ps.setString(7, Integer.toString(product.getDiscount()));
			ps.setString(8, product.getColour());
			ps.setDouble(9, product.getSellingPrice());
			ps.setString(10, product.getProductCode());
			ps.setInt(11, brandId);
			ps.setString(12, String.valueOf(product.getTax()));
			ps.setString(13, product.getHSNNumber());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {

				autoId = rs.getInt(1);
			}
			return autoId;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int insertSizeQuantity(int productId, int size, int quantity, int subcategoryId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		if (size != -1) {
			query = "insert into  sizequantitypivot (productId,quantity,subCategoryId,size)" + "VALUES (?,?,?,?)";
		} else {
			query = "insert into  sizequantitypivot (productId,quantity,subCategoryId,size)" + "VALUES (?,?,?,?)";
		}
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, productId);
			ps.setInt(2, quantity);
			ps.setInt(3, subcategoryId);
			if (size != -1)
				ps.setInt(4, size);
			else {
				ps.setInt(4, 0);
			}
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int insertIntoBrandTable(String brandName) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int brandId = 0;

		String query = "insert into  brand (brandName)" + "VALUES (?)";
		;
		try {
			ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, brandName);
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {

				brandId = rs.getInt(1);
			}
			return brandId;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int updateQuantity(int sizeId, int quantity) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "update  sizequantitypivot set quantity=quantity +" + quantity + " where sizeid=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, sizeId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static HashSet<String> getSizes(int subcategoryId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashSet<String> sizeSet = new HashSet<>();

		String query = "SELECT size from sizequantitypivot where subCategoryId= ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, subcategoryId);
			rs = ps.executeQuery();
			while (rs.next()) {
				String size = rs.getString("size");
				if (size != null && size.equalsIgnoreCase("null"))
					sizeSet.add(rs.getString("size"));
			}
			return sizeSet;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static HashSet<String> getColours(int subcategoryId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashSet<String> colourSet = new HashSet<>();

		String query = "SELECT colour from product where subCategoryId= ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, subcategoryId);
			rs = ps.executeQuery();
			while (rs.next()) {
				colourSet.add(rs.getString("colour"));
			}
			return colourSet;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static HashSet<String> getBrands(int subcategoryId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashSet<String> brandSet = new HashSet<>();

		String query = "SELECT brand.brandName from brand " + "INNER JOIN product on product.brandId= brand.brandId "
				+ " where product.subCategoryId= ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, subcategoryId);
			rs = ps.executeQuery();
			while (rs.next()) {
				brandSet.add(rs.getString("brandName"));
			}
			return brandSet;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int getProductCount(int subcategory) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT count(*) as rowcount  FROM product " + "where  subCategoryId=? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, subcategory);
			rs = ps.executeQuery();
			if (rs.next()) {

				return (rs.getInt("rowcount"));
			}

			return -1;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static String getBrandIDS(List brandList) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String brandIds = null;

		String query = "SELECT brandId from brand where  ";
		Iterator<String> itr = brandList.iterator();
		while (itr.hasNext()) {
			itr.next();
			query = query + " brandName = ?";
			if (itr.hasNext()) {
				query = query + " || ";
			}
		}
		try {
			ps = connection.prepareStatement(query);
			int i = 1;
			itr = brandList.iterator();
			while (itr.hasNext()) {
				ps.setString(i, itr.next());
				i++;
			}

			// ps.setString(1, brandName);
			rs = ps.executeQuery();
			while (rs.next()) {
				if (brandIds == null) {
					brandIds = rs.getString("brandId");
				} else {
					brandIds = brandIds + "," + rs.getString("brandId");
				}
			}
			return brandIds;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static ArrayList<Product> selectProductsFiltering(int subcategoryId, List<String> brandIdsList,
			List<String> colourList, List<String> productIDSListSize, int startIndex, int recordsPerPage) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT product.productId,product.name,product.productCode,product.costprice,product.sellingPrice,"
				+ "product.discount,product.colour FROM product "
				+ "where  subCategoryId =?";/*
											 * LIMIT " + (startIndex) + "," + recordPerpage;
											 */
		String brandQuery = "( ";
		if (brandIdsList != null && brandIdsList.size() > 0) {
			Iterator<String> itr = brandIdsList.iterator();
			while (itr.hasNext()) {
				itr.next();
				brandQuery = brandQuery + " brandId = ?";
				if (itr.hasNext()) {
					brandQuery = brandQuery + " || ";
				}
			}
			brandQuery = brandQuery + " )";
			query = query + " AND " + brandQuery;
		}
		String colorQuery = "( ";
		if (colourList != null && colourList.size() > 0) {
			Iterator<String> itrClr = colourList.iterator();
			while (itrClr.hasNext()) {
				itrClr.next();
				colorQuery = colorQuery + " colour = ?";
				if (itrClr.hasNext()) {
					colorQuery = colorQuery + " || ";
				}
			}
			colorQuery = colorQuery + " )";
			query = query + " AND " + colorQuery;
		}
		String sizeQuery = "( ";
		if (productIDSListSize != null && productIDSListSize.size() > 0) {
			Iterator<String> itrSize = productIDSListSize.iterator();
			while (itrSize.hasNext()) {
				itrSize.next();
				sizeQuery = sizeQuery + " productId = ?";
				if (itrSize.hasNext()) {
					sizeQuery = sizeQuery + " || ";
				}
			}
			sizeQuery = sizeQuery + " )";
			query = query + " AND " + sizeQuery;
		}
		// queryString.append(query);
		query = query + " LIMIT " + (startIndex) + "," + recordsPerPage;
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, subcategoryId);
			int i = 2;
			if (brandIdsList != null && brandIdsList.size() > 0) {
				Iterator<String> itr = brandIdsList.iterator();
				while (itr.hasNext()) {
					ps.setString(i, itr.next());
					i++;
				}
			}
			if (colourList != null && colourList.size() > 0) {
				Iterator<String> itr = colourList.iterator();
				while (itr.hasNext()) {
					ps.setString(i, itr.next());
					i++;
				}
			}
			if (productIDSListSize != null && productIDSListSize.size() > 0) {
				Iterator<String> itr = productIDSListSize.iterator();
				while (itr.hasNext()) {
					ps.setString(i, itr.next());
					i++;
				}
			}
			rs = ps.executeQuery();
			ArrayList<Product> products = new ArrayList<Product>();
			while (rs.next()) {
				Product p = new Product();
				p.setId(rs.getString("productId"));
				p.setName(rs.getString("name"));
				p.setCostPrice(rs.getDouble("costprice"));
				p.setSellingPrice(rs.getDouble("sellingPrice"));
				p.setDiscount(rs.getInt("discount"));
				// brandList.add(brand);
				String colour = rs.getString("colour");
				p.setProductCode(rs.getString("productCode"));
				p.setColour(colour);
				// colourSet.add(colour);
				// p.setDescription(rs.getString("ProductDescription"));
				// p.setPrice(rs.getDouble("ProductPrice"));
				products.add(p);
			}
			return products;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static String getProductIdsIDsForSizeFiltering(List sizeList, int subCategoryId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String productIds = null;

		String query = "SELECT productId from sizequantitypivot " + "where  subCategoryId =?";
		Iterator<String> itr = sizeList.iterator();
		String sizeQuery = " ( ";
		while (itr.hasNext()) {
			itr.next();
			sizeQuery = sizeQuery + " size = ?";
			if (itr.hasNext()) {
				sizeQuery = sizeQuery + " || ";
			}
		}
		sizeQuery = sizeQuery + " )";
		query = query + " AND " + sizeQuery;
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, subCategoryId);
			int i = 2;
			itr = sizeList.iterator();
			while (itr.hasNext()) {
				ps.setString(i, itr.next());
				i++;
			}

			// ps.setString(1, brandName);
			rs = ps.executeQuery();
			while (rs.next()) {
				if (productIds == null) {
					productIds = rs.getString("productId");
				} else {
					productIds = productIds + "," + rs.getString("productId");
				}
			}
			return productIds;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int selectProductsFilteringRowCount(int subcategoryId, List<String> brandIdsList,
			List<String> colourList, List<String> productIDSListSize) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT Count(*) as rowCount FROM product "
				+ "where  subCategoryId =?";/*
											 * LIMIT " + (startIndex) + "," + recordPerpage;
											 */
		String brandQuery = "( ";
		if (brandIdsList != null && brandIdsList.size() > 0) {
			Iterator<String> itr = brandIdsList.iterator();
			while (itr.hasNext()) {
				itr.next();
				brandQuery = brandQuery + " brandId = ?";
				if (itr.hasNext()) {
					brandQuery = brandQuery + " || ";
				}
			}
			brandQuery = brandQuery + " )";
			query = query + " AND " + brandQuery;
		}
		String colorQuery = "( ";
		if (colourList != null && colourList.size() > 0) {
			Iterator<String> itrClr = colourList.iterator();
			while (itrClr.hasNext()) {
				itrClr.next();
				colorQuery = colorQuery + " colour = ?";
				if (itrClr.hasNext()) {
					colorQuery = colorQuery + " || ";
				}
			}
			colorQuery = colorQuery + " )";
			query = query + " AND " + colorQuery;
		}
		String sizeQuery = "( ";
		if (productIDSListSize != null && productIDSListSize.size() > 0) {
			Iterator<String> itrSize = productIDSListSize.iterator();
			while (itrSize.hasNext()) {
				itrSize.next();
				sizeQuery = sizeQuery + " productId = ?";
				if (itrSize.hasNext()) {
					sizeQuery = sizeQuery + " || ";
				}
			}
			sizeQuery = sizeQuery + " )";
			query = query + " AND " + sizeQuery;
		}
		// queryString.append(query);
		// query = query + " LIMIT " + (startIndex) + "," + recordsPerPage;
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, subcategoryId);
			int i = 2;
			if (brandIdsList != null && brandIdsList.size() > 0) {
				Iterator<String> itr = brandIdsList.iterator();
				while (itr.hasNext()) {
					ps.setString(i, itr.next());
					i++;
				}
			}
			if (colourList != null && colourList.size() > 0) {
				Iterator<String> itr = colourList.iterator();
				while (itr.hasNext()) {
					ps.setString(i, itr.next());
					i++;
				}
			}
			if (productIDSListSize != null && productIDSListSize.size() > 0) {
				Iterator<String> itr = productIDSListSize.iterator();
				while (itr.hasNext()) {
					ps.setString(i, itr.next());
					i++;
				}
			}
			rs = ps.executeQuery();
			ArrayList<Product> products = new ArrayList<Product>();
			if (rs.next()) {
				return (rs.getInt("rowCount"));
			}
			return -1;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static ArrayList<String> getProducReviews(String productId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "SELECT review from productreview " + " where  productId=? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, productId);
			rs = ps.executeQuery();
			ArrayList<String> productReview = new ArrayList();
			while (rs.next()) {
				productReview.add(rs.getString("review"));

			}
			return productReview;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int updateProduct(Product product, int brandId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int autoId = 0;
		String query = "update product set name=?,productShortDescription=?,productLongDescription=?,productAdditionalInformation=?,costprice=?,discount=?,colour=?,sellingPrice=?,brandId=?,tax=?,hsn=?"
				+ " where productId=?";
		try {
			ps = connection.prepareStatement(query);
			// ps.setInt(1, subcategoryId);
			ps.setString(1, product.getName());
			ps.setString(2, product.getProductShortDescription());
			ps.setString(3, product.getProductLongDescription());
			ps.setString(4, product.getProductAdditionalInformation());
			ps.setDouble(5, product.getCostPrice());
			ps.setString(6, Integer.toString(product.getDiscount()));
			ps.setString(7, product.getColour());
			ps.setDouble(8, product.getSellingPrice());
			// ps.setString(9, product.getProductCode());
			ps.setInt(9, brandId);
			ps.setString(10, String.valueOf(product.getTax()));
			ps.setString(11, product.getHSNNumber());
			ps.setString(12, product.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}
	public static int updateSizeQuantity(int sizeId, int quantity) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "update  sizequantitypivot set quantity=" + quantity + " where sizeid=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, sizeId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

}