package com.ecommerce.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class QueryUtil {

	public static void generateWhereClauseForBrand(
			ArrayList<String> brandFilter, StringBuilder brandClause) {
		for (int i = 0; i < brandFilter.size(); i++) {
			String brandId = ProductDB.getBrandID(brandFilter.get(i));
			brandClause.append("\' " + brandId + " \'");
			brandClause.append(",");

		}

	}

	public static void generateWhereClauseForProduct(HashSet<String> wordSet,
			StringBuilder whereClause) {
		// whereClause already has where statement
		// iterate over set and form || with wildcards
		int size = wordSet.size();
		Iterator<String> itr = wordSet.iterator();
		while (itr.hasNext()) {
			String word = itr.next();
			word = "\'%" + word + "%\'";
			whereClause.append(" product.name LIKE ");
			whereClause.append(word);
			whereClause.append(" OR ");
		}
		// return whereClause;

	}
}
