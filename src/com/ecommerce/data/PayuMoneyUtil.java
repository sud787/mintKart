package com.ecommerce.data;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PayuMoneyUtil {
	private static final String merchant_key = "ExznaBO5";
	private static final String salt = "WOls62kajs";
	private static final String service_provider = "payu_paisa";
	private String action1 = "";
	private String base_url = "https://sandboxsecure.payu.in";
	int error = 0;
	private String hashString;
	private String txnid;
	private static final String surl = "http://localhost:8080/mintKart/sucessHandler";
	private static final String furl = "http://localhost:8080/mintKart/paymnetFailureHandler";
	private float amount;

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public static String getSurl() {
		return surl;
	}

	public static String getFurl() {
		return furl;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getAction1() {
		return action1;
	}

	public void setAction1(String action1) {
		this.action1 = action1;
	}

	public String getBase_url() {
		return base_url;
	}

	public void setBase_url(String base_url) {
		this.base_url = base_url;
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}

	public String getHashString() {
		return hashString;
	}

	public void setHashString(String hashString) {
		this.hashString = hashString;
	}

	public static String getMerchantKey() {
		return merchant_key;
	}

	public static String getSalt() {
		return salt;
	}

	public static String getServiceProvider() {
		return service_provider;
	}

	public String hashCal(String type, String str) {
		byte[] hashseq = str.getBytes();
		StringBuffer hexString = new StringBuffer();
		try {
			MessageDigest algorithm = MessageDigest.getInstance(type);
			algorithm.reset();
			algorithm.update(hashseq);
			byte messageDigest[] = algorithm.digest();

			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1)
					hexString.append("0");
				hexString.append(hex);
			}

		} catch (NoSuchAlgorithmException nsae) {
		}

		return hexString.toString();

	}

}
