package com.ecommerce.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ecommerce.business.Address;
import com.ecommerce.business.Cart;
import com.ecommerce.business.LineItem;
import com.ecommerce.business.Product;

public class CartDB {

	public static int addToCart(LineItem item, int cartId, int sizeId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "INSERT INTO cartdetails (cartId,productId,sizeid,quantity) " + "VALUES (?,?,?,?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, cartId);
			ps.setString(2, item.getProduct().getId());
			ps.setInt(3, sizeId);
			ps.setInt(4, item.getQuantity());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static Cart getCart(String userId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Cart cart = new Cart();
		ArrayList<LineItem> items = new ArrayList();
		String query = "SELECT product.productId,product.productCode,product.colour,product.name,product.costprice,product.sellingPrice,product.discount,product.productShortDescription,product.productLongDescription, "
				+ "product.productAdditionalInformation,product.discount,brand.brandName,sizequantitypivot.size,cartdetails.quantity FROM cart "
				+ "INNER JOIN cartdetails on cartdetails.cartId = cart.cartId "
				+ "INNER JOIN product on product.productId = cartdetails.productId  "
				+ "INNer JOIN sizequantitypivot on sizequantitypivot.sizeid=cartdetails.sizeid "
				+ "INNER JOIN brand on product.brandId=brand.brandId " + "where userId= ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				LineItem item = new LineItem();
				Product p = new Product();
				p.setId(rs.getString("productId"));
				p.setName(rs.getString("name"));
				p.setSellingPrice(rs.getDouble("sellingPrice"));
				p.setCostPrice(rs.getDouble("costprice"));
				p.setDiscount(rs.getInt("discount"));
				p.setProductShortDescription(rs.getString("productShortDescription"));
				p.setProductLongDescription(rs.getString("productLongDescription"));
				p.setProductAdditionalInformation(rs.getString("productAdditionalInformation"));
				p.setDiscount(rs.getInt("discount"));
				p.setBrand(rs.getString("brandName"));
				p.setColour(rs.getString("colour"));
				p.setProductCode(rs.getString("productCode"));
				item.setProduct(p);
				item.setSize(rs.getInt("size"));
				item.setQuantity(rs.getInt("quantity"));
				items.add(item);

			}
			cart.setItems(items);
			return cart;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static Cart getCart(int cartid) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Cart cart = new Cart();
		int cartId = 0;
		ArrayList<LineItem> items = new ArrayList();
		String query = "SELECT cart.cartId,product.productId,product.productCode,product.colour,product.name,product.costprice,product.sellingPrice,product.discount,product.productShortDescription,product.productLongDescription, "
				+ "product.productAdditionalInformation,product.discount,brand.brandName,sizequantitypivot.size,cartdetails.quantity FROM cart "
				+ "INNER JOIN cartdetails on cartdetails.cartId = cart.cartId "
				+ "INNER JOIN product on product.productId = cartdetails.productId  "
				+ "INNer JOIN sizequantitypivot on sizequantitypivot.sizeid=cartdetails.sizeid "
				+ "INNER JOIN brand on product.brandId=brand.brandId " + "where cart.cartId= ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, cartid);
			rs = ps.executeQuery();
			while (rs.next()) {
				LineItem item = new LineItem();
				Product p = new Product();
				p.setId(rs.getString("productId"));
				p.setName(rs.getString("name"));
				p.setSellingPrice(rs.getDouble("sellingPrice"));
				p.setCostPrice(rs.getDouble("costprice"));
				p.setDiscount(rs.getInt("discount"));
				p.setProductShortDescription(rs.getString("productShortDescription"));
				p.setProductLongDescription(rs.getString("productLongDescription"));
				p.setProductAdditionalInformation(rs.getString("productAdditionalInformation"));
				p.setDiscount(rs.getInt("discount"));
				p.setBrand(rs.getString("brandName"));
				p.setColour(rs.getString("colour"));
				p.setProductCode(rs.getString("productCode"));
				cartId = rs.getInt("cartId");
				item.setProduct(p);
				item.setSize(rs.getInt("size"));
				item.setQuantity(rs.getInt("quantity"));
				items.add(item);

			}
			cart.setItems(items);
			return cart;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int insertIntocart(String userId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Cart cart = new Cart();
		ArrayList<LineItem> items = new ArrayList();
		String query = "insert into cart (userId) values (?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, userId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int getCartId(String userId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Cart cart = new Cart();
		int cartId = 0;
		ArrayList<LineItem> items = new ArrayList();
		String query = "select cartId from  cart where userId= ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				cartId = rs.getInt("cartId");
			}
			return cartId;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int updateQuantityCart(LineItem item, int cartId,int quantity) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "update cartdetails set cartdetails.quantity=cartdetails.quantity+"+quantity+" where cartid=? and productId=? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, cartId);
			ps.setString(2, item.getProduct().getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	// overloaded method
	public static int UpdateQuantityCart(String quantity, int cartId, String productId, int sizeId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "update cartdetails set quantity=" + quantity + " where cartid=? and productId=? and sizeid=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, cartId);
			ps.setString(2, productId);
			ps.setInt(3, sizeId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int updateGuestToLoginCart(int guestCartID, int loggedInCartId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "update cartdetails set cartdetails.cartId =? where cartid=?  ";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, loggedInCartId);
			ps.setInt(2, guestCartID);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static void updateGuestToLoginProductHandling(int guestCartID, int loggedInCartId) {
		Cart guestUserCart = getCart(guestCartID);
		ArrayList<LineItem> guestUserItem = guestUserCart.getItems();
		Cart loggedinUserCart = getCart(loggedInCartId);
		ArrayList<LineItem> loggedinUserItem = loggedinUserCart.getItems();
		for (int i = 0; i < loggedinUserItem.size(); i++) {
			for (int j = 0; j < guestUserItem.size(); j++) {
				if (loggedinUserItem.get(i).getProduct().getId().equals(guestUserItem.get(j).getProduct().getId())
						&& loggedinUserItem.get(i).getSize() == guestUserItem.get(j).getSize()) {
					int totalQuantity = loggedinUserItem.get(i).getQuantity() + guestUserItem.get(j).getQuantity();
					int sizeId = ProductDB.getSizeID(loggedinUserItem.get(i).getProduct().getId(),
							loggedinUserItem.get(i).getSize());
					UpdateQuantityCart(Integer.toString(totalQuantity), loggedInCartId,
							loggedinUserItem.get(i).getProduct().getId(), sizeId);
					removeFromCart(guestCartID, guestUserItem.get(j).getProduct().getId(), sizeId);

				}
			}
		}
		updateGuestToLoginCart(guestCartID, loggedInCartId);

	}

	public static int clearCart(String userId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "delete from cart where userId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, userId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int removeFromCart(int cartId, String productId, int sizeid) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "delete from cartdetails where cartId=? and productId=? and sizeid=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, cartId);
			ps.setString(2, productId);
			ps.setInt(3, sizeid);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int isCartEmpty(int cartId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select Count(*) as count from cartdetails where cartId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, cartId);

			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("count");
			}
			return -1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int getQuantity(String productId, int size) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int quantity = -1;
		String query = "select quantity from sizequantitypivot where size=? and productId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, size);
			ps.setString(2, productId);
			rs = ps.executeQuery();
			if (rs.next()) {
				quantity = rs.getInt("quantity");
			}
			return quantity;
		} catch (SQLException e) {
			e.printStackTrace();
			return quantity;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int removeCartEntry(int cartId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "delete from cart where cartId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, cartId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

}
