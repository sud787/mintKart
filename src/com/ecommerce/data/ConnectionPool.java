package com.ecommerce.data;

import java.sql.*;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;

public class ConnectionPool {
	private volatile static ConnectionPool pool = null;
	private static DataSource dataSource = null;

	private ConnectionPool() {
		try {
			InitialContext ic = new InitialContext();
			Context envContext = (Context) ic.lookup("java:/comp/env");
			dataSource = (DataSource) envContext.lookup("jdbc/mintKart");
			// (DataSource) ic.lookup("java:/comp/env/jdbc/mintKart");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * "double-checked locking" to reduce the use of synchronization in
	 * getInstance()
	 */
	public static ConnectionPool getInstance() {
		if (pool == null) {
			synchronized (ConnectionPool.class) {
				// once in the block,check again
				if (pool == null) {
					pool = new ConnectionPool();
				}
			}
		}
		return pool;
	}

	public Connection getConnection() {
		try {
			return dataSource.getConnection();
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			return null;
		}
	}

	public void freeConnection(Connection c) {
		try {
			c.close();
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
}
