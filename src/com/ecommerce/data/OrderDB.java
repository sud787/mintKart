package com.ecommerce.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.ecommerce.business.Address;
import com.ecommerce.business.Cart;
import com.ecommerce.business.LineItem;
import com.ecommerce.business.Order;
import com.ecommerce.business.Product;
import com.mysql.jdbc.Statement;

public class OrderDB {

	public static int getOrderId(String userId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Cart cart = new Cart();
		int cartId = 0;
		ArrayList<LineItem> items = new ArrayList();
		String query = "select orderId from  ordertable where userId= ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				cartId = rs.getInt("cartId");
			}
			return cartId;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int insertIntoOrder(String userId, Date sqlDate, String orderStatus, String payementMethod,
			double tax, double shipmentCharge, double productCost, double totalCost, String invoiceId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Timestamp timestamp = new Timestamp(sqlDate.getTime());
		int autoId = 0;

		String query = "insert into ordertable (userId,orderDate,oderStatus,payementMethod,tax,shippementCharges,productCost,totalCost,invoiceid) values (?,?,?,?,?,?,?,?,?)";
		try {
			ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, userId);
			ps.setTimestamp(2, timestamp);
			ps.setString(3, orderStatus);
			ps.setString(4, payementMethod);
			ps.setDouble(5, tax);
			ps.setDouble(6, shipmentCharge);
			ps.setDouble(7, productCost);
			ps.setDouble(8, totalCost);
			ps.setString(9, invoiceId);
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {

				autoId = rs.getInt(1);
			}
			return autoId;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	// online payment overloadedMethod
	public static int insertIntoOrder(String userId, Date sqlDate, String orderStatus, String payementMethod,
			double tax, double shipmentCharge, double productCost, double totalCost, String invoiceId,
			String payementId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Timestamp timestamp = new Timestamp(sqlDate.getTime());
		int autoId = 0;

		String query = "insert into ordertable (userId,orderDate,oderStatus,payementMethod,tax,shippementCharges,productCost,totalCost,invoiceid,payementId) values (?,?,?,?,?,?,?,?,?,?)";
		try {
			ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, userId);
			ps.setTimestamp(2, timestamp);
			ps.setString(3, orderStatus);
			ps.setString(4, payementMethod);
			ps.setDouble(5, tax);
			ps.setDouble(6, shipmentCharge);
			ps.setDouble(7, productCost);
			ps.setDouble(8, totalCost);
			ps.setString(9, invoiceId);
			ps.setString(10, payementId);

			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {

				autoId = rs.getInt(1);
			}
			return autoId;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int addOrder(int orderId, String productId, int sizeid, int quantity, double price) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		String query = "insert into orderdetails (orderId,productId,sizeid,quantity,price) values (?,?,?,?,?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, orderId);
			ps.setString(2, productId);
			ps.setInt(3, sizeid);
			ps.setInt(4, quantity);
			ps.setDouble(5, price);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static boolean getorderTable(int orderId, Order order) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Address address = new Address();
		String query = "select ordertable.orderDate,ordertable.oderStatus,ordertable.payementMethod,ordertable.tax,ordertable.shippementCharges,ordertable.productCost,ordertable.totalCost,ordertable.trackingId,ordertable.invoiceid ,"
				+ "address.phoneNumber,address.email,address.city,address.pincode,address.state,address.address,address.name FROM ordertable "
				+ "INNER JOIN address on address.OrderId = ordertable.orderId " + "where ordertable.orderId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, orderId);
			rs = ps.executeQuery();
			if (rs.next()) {
				// oder.setOrderDate((rs.getString("productId"));
				Timestamp timestamp = rs.getTimestamp("orderDate");
				Date date = new Date(timestamp.getTime());
				order.setOrderDate(date);
				order.setOderStatus((rs.getString("oderStatus")));
				order.setPayementMethod((rs.getString("payementMethod")));
				order.setTax(rs.getDouble("tax"));
				order.setShippementCharges(rs.getDouble("shippementCharges"));
				order.setProductCost(rs.getDouble("productCost"));
				order.setTotalCost(rs.getDouble("totalCost"));
				order.setTrackingId(rs.getString("trackingId"));
				order.setInvoiceId(rs.getString("invoiceid"));
				address.setNumber(rs.getString("phoneNumber"));
				address.setEmail(rs.getString("email"));
				address.setCity(rs.getString("city"));
				address.setPincode(rs.getString("pincode"));
				address.setState(rs.getString("state"));
				address.setAddress(rs.getString("address"));
				address.setName(rs.getString("name"));
				order.setAddress(address);

			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static ArrayList<LineItem> getOrderItemDetails(int orderId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ArrayList<LineItem> items = new ArrayList();
		ResultSet rs = null;
		String query = "select productreview.reviewId,product.productId,product.productCode,product.colour,product.name,product.productShortDescription,sizequantitypivot.size,orderdetails.quantity,orderdetails.price,orderdetails.isOrderCancelled FROM orderdetails "
				+ "INNER JOIN product on product.productId = orderdetails.productId "
				+ " LEFT JOIN productreview ON productreview.productId=orderdetails.productId  and productreview.orderId= orderdetails.orderId "
				+ "INNer JOIN sizequantitypivot on sizequantitypivot.sizeid=orderdetails.sizeid "
				+ "where orderdetails.orderId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, orderId);
			rs = ps.executeQuery();
			while (rs.next()) {
				LineItem item = new LineItem();
				Product p = new Product();
				p.setId(rs.getString("productId"));
				p.setName(rs.getString("name"));
				p.setSellingPrice(rs.getDouble("price"));
				p.setColour(rs.getString("colour"));
				p.setProductShortDescription(rs.getString("productShortDescription"));

				p.setProductCode(rs.getString("productCode"));
				if (rs.getString("isOrderCancelled") != null) {
					p.setIsOrderCancelled(rs.getString("isOrderCancelled"));
				}
				item.setProduct(p);
				item.setSize(rs.getInt("size"));
				item.setQuantity(rs.getInt("quantity"));
				item.setIsProductReviewed(rs.getString("reviewId"));
				items.add(item);
			}
			return items;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}
	// public static

	public static ArrayList<Order> getOrderList(String userId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ArrayList<Order> orderList = new ArrayList();
		ResultSet rs = null;
		String query = "select orderId,orderDate,oderStatus,payementMethod,tax,shippementCharges,productCost,totalCost from "
				+ "ordertable  where userId=? order by orderDate desc";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Order order = new Order();
				order.setOrderId(rs.getInt("orderId"));
				Timestamp orderdate = rs.getTimestamp("orderDate");
				Date date = new Date(orderdate.getTime());
				order.setOrderDate(date);
				order.setOderStatus(rs.getString("oderStatus"));
				order.setPayementMethod(rs.getString("payementMethod"));
				order.setTax(rs.getDouble("tax"));
				order.setShippementCharges(rs.getDouble("shippementCharges"));
				order.setProductCost(rs.getDouble("productCost"));
				order.setTotalCost(rs.getDouble("totalCost"));
				orderList.add(order);

			}
			return orderList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static ArrayList<Order> getPendingOrder() {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ArrayList<Order> orderList = new ArrayList();
		ResultSet rs = null;
		// Date todate = new Date();
		// java.sql.Date sqlDate = new java.sql.Date(todate.getTime());
		String query = "select orderId,orderDate,oderStatus,payementMethod,tax,shippementCharges,productCost,totalCost from ordertable where oderStatus = ? order by orderDate desc";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, "Order Placed");
			rs = ps.executeQuery();
			while (rs.next()) {
				Order order = new Order();
				order.setOrderId(rs.getInt("orderId"));
				java.sql.Date dbSqlDate = rs.getDate("orderDate");
				Date date = new Date(dbSqlDate.getTime());
				order.setOrderDate(date);
				order.setOderStatus(rs.getString("oderStatus"));
				order.setPayementMethod(rs.getString("payementMethod"));
				order.setTax(rs.getDouble("tax"));
				order.setShippementCharges(rs.getDouble("shippementCharges"));
				order.setProductCost(rs.getDouble("productCost"));
				order.setTotalCost(rs.getDouble("totalCost"));
				orderList.add(order);
			}
			return orderList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static ArrayList<Order> getShippedOrder() {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ArrayList<Order> orderList = new ArrayList();
		ResultSet rs = null;
		// Date todate = new Date();
		// java.sql.Date sqlDate = new java.sql.Date(todate.getTime());
		String query = "select orderId,orderDate,oderStatus,payementMethod,tax,shippementCharges,productCost,totalCost,trackingId from ordertable where oderStatus = ? order by orderDate desc";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, "Shipped");
			rs = ps.executeQuery();
			while (rs.next()) {
				Order order = new Order();
				order.setOrderId(rs.getInt("orderId"));
				java.sql.Date dbSqlDate = rs.getDate("orderDate");
				Date date = new Date(dbSqlDate.getTime());
				order.setOrderDate(date);
				order.setOderStatus(rs.getString("oderStatus"));
				order.setPayementMethod(rs.getString("payementMethod"));
				order.setTax(rs.getDouble("tax"));
				order.setShippementCharges(rs.getDouble("shippementCharges"));
				order.setProductCost(rs.getDouble("productCost"));
				order.setTotalCost(rs.getDouble("totalCost"));
				order.setTrackingId(rs.getString("trackingId"));
				orderList.add(order);
			}
			return orderList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static ArrayList<Order> getDeliveredOrder() {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ArrayList<Order> orderList = new ArrayList();
		ResultSet rs = null;
		// Date todate = new Date();
		// java.sql.Date sqlDate = new java.sql.Date(todate.getTime());
		String query = "select orderId,orderDate,oderStatus,payementMethod,tax,shippementCharges,productCost,totalCost,trackingId from ordertable where oderStatus = ? order by orderDate desc";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, "Order Delivered");
			rs = ps.executeQuery();
			while (rs.next()) {
				Order order = new Order();
				order.setOrderId(rs.getInt("orderId"));
				java.sql.Date dbSqlDate = rs.getDate("orderDate");
				Date date = new Date(dbSqlDate.getTime());
				order.setOrderDate(date);
				order.setOderStatus(rs.getString("oderStatus"));
				order.setPayementMethod(rs.getString("payementMethod"));
				order.setTax(rs.getDouble("tax"));
				order.setShippementCharges(rs.getDouble("shippementCharges"));
				order.setProductCost(rs.getDouble("productCost"));
				order.setTotalCost(rs.getDouble("totalCost"));
				order.setTrackingId(rs.getString("trackingId"));
				orderList.add(order);
			}
			return orderList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static ArrayList<Order> getUnDeliveredOrder() {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ArrayList<Order> orderList = new ArrayList();
		ResultSet rs = null;
		// Date todate = new Date();
		// java.sql.Date sqlDate = new java.sql.Date(todate.getTime());
		String query = "select orderId,orderDate,oderStatus,payementMethod,tax,shippementCharges,productCost,totalCost,trackingId from ordertable where oderStatus = ? order by orderDate desc";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, "Delivery Attempt Failed");
			rs = ps.executeQuery();
			while (rs.next()) {
				Order order = new Order();
				order.setOrderId(rs.getInt("orderId"));
				java.sql.Date dbSqlDate = rs.getDate("orderDate");
				Date date = new Date(dbSqlDate.getTime());
				order.setOrderDate(date);
				order.setOderStatus(rs.getString("oderStatus"));
				order.setPayementMethod(rs.getString("payementMethod"));
				order.setTax(rs.getDouble("tax"));
				order.setShippementCharges(rs.getDouble("shippementCharges"));
				order.setProductCost(rs.getDouble("productCost"));
				order.setTotalCost(rs.getDouble("totalCost"));
				order.setTrackingId(rs.getString("trackingId"));
				orderList.add(order);
			}
			return orderList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int cancelOrder(String orderId, String productId, int sizeid) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "UPDATE  orderdetails set isOrderCancelled=? where orderId=? and productId=? and sizeid=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, "Yes");
			ps.setString(2, orderId);
			ps.setString(3, productId);
			ps.setInt(4, sizeid);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static HashMap<String, Double> getOrderCost(int orderId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		HashMap<String, Double> costMap = new HashMap<>();
		ResultSet rs = null;
		String query = "select productCost,totalCost  from ordertable where orderId = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, orderId);
			rs = ps.executeQuery();
			if (rs.next()) {
				double productCost = rs.getDouble("productCost");
				double totalCost = rs.getDouble("totalCost");
				costMap.put("productCost", productCost);
				costMap.put("totalCost", totalCost);
			}
			return costMap;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int cancelFullOrder(String orderId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "UPDATE  ordertable set tax=?,shippementCharges=?, productCost=?,totalCost=?,oderStatus=? where orderId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setDouble(1, 0);
			ps.setDouble(2, 0);
			ps.setDouble(3, 0);
			ps.setDouble(4, 0);
			ps.setString(5, "Cancelled");
			ps.setString(6, orderId);

			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int updateOrderPrice(String orderId, Double totalCost, Double productCost) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "UPDATE  ordertable set  productCost=?,totalCost=?where orderId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setDouble(1, productCost);
			ps.setDouble(2, totalCost);
			ps.setString(3, orderId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static HashMap<String, String> getMailingDetails(String orderId) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select ordertable.totalCost,address.email  from ordertable INNER JOIN address on address.OrderId=ordertable.orderId where ordertable.orderId=? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, orderId);
			rs = ps.executeQuery();
			if (rs.next()) {
				HashMap<String, String> emailCostMap = new HashMap<>();
				String totalCost = rs.getString("totalCost");
				String emailAddress = rs.getString("email");
				emailCostMap.put("totalCost", totalCost);
				emailCostMap.put("emailAddress", emailAddress);
				return emailCostMap;
			}
			// return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

		return null;

	}

	public static String getTrackingId(String orderId) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select trackingId  from ordertable where ordertable.orderId=? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, orderId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("trackingId");
			}
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

		// return null;

	}

	public static String getUserIdFromOrderId(String orderId) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select userId from ordertable where orderId=? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, orderId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("userId");
			}
			// return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

		return null;

	}

	public static int addProductReview(String productId, String orderId, String reviewMessage) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "insert into productreview (orderid,productId,review) values(?,?,?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, orderId);
			ps.setString(2, productId);
			ps.setString(3, reviewMessage);
			return ps.executeUpdate();
			// return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int updateOrderToShipped(String orderId, String trackingId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "UPDATE  ordertable set  oderStatus=?,trackingId=? where orderId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, "Shipped");
			ps.setString(2, trackingId);
			ps.setString(3, orderId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static int updateOrderDeliveryStatus(String orderId, String deliveryStatus) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "UPDATE  ordertable set  oderStatus=? where orderId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, deliveryStatus);
			// ps.setString(2, trackingId);
			ps.setString(2, orderId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static String getPaymentMethod(String orderId) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select  payementMethod from ordertable where orderid=?";
		try {
			ps = connection.prepareStatement(query);
			// ps.setString(2, trackingId);
			ps.setString(1, orderId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("payementMethod");
			}
			return null;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}
	public static String getPaymentId(String orderId) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select  payementId from ordertable where orderid=?";
		try {
			ps = connection.prepareStatement(query);
			// ps.setString(2, trackingId);
			ps.setString(1, orderId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("payementId");
			}
			return null;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

}
