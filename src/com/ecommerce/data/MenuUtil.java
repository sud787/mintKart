package com.ecommerce.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.ecommerce.business.Product;

public class MenuUtil {

	public static ArrayList<String> getSubcategoryMenu(String topMenuNeame) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT subcategory.subcategoryName FROM topcategory"
				+ " INNER JOIN category on topcategory.id=category.topcategoryid"
				+ " INNER JOIN subcategory on subcategory.categoryId=category.categoryId" + " where topcategory.name=?";
		// fetch topcategory id and corresponding subcategory ids and
		// corresponding products related with subcategory ids
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, topMenuNeame);
			rs = ps.executeQuery();
			if (rs.next()) {
				ArrayList<String> subCategory = new ArrayList<>();

				subCategory.add(rs.getString("subcategoryName"));
				while (rs.next()) {
					subCategory.add(rs.getString("subcategoryName"));
				}
				return subCategory;
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}
	public static HashMap<Integer,String> getSubcategoryList() {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT subcategoryid,subcategoryName FROM subcategory";
		try {
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				HashMap<Integer,String> subCategoryMap = new HashMap<>();

				subCategoryMap.put(rs.getInt("subcategoryid"),rs.getString("subcategoryName"));
				while (rs.next()) {
					subCategoryMap.put(rs.getInt("subcategoryid"),rs.getString("subcategoryName"));
				}
				return subCategoryMap;
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static ArrayList<String> getSubcategory(String category) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT subcategory.subcategoryName FROM category"
				+ " INNER JOIN subcategory on subcategory.categoryId=category.categoryId"
				+ " where category.categoryName=?";
		// fetch topcategory id and corresponding subcategory ids and
		// corresponding products related with subcategory ids
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, category);
			rs = ps.executeQuery();
			ArrayList<String> subCategory = new ArrayList<>();
			while (rs.next()) {
				subCategory.add(rs.getString("subcategoryName"));
			}
			return subCategory;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static ArrayList<String> getCategories(String subcategory) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// select subcategoryName from subcategory where categoryId IN(select
		// categoryId from subcategory where subcategoryName='loafers' );
		String query = "select subcategoryName from subcategory where categoryId IN"
				+ "(select categoryId from subcategory where subcategoryName=? )";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, subcategory);
			rs = ps.executeQuery();
			if (rs.next()) {
				ArrayList<String> categoryList = new ArrayList<>();

				categoryList.add(rs.getString("subcategoryName"));
				while (rs.next()) {
					categoryList.add(rs.getString("subcategoryName"));
				}
				return categoryList;
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static ArrayList<String> getSubCategoryBrands(String subcategory) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		// select subcategoryName from subcategory where categoryId IN(select
		// categoryId from subcategory where subcategoryName='loafers' );
		String query = "select brand.brandName from subcategory "
				+ " INNER JOIN brandsubcategorypivot on subcategory.subcategoryid=brandsubcategorypivot.subcategoryid"
				+ " INNER JOIN brand on brand.brandId=brandsubcategorypivot.brandId"
				+ " where subcategory.subcategoryName=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, subcategory);
			rs = ps.executeQuery();
			if (rs.next()) {
				ArrayList<String> brandList = new ArrayList<>();

				brandList.add(rs.getString("brandName"));
				while (rs.next()) {
					brandList.add(rs.getString("brandName"));
				}
				return brandList;
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static ArrayList<String> getTopCategory() {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> topCategory = new ArrayList<>();
		// select subcategoryName from subcategory where categoryId IN(select
		// categoryId from subcategory where subcategoryName='loafers' );
		String query = "select name from topcategory ";
		// +
		// " INNER JOIN brandsubcategorypivot on
		// subcategory.subcategoryid=brandsubcategorypivot.subcategoryid"
		// + " INNER JOIN brand on brand.brandId=brandsubcategorypivot.brandId"
		// + " where subcategory.subcategoryName=?";
		try {
			ps = connection.prepareStatement(query);
			// ps.setString(1, subcategory);
			rs = ps.executeQuery();
			while (rs.next()) {

				topCategory.add(rs.getString("name"));

				// return brandList;
			}
			return topCategory;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static ArrayList<String> getCategory(String topMenuNeame) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> categoryList = new ArrayList<>();
		HashMap<String, ArrayList<String>> categorySubCategoryMap = new HashMap<String, ArrayList<String>>();
		String query = "SELECT category.categoryName FROM topcategory"
				+ " INNER JOIN category on topcategory.id=category.topcategoryid" + " where topcategory.name=?";
		// fetch topcategory id and corresponding subcategory ids and
		// corresponding products related with subcategory ids
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, topMenuNeame);
			rs = ps.executeQuery();
			while (rs.next()) {

				categoryList.add(rs.getString("categoryName"));

			}
			return categoryList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int getSubCategoryId(String subcategory) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> topCategory = new ArrayList<>();
		// select subcategoryName from subcategory where categoryId IN(select
		// categoryId from subcategory where subcategoryName='loafers' );
		String query = "select subcategoryid from subcategory where subcategoryName=? ";
		// +
		// " INNER JOIN brandsubcategorypivot on
		// subcategory.subcategoryid=brandsubcategorypivot.subcategoryid"
		// + " INNER JOIN brand on brand.brandId=brandsubcategorypivot.brandId"
		// + " where subcategory.subcategoryName=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, subcategory);
			rs= ps.executeQuery();
			if(rs.next()) {
				return rs.getInt("subcategoryid");
			}
			return -1;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

	public static int addTopCategory(String topCategoryName) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "insert into  topcategory (name) values(?)  ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, topCategoryName);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}
	public static int addCategory(String categoryName,int topCategoryId) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "insert into category (categoryName,topcategoryid) values(?,?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, categoryName);
			ps.setInt(2, topCategoryId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}
	public static int addSubCategory(String subCategoryName,int categoryId) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "insert into subcategory (subcategoryName,categoryId) values(?,?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, subCategoryName);
			ps.setInt(2, categoryId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}
	public static int getTopCategoryId(String topCategory) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select id from topcategory where name=? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, topCategory);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("id");
			}
			return -1;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}
	public static int geCategoryId(String category) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select categoryId from category where categoryName=? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, category);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("id");
			}
			return -1;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}
	public static int getCategoryId(String category) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select categoryId from category where categoryName=? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, category);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("categoryId");
			}
			return -1;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}
	public static int updateTopCategory(String updatetopCategoryName,int topCategoryId) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "update  topcategory set name=? where id=?  ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, updatetopCategoryName);
			ps.setInt(2, topCategoryId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}
	public static int updateCategory(String updatedCategoryName,int categoryId) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "update  category set categoryName=? where categoryId=?  ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, updatedCategoryName);
			ps.setInt(2, categoryId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}
	public static int updateSubCategory(String updatedSubCategoryName,int subCategoryId) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "update  subcategory set subcategoryName=? where subcategoryid=?  ";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, updatedSubCategoryName);
			ps.setInt(2, subCategoryId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}
	}

}
