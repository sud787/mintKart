package com.ecommerce.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ecommerce.business.Address;

public class AddressDB {
	public static int insertIntoAddress(String userId, Address address, int insertedid) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "insert into address (phoneNumber,email,city,pincode,state,address,userId,name,OrderId) values (?,?,?,?,?,?,?,?,?)";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, address.getNumber());
			ps.setString(2, address.getEmail());
			ps.setString(3, address.getCity());
			ps.setString(4, address.getPincode());
			ps.setString(5, address.getState());
			ps.setString(6, address.getAddress());
			ps.setString(7, userId);
			ps.setString(8, address.getName());
			ps.setInt(9, insertedid);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

	public static String getMailingAddress(String orderId) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select email from  address where OrderId=?";
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, orderId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("email");
			}
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.closeResultSet(rs);
			DBUtil.closePreparedStatement(ps);
			pool.freeConnection(connection);
		}

	}

}
