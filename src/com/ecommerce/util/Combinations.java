package com.ecommerce.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Combinations {
	public static HashSet<String> formWord(String input) {
		final Set<Character> inputSet = new HashSet<>();
		for (int i = 0; i < input.length(); i++) {
			inputSet.add(input.charAt(i));
		}
		// Use the method to compute the power set
		Set<Set<Character>> powerSet = powerSet(inputSet);
		HashSet<String> wordsSet = new HashSet<>();
		for (Set<Character> element : powerSet) {
			// Combine the character in the set to a String
			StringBuilder sb = new StringBuilder();
			for (Character c : element) {
				sb.append(c);
			}

			// Here is a final element ready for collection or a print
			String outputElement = sb.toString();
			// The method already prints the results by itself, you can modify
			// it easily such that it returns a Set<String> or similar
			permutation(outputElement, wordsSet);
			System.out.println("set elements" + wordsSet);
		}
		return wordsSet;
	}

	public static void permutation(String str, HashSet<String> wordsSet) {
		permutation("", str, wordsSet);
	}

	private static void permutation(String prefix, String str,
			HashSet<String> wordsSet) {
		int n = str.length();
		if (n == 0 && !prefix.isEmpty()) {
			System.out.println("--" + prefix);
			wordsSet.add(prefix);
		} else {
			for (int i = 0; i < n; i++)
				permutation(prefix + str.charAt(i),
						str.substring(0, i) + str.substring(i + 1, n), wordsSet);
		}
	}

	public static <T> Set<Set<T>> powerSet(Set<T> originalSet) {
		Set<Set<T>> sets = new HashSet<Set<T>>();
		if (originalSet.isEmpty()) {
			sets.add(new HashSet<T>());
			return sets;
		}
		List<T> list = new ArrayList<T>(originalSet);
		T head = list.get(0);
		Set<T> rest = new HashSet<T>(list.subList(1, list.size()));
		for (Set<T> set : powerSet(rest)) {
			Set<T> newSet = new HashSet<T>();
			newSet.add(head);
			newSet.addAll(set);
			sets.add(newSet);
			sets.add(set);
		}
		return sets;
	}

}
