package com.ecommerce.util;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

public class MailUtil {

	/*static final String FROM = "mintkart12@gmail.com";
	static final String FROMNAME = "Mint Kart";
	// Replace smtp_username with your Amazon SES SMTP user name.
	static final String SMTP_USERNAME ="MINT KART"; 
			//"AKIAJU5WLS7ONPBW7IGA";

	// Replace smtp_password with your Amazon SES SMTP password.
	static final String SMTP_PASSWORD ="MintkarT@12";
			//"ArtwRbB2z+U4uI9qtaUboysN8bM6ZwR06/btEo5H6XCg";

	// The name of the Configuration Set to use for this message.
	// If you comment out or remove this variable, you will also need to
	// comment out or remove the header below.
	//static final String CONFIGSET = "ConfigSet";

	// Amazon SES SMTP host name. This example uses the US West (Oregon) Region.
	static final String HOST = "email-smtp.us-west-2.amazonaws.com";

	// The port you will connect to on the Amazon SES SMTP endpoint.
	static final int PORT = 587;*/

	public static void sendOrderMailToCustomer(String to, int orderId, double totalCost)
			throws MessagingException, UnsupportedEncodingException {
		// 1 - get a mail session
		// Create a Properties object to contain connection configuration information.
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtps.host", "smtp.gmail.com");
		props.put("mail.smtps.port", 465);
		props.put("mail.smtps.auth", "true");
		props.put("mail.smtps.quitwait", "false");
		Session session = Session.getDefaultInstance(props);
		session.setDebug(false);

		// 2 - create a message
		Message message = new MimeMessage(session);
		message.setSubject("Order # " + orderId + " confirmation");
		// if (bodyIsHTML)
		 
	//	 HtmlEmailSender mailer = new HtmlEmailSender();
		// else
		 String htmlEmailMessage = "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 100%; max-width: 600px;\">\n" + 
		 		"                <tr>\n" + 
		 		"                    <td bgcolor=\"#c7d8a7\" style=\"padding: 40px 30px 20px 30px;\">\n" + 
		 		"					<table width=\"70\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
		 		"						<tr>\n" + 
		 		"							<td height=\"70\" style=\"padding: 0 20px 20px 0;\">\n" + 
		 		"								<img src=\"email.png\" width=\"70\" height=\"70\" border=\"0\" alt=\"\"> \n" + 
		 		"							</td>\n" + 
		 		"						</tr>\n" + 
		 		"					</table>\n" + 
		 		"					\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 425px;\">\n" + 
		 		"									<tr>\n" + 
		 		"										<td height=\"70\">\n" + 
		 		"										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 0 0 0 3px;font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;\">\n" + 
		 		"													Welcome To\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 5px 0 0 0;color: #153643; font-family: sans-serif;font-size: 33px; line-height: 38px; font-weight: bold;\">\n" + 
		 		"													Mint Kart\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"										</table>\n" + 
		 		"										</td>\n" + 
		 		"									</tr>\n" + 
		 		"								</table>\n" + 
		 		"							\n" + 
		 		"					</td>\n" + 
		 		"                </tr>\n" + 
		 		"				<tr bgcolor=\"white\">\n" + 
		 		"					<td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Greetings From Mint Kart\n" + 
		 		"									\n" + 
		 		"								</td>\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Your order #"+orderId+" has been ordered sucessfulyy.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Total cost of your order is Rs"+totalCost+"\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"							  <td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"								\n" + 
		 		"								<!--[if (gte mso 9)|(IE)]>\n" + 
		 		"								  <table width=\"380\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" + 
		 		"									<tr>\n" + 
		 		"									  <td>\n" + 
		 		"								<![endif]-->\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 380px;\">  \n" + 
		 		"								  <tr>\n" + 
		 		"									<td>\n" + 
		 		"									  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"										<tr>\n" + 
		 		"										  <td style=\"padding: 20px 0 0 0;\">\n" + 
		 		"											<table  bgcolor=\"#e05443\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											  <tr>\n" + 
		 		"												<td height=\"45\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\">\n" + 
		 		"												  <a href=\"http://www.mintkart.in/orderDetailsServlet?orderId="+orderId+"\" style=\"color: #ffffff; text-decoration: none;\">View Order</a>\n" + 
		 		"												</td>\n" + 
		 		"											  </tr>\n" + 
		 		"											</table>\n" + 
		 		"										  </td>\n" + 
		 		"										</tr>\n" + 
		 		"									  </table>\n" + 
		 		"									</td>\n" + 
		 		"								  </tr>\n" + 
		 		"								</table>\n" + 
		 		"								\n" + 
		 		"							  </td>\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									We will keep you updated regarding your order status.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Thanks For Shopping With Us.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							\n" + 
		 		"						</table>\n" + 
		 		"					</td>\n" + 
		 		"				</tr>\n" + 
		 		"            </table>";
		 htmlEmailMessage += "<b>Wish you a nice day!</b><br>";
		
		 htmlEmailMessage += "<font color=red>Mint Kart</font>";
		 message.setContent(htmlEmailMessage, "text/html");
		//message.setText(messageNew);
		/*message.setText("Greeting from Fashion HUb!.\n"
				+"Your order #"+orderId+"has been ordered sucessfulyy.Total cost of your order is "+totalCost+".\n"
				+"We will keep you updated regarding your order status\n"
				+"Thanks for shopping with us \n"
				+ "Thanks \n"
				+ "Team Fashion Hub");*/
		// 3 - address the message
		// Address fromAddress = new InternetAddress(from);
		// Address toAddress = new InternetAddress(to);
		message.setFrom(new InternetAddress("Sudhanshusingh888@gmail.com"));
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));

		// 4 - send the message
		// Transport.send(message);
		Transport transport = session.getTransport();
		transport.connect("mintkart12@gmail.com", "MintkarT@12");
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();

	}
	public static void sendCancellationMailToAdmin(int orderId, double totalCost)
			throws MessagingException, UnsupportedEncodingException {
		// 1 - get a mail session
		// Create a Properties object to contain connection configuration information.
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtps.host", "smtp.gmail.com");
		props.put("mail.smtps.port", 465);
		props.put("mail.smtps.auth", "true");
		props.put("mail.smtps.quitwait", "false");
		Session session = Session.getDefaultInstance(props);
		session.setDebug(false);

		// 2 - create a message
		Message message = new MimeMessage(session);
		message.setSubject("Order Cancellation");
		// if (bodyIsHTML)
		 
	//	 HtmlEmailSender mailer = new HtmlEmailSender();
		// else
		 String htmlEmailMessage = "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 100%; max-width: 600px;\">\n" + 
		 		"                <tr>\n" + 
		 		"                    <td bgcolor=\"#c7d8a7\" style=\"padding: 40px 30px 20px 30px;\">\n" + 
		 		"					<table width=\"70\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
		 		"						<tr>\n" + 
		 		"							<td height=\"70\" style=\"padding: 0 20px 20px 0;\">\n" + 
		 		"								<img src=\"email.png\" width=\"70\" height=\"70\" border=\"0\" alt=\"\"> \n" + 
		 		"							</td>\n" + 
		 		"						</tr>\n" + 
		 		"					</table>\n" + 
		 		"					\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 425px;\">\n" + 
		 		"									<tr>\n" + 
		 		"										<td height=\"70\">\n" + 
		 		"										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 0 0 0 3px;font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;\">\n" + 
		 		"													Welcome To\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 5px 0 0 0;color: #153643; font-family: sans-serif;font-size: 33px; line-height: 38px; font-weight: bold;\">\n" + 
		 		"													Mint Kart\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"										</table>\n" + 
		 		"										</td>\n" + 
		 		"									</tr>\n" + 
		 		"								</table>\n" + 
		 		"							\n" + 
		 		"					</td>\n" + 
		 		"                </tr>\n" + 
		 		"				<tr bgcolor=\"white\">\n" + 
		 		"					<td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Greetings From Mint Kart\n" + 
		 		"									\n" + 
		 		"								</td>\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Some products from  #"+orderId+" has been Cancelled by user.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Total cost of the  order is Rs"+totalCost+"\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"							  <td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"								\n" + 
		 		"								<!--[if (gte mso 9)|(IE)]>\n" + 
		 		"								  <table width=\"380\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" + 
		 		"									<tr>\n" + 
		 		"									  <td>\n" + 
		 		"								<![endif]-->\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 380px;\">  \n" + 
		 		"								  <tr>\n" + 
		 		"									<td>\n" + 
		 		"									  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"										<tr>\n" + 
		 		"										  <td style=\"padding: 20px 0 0 0;\">\n" + 
		 		"											<table  bgcolor=\"#e05443\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											  <tr>\n" + 
		 		"												<td height=\"45\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\">\n" + 
		 		"												  <a href=\"http://www.mintkart.in/admin/viewOrderDetails?orderId="+orderId+"\" style=\"color: #ffffff; text-decoration: none;\">View Order</a>\n" + 
		 		"												</td>\n" + 
		 		"											  </tr>\n" + 
		 		"											</table>\n" + 
		 		"										  </td>\n" + 
		 		"										</tr>\n" + 
		 		"									  </table>\n" + 
		 		"									</td>\n" + 
		 		"								  </tr>\n" + 
		 		"								</table>\n" + 
		 		"								\n" + 
		 		"							  </td>\n" + 
		 		"							</tr>\n" +  
		 		"							\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Login to admin panel for more details.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							\n" + 
		 		"						</table>\n" + 
		 		"					</td>\n" + 
		 		"				</tr>\n" + 
		 		"            </table>";
		 htmlEmailMessage += "<b>Wish you a nice day!</b><br>";
		
		 htmlEmailMessage += "<font color=red>Mint Kart</font>";
		 message.setContent(htmlEmailMessage, "text/html");
		//message.setText(messageNew);
		/*message.setText("Greeting from Fashion HUb!.\n"
				+"Your order #"+orderId+"has been ordered sucessfulyy.Total cost of your order is "+totalCost+".\n"
				+"We will keep you updated regarding your order status\n"
				+"Thanks for shopping with us \n"
				+ "Thanks \n"
				+ "Team Fashion Hub");*/
		// 3 - address the message
		// Address fromAddress = new InternetAddress(from);
		// Address toAddress = new InternetAddress(to);
		message.setFrom(new InternetAddress("Sudhanshusingh888@gmail.com"));
		message.setRecipient(Message.RecipientType.TO, new InternetAddress("mintkart12@gmail.com"));

		// 4 - send the message
		// Transport.send(message);
		Transport transport = session.getTransport();
		transport.connect("mintkart12@gmail.com", "MintkarT@12");
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();

	}
	public static void sendOrderMailToAdmin(int orderId, double totalCost)
			throws MessagingException {
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtps.host", "smtp.gmail.com");
		props.put("mail.smtps.port", 465);
		props.put("mail.smtps.auth", "true");
		props.put("mail.smtps.quitwait", "false");
		Session session = Session.getDefaultInstance(props);
		session.setDebug(false);

		// 2 - create a message
		Message message = new MimeMessage(session);
		message.setSubject(" New Order # " + orderId + " confirmation");
		// if (bodyIsHTML)
		 
	//	 HtmlEmailSender mailer = new HtmlEmailSender();
		// else
		 String htmlEmailMessage = "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 100%; max-width: 600px;\">\n" + 
		 		"                <tr>\n" + 
		 		"                    <td bgcolor=\"#c7d8a7\" style=\"padding: 40px 30px 20px 30px;\">\n" + 
		 		"					<table width=\"70\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
		 		"						<tr>\n" + 
		 		"							<td height=\"70\" style=\"padding: 0 20px 20px 0;\">\n" + 
		 		"								<img src=\"email.png\" width=\"70\" height=\"70\" border=\"0\" alt=\"\"> \n" + 
		 		"							</td>\n" + 
		 		"						</tr>\n" + 
		 		"					</table>\n" + 
		 		"					\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 425px;\">\n" + 
		 		"									<tr>\n" + 
		 		"										<td height=\"70\">\n" + 
		 		"										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 0 0 0 3px;font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;\">\n" + 
		 		"													Welcome To\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 5px 0 0 0;color: #153643; font-family: sans-serif;font-size: 33px; line-height: 38px; font-weight: bold;\">\n" + 
		 		"													Mint Kart\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"										</table>\n" + 
		 		"										</td>\n" + 
		 		"									</tr>\n" + 
		 		"								</table>\n" + 
		 		"							\n" + 
		 		"					</td>\n" + 
		 		"                </tr>\n" + 
		 		"				<tr bgcolor=\"white\">\n" + 
		 		"					<td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Greetings From Mint Kart\n" + 
		 		"									\n" + 
		 		"								</td>\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									A new  order #"+orderId+" has been ordered sucessfulyy.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Total cost of your order is Rs"+totalCost+"\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"							  <td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"								\n" + 
		 		"								<!--[if (gte mso 9)|(IE)]>\n" + 
		 		"								  <table width=\"380\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" + 
		 		"									<tr>\n" + 
		 		"									  <td>\n" + 
		 		"								<![endif]-->\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 380px;\">  \n" + 
		 		"								  <tr>\n" + 
		 		"									<td>\n" + 
		 		"									  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"										<tr>\n" + 
		 		"										  <td style=\"padding: 20px 0 0 0;\">\n" + 
		 		"											<table  bgcolor=\"#e05443\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											  <tr>\n" + 
		 		"												<td height=\"45\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\">\n" + 
		 		"												  <a href=\"http://www.mintkart.in/admin/viewOrderDetails?orderId="+orderId+"\" style=\"color: #ffffff; text-decoration: none;\">View Order</a>\n" + 
		 		"												</td>\n" + 
		 		"											  </tr>\n" + 
		 		"											</table>\n" + 
		 		"										  </td>\n" + 
		 		"										</tr>\n" + 
		 		"									  </table>\n" + 
		 		"									</td>\n" + 
		 		"								  </tr>\n" + 
		 		"								</table>\n" + 
		 		"								\n" + 
		 		"							  </td>\n" + 
		 		"							</tr>\n" + 
		 		"							\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Login to admin Panel to update\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							\n" + 
		 		"						</table>\n" + 
		 		"					</td>\n" + 
		 		"				</tr>\n" + 
		 		"            </table>";
		 htmlEmailMessage += "<b>Wish you a nice day!</b><br>";
		
		 htmlEmailMessage += "<font color=red>Mint Kart</font>";
		 message.setContent(htmlEmailMessage, "text/html");
		//message.setText(messageNew);
		/*message.setText("Greeting from Fashion HUb!.\n"
				+"Your order #"+orderId+"has been ordered sucessfulyy.Total cost of your order is "+totalCost+".\n"
				+"We will keep you updated regarding your order status\n"
				+"Thanks for shopping with us \n"
				+ "Thanks \n"
				+ "Team Fashion Hub");*/
		// 3 - address the message
		// Address fromAddress = new InternetAddress(from);
		// Address toAddress = new InternetAddress(to);
		message.setFrom(new InternetAddress("Sudhanshusingh888@gmail.com"));
		message.setRecipient(Message.RecipientType.TO, new InternetAddress("mintkart12@gmail.com"));

		// 4 - send the message
		// Transport.send(message);
		Transport transport = session.getTransport();
		transport.connect("mintkart12@gmail.com", "MintkarT@12");
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();

	}

	public static void sendProductStockMail(String to, String from, String subject, String body, boolean bodyIsHTML)
			throws MessagingException {
		// 1 - get a mail session
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtps.host", "smtp.gmail.com");
		props.put("mail.smtps.port", 465);
		props.put("mail.smtps.auth", "true");
		props.put("mail.smtps.quitwait", "false");
		Session session = Session.getDefaultInstance(props);
		session.setDebug(true);

		// 2 - create a message
		Message message = new MimeMessage(session);
		message.setSubject("message check");
		// if (bodyIsHTML)
		// message.setContent(body, "text/html");
		// else
		message.setText("message being sent");

		// 3 - address the message
		// Address fromAddress = new InternetAddress(from);
		// Address toAddress = new InternetAddress(to);
		message.setFrom(new InternetAddress("Sudhanshusingh888@gmail.com"));
		message.setRecipient(Message.RecipientType.TO, new InternetAddress("sudhanshusingh787@gmail.com"));

		// 4 - send the message
		// Transport.send(message);
		Transport transport = session.getTransport();
		transport.connect("sudhanshusingh787@gmail.com", "khutawnanaresh@meera");
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();

	}

	public static void sendResetPasswordMail(String to, String tokenId) throws MessagingException {
		// 1 - get a mail session
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtps.host", "smtp.gmail.com");
		props.put("mail.smtps.port", 465);
		props.put("mail.smtps.auth", "true");
		props.put("mail.smtps.quitwait", "false");
		props.put("mail.smtp.user", "mintkart12@gmail.com");
		props.put("mail.smtp.starttls.enable","true");
		props.put("mail.smtp.debug", "true");
		props.put("mail.smtp.socketFactory.port", 465);
	    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	    props.put("mail.smtp.socketFactory.fallback", "false");
		Session session = Session.getDefaultInstance(props);
		session.setDebug(false);

		// 2 - create a message
		Message message = new MimeMessage(session);
		message.setSubject("Reset Password Link");
		// if (bodyIsHTML)
		// message.setContent(body, "text/html");
		// else
		 String htmlEmailMessage = "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 100%; max-width: 600px;\">\n" + 
			 		"                <tr>\n" + 
			 		"                    <td bgcolor=\"#c7d8a7\" style=\"padding: 40px 30px 20px 30px;\">\n" + 
			 		"					<table width=\"70\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
			 		"						<tr>\n" + 
			 		"							<td height=\"70\" style=\"padding: 0 20px 20px 0;\">\n" + 
			 		"								<img src=\"email.png\" width=\"70\" height=\"70\" border=\"0\" alt=\"\"> \n" + 
			 		"							</td>\n" + 
			 		"						</tr>\n" + 
			 		"					</table>\n" + 
			 		"					\n" + 
			 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 425px;\">\n" + 
			 		"									<tr>\n" + 
			 		"										<td height=\"70\">\n" + 
			 		"										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
			 		"											<tr>\n" + 
			 		"												<td style=\"padding: 0 0 0 3px;font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;\">\n" + 
			 		"													Welcome To\n" + 
			 		"												</td>\n" + 
			 		"											</tr>\n" + 
			 		"											<tr>\n" + 
			 		"												<td style=\"padding: 5px 0 0 0;color: #153643; font-family: sans-serif;font-size: 33px; line-height: 38px; font-weight: bold;\">\n" + 
			 		"													Mint Kart\n" + 
			 		"												</td>\n" + 
			 		"											</tr>\n" + 
			 		"										</table>\n" + 
			 		"										</td>\n" + 
			 		"									</tr>\n" + 
			 		"								</table>\n" + 
			 		"							\n" + 
			 		"					</td>\n" + 
			 		"                </tr>\n" + 
			 		"				<tr bgcolor=\"white\">\n" + 
			 		"					<td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
			 		"						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
			 		"							<tr>\n" + 
			 		"								<td style=\"padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;color: #153643; font-family: sans-serif;\">\n" + 
			 		"									Greetings From Mint Kart\n" + 
			 		"									\n" + 
			 		"								</td>\n" + 
			 		"							</tr>\n" + 
			 		"							<tr>\n" + 
			 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
			 		"									Click on the below link to Reset your password\n" + 
			 		"								</td>\n" + 
			 		"								\n" + 
			 		"							</tr>\n" + 
			 		"							<tr>\n" + 
			 		"							  <td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
			 		"								\n" + 
			 		"								<!--[if (gte mso 9)|(IE)]>\n" + 
			 		"								  <table width=\"380\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" + 
			 		"									<tr>\n" + 
			 		"									  <td>\n" + 
			 		"								<![endif]-->\n" + 
			 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 380px;\">  \n" + 
			 		"								  <tr>\n" + 
			 		"									<td>\n" + 
			 		"									  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
			 		"										<tr>\n" + 
			 		"										  <td style=\"padding: 20px 0 0 0;\">\n" + 
			 		"											<table  bgcolor=\"#e05443\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
			 		"											  <tr>\n" + 
			 		"												<td height=\"45\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\">\n" + 
			 		"												  <a href=\"http://www.mintkart.in/resetPassword?tokenId="+tokenId+"\" style=\"color: #ffffff; text-decoration: none;\">Reset Password</a>\n" + 
			 		"												</td>\n" + 
			 		"											  </tr>\n" + 
			 		"											</table>\n" + 
			 		"										  </td>\n" + 
			 		"										</tr>\n" + 
			 		"									  </table>\n" + 
			 		"									</td>\n" + 
			 		"								  </tr>\n" + 
			 		"								</table>\n" + 
			 		"								\n" + 
			 		"							  </td>\n" + 
			 		"							</tr>\n" + 
			 		"							\n" + 
			 		"						</table>\n" + 
			 		"					</td>\n" + 
			 		"				</tr>\n" + 
			 		"            </table>";
			 htmlEmailMessage += "<b>Wish you a nice day!</b><br>";
			
			 htmlEmailMessage += "<font color=red>Mint Kart</font>";
		
		
		
			 message.setContent(htmlEmailMessage, "text/html");

			 message.setFrom(new InternetAddress("Sudhanshusingh888@gmail.com"));
				message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));

				// 4 - send the message
				// Transport.send(message);
				Transport transport = session.getTransport("smtps");
				transport.connect("smtp.gmail.com",465,"mintkart12@gmail.com", "MintkarT@12");
				transport.sendMessage(message, message.getAllRecipients());
				transport.close();

	}
	public static void sendOrderShipmentMailToCustomer(String to, int orderId, double totalCost,String trackingNumber)
			throws MessagingException, UnsupportedEncodingException {
		// 1 - get a mail session
		// Create a Properties object to contain connection configuration information.
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtps.host", "smtp.gmail.com");
		props.put("mail.smtps.port", 465);
		props.put("mail.smtps.auth", "true");
		props.put("mail.smtps.quitwait", "false");
		Session session = Session.getDefaultInstance(props);
		session.setDebug(false);

		// 2 - create a message
		Message message = new MimeMessage(session);
		message.setSubject("Order # " + orderId + " Is Shipped Tracking Id"+trackingNumber);
		// if (bodyIsHTML)
		 
	//	 HtmlEmailSender mailer = new HtmlEmailSender();
		// else
		 String htmlEmailMessage = "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 100%; max-width: 600px;\">\n" + 
		 		"                <tr>\n" + 
		 		"                    <td bgcolor=\"#c7d8a7\" style=\"padding: 40px 30px 20px 30px;\">\n" + 
		 		"					<table width=\"70\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
		 		"						<tr>\n" + 
		 		"							<td height=\"70\" style=\"padding: 0 20px 20px 0;\">\n" + 
		 		"								<img src=\"email.png\" width=\"70\" height=\"70\" border=\"0\" alt=\"\"> \n" + 
		 		"							</td>\n" + 
		 		"						</tr>\n" + 
		 		"					</table>\n" + 
		 		"					\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 425px;\">\n" + 
		 		"									<tr>\n" + 
		 		"										<td height=\"70\">\n" + 
		 		"										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 0 0 0 3px;font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;\">\n" + 
		 		"													Welcome To\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 5px 0 0 0;color: #153643; font-family: sans-serif;font-size: 33px; line-height: 38px; font-weight: bold;\">\n" + 
		 		"													Mint Kart\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"										</table>\n" + 
		 		"										</td>\n" + 
		 		"									</tr>\n" + 
		 		"								</table>\n" + 
		 		"							\n" + 
		 		"					</td>\n" + 
		 		"                </tr>\n" + 
		 		"				<tr bgcolor=\"white\">\n" + 
		 		"					<td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Greetings From Mint Kart\n" + 
		 		"									\n" + 
		 		"								</td>\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Your order #"+orderId+" has been Shipped through BlueDart sucessfulyy.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Your Tracking Id  is"+trackingNumber+"\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Total cost of your order is Rs"+totalCost+"\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"							  <td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"								\n" + 
		 		"								<!--[if (gte mso 9)|(IE)]>\n" + 
		 		"								  <table width=\"380\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" + 
		 		"									<tr>\n" + 
		 		"									  <td>\n" + 
		 		"								<![endif]-->\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 380px;\">  \n" + 
		 		"								  <tr>\n" + 
		 		"									<td>\n" + 
		 		"									  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"										<tr>\n" + 
		 		"										  <td style=\"padding: 20px 0 0 0;\">\n" + 
		 		"											<table  bgcolor=\"#e05443\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											  <tr>\n" + 
		 		"												<td height=\"45\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\">\n" + 
		 		"												  <a href=\"http://www.mintkart.in/orderDetailsServlet?orderId="+orderId+"\" style=\"color: #ffffff; text-decoration: none;\">View Order</a>\n" + 
		 		"												</td>\n" + 
		 		"											  </tr>\n" + 
		 		"											</table>\n" + 
		 		"										  </td>\n" + 
		 		"										</tr>\n" + 
		 		"									  </table>\n" + 
		 		"									</td>\n" + 
		 		"								  </tr>\n" + 
		 		"								</table>\n" + 
		 		"								\n" + 
		 		"							  </td>\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									We will keep you updated regarding your order status.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Thanks For Shopping With Us.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							\n" + 
		 		"						</table>\n" + 
		 		"					</td>\n" + 
		 		"				</tr>\n" + 
		 		"            </table>";
		 htmlEmailMessage += "<b>Wish you a nice day!</b><br>";
		
		 htmlEmailMessage += "<font color=red>Mint Kart</font>";
		 message.setContent(htmlEmailMessage, "text/html");
		//message.setText(messageNew);
		/*message.setText("Greeting from Fashion HUb!.\n"
				+"Your order #"+orderId+"has been ordered sucessfulyy.Total cost of your order is "+totalCost+".\n"
				+"We will keep you updated regarding your order status\n"
				+"Thanks for shopping with us \n"
				+ "Thanks \n"
				+ "Team Fashion Hub");*/
		// 3 - address the message
		// Address fromAddress = new InternetAddress(from);
		// Address toAddress = new InternetAddress(to);
		message.setFrom(new InternetAddress("Sudhanshusingh888@gmail.com"));
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));

		// 4 - send the message
		// Transport.send(message);
		Transport transport = session.getTransport();
		transport.connect("mintkart12@gmail.com", "MintkarT@12");
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();

	}
	public static void sendOrderDeliveredMailToCustomer(String to, int orderId, double totalCost)
			throws MessagingException, UnsupportedEncodingException {
		// 1 - get a mail session
		// Create a Properties object to contain connection configuration information.
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtps.host", "smtp.gmail.com");
		props.put("mail.smtps.port", 465);
		props.put("mail.smtps.auth", "true");
		props.put("mail.smtps.quitwait", "false");
		Session session = Session.getDefaultInstance(props);
		session.setDebug(false);

		// 2 - create a message
		Message message = new MimeMessage(session);
		message.setSubject("Order # " + orderId + " Is delivered");
		// if (bodyIsHTML)
		 
	//	 HtmlEmailSender mailer = new HtmlEmailSender();
		// else
		 String htmlEmailMessage = "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 100%; max-width: 600px;\">\n" + 
		 		"                <tr>\n" + 
		 		"                    <td bgcolor=\"#c7d8a7\" style=\"padding: 40px 30px 20px 30px;\">\n" + 
		 		"					<table width=\"70\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
		 		"						<tr>\n" + 
		 		"							<td height=\"70\" style=\"padding: 0 20px 20px 0;\">\n" + 
		 		"								<img src=\"email.png\" width=\"70\" height=\"70\" border=\"0\" alt=\"\"> \n" + 
		 		"							</td>\n" + 
		 		"						</tr>\n" + 
		 		"					</table>\n" + 
		 		"					\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 425px;\">\n" + 
		 		"									<tr>\n" + 
		 		"										<td height=\"70\">\n" + 
		 		"										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 0 0 0 3px;font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;\">\n" + 
		 		"													Welcome To\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 5px 0 0 0;color: #153643; font-family: sans-serif;font-size: 33px; line-height: 38px; font-weight: bold;\">\n" + 
		 		"													Mint Kart\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"										</table>\n" + 
		 		"										</td>\n" + 
		 		"									</tr>\n" + 
		 		"								</table>\n" + 
		 		"							\n" + 
		 		"					</td>\n" + 
		 		"                </tr>\n" + 
		 		"				<tr bgcolor=\"white\">\n" + 
		 		"					<td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Greetings From Mint Kart\n" + 
		 		"									\n" + 
		 		"								</td>\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Your order #"+orderId+" has been Delivered sucessfully.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Total cost of your order is Rs"+totalCost+"\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"							  <td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"								\n" + 
		 		"								<!--[if (gte mso 9)|(IE)]>\n" + 
		 		"								  <table width=\"380\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" + 
		 		"									<tr>\n" + 
		 		"									  <td>\n" + 
		 		"								<![endif]-->\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 380px;\">  \n" + 
		 		"								  <tr>\n" + 
		 		"									<td>\n" + 
		 		"									  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"										<tr>\n" + 
		 		"										  <td style=\"padding: 20px 0 0 0;\">\n" + 
		 		"											<table  bgcolor=\"#e05443\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											  <tr>\n" + 
		 		"												<td height=\"45\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\">\n" + 
		 		"												  <a href=\"http://www.mintkart.in/orderDetailsServlet?orderId="+orderId+"\" style=\"color: #ffffff; text-decoration: none;\">View Order</a>\n" + 
		 		"												</td>\n" + 
		 		"											  </tr>\n" + 
		 		"											</table>\n" + 
		 		"										  </td>\n" + 
		 		"										</tr>\n" + 
		 		"									  </table>\n" + 
		 		"									</td>\n" + 
		 		"								  </tr>\n" + 
		 		"								</table>\n" + 
		 		"								\n" + 
		 		"							  </td>\n" + 
		 		"							</tr>\n" + 
		 		"							\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Thanks For Shopping With Us.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							\n" + 
		 		"						</table>\n" + 
		 		"					</td>\n" + 
		 		"				</tr>\n" + 
		 		"            </table>";
		 htmlEmailMessage += "<b>Wish you a nice day!</b><br>";
		
		 htmlEmailMessage += "<font color=red>Mint Kart</font>";
		 message.setContent(htmlEmailMessage, "text/html");
		//message.setText(messageNew);
		/*message.setText("Greeting from Fashion HUb!.\n"
				+"Your order #"+orderId+"has been ordered sucessfulyy.Total cost of your order is "+totalCost+".\n"
				+"We will keep you updated regarding your order status\n"
				+"Thanks for shopping with us \n"
				+ "Thanks \n"
				+ "Team Fashion Hub");*/
		// 3 - address the message
		// Address fromAddress = new InternetAddress(from);
		// Address toAddress = new InternetAddress(to);
		message.setFrom(new InternetAddress("Sudhanshusingh888@gmail.com"));
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));

		// 4 - send the message
		// Transport.send(message);
		Transport transport = session.getTransport();
		transport.connect("mintkart12@gmail.com", "MintkarT@12");
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();

	}
	public static void sendOrderDelivereyFailMailToCustomer(String to, int orderId, double totalCost,String trackingNumber)
			throws MessagingException, UnsupportedEncodingException {
		// 1 - get a mail session
		// Create a Properties object to contain connection configuration information.
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtps.host", "smtp.gmail.com");
		props.put("mail.smtps.port", 465);
		props.put("mail.smtps.auth", "true");
		props.put("mail.smtps.quitwait", "false");
		Session session = Session.getDefaultInstance(props);
		session.setDebug(false);

		// 2 - create a message
		Message message = new MimeMessage(session);
		message.setSubject("Delivery Attempt for Order # " + orderId + " Failed");
		// if (bodyIsHTML)
		 
	//	 HtmlEmailSender mailer = new HtmlEmailSender();
		// else
		 String htmlEmailMessage = "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 100%; max-width: 600px;\">\n" + 
		 		"                <tr>\n" + 
		 		"                    <td bgcolor=\"#c7d8a7\" style=\"padding: 40px 30px 20px 30px;\">\n" + 
		 		"					<table width=\"70\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
		 		"						<tr>\n" + 
		 		"							<td height=\"70\" style=\"padding: 0 20px 20px 0;\">\n" + 
		 		"								<img src=\"email.png\" width=\"70\" height=\"70\" border=\"0\" alt=\"\"> \n" + 
		 		"							</td>\n" + 
		 		"						</tr>\n" + 
		 		"					</table>\n" + 
		 		"					\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 425px;\">\n" + 
		 		"									<tr>\n" + 
		 		"										<td height=\"70\">\n" + 
		 		"										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 0 0 0 3px;font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;\">\n" + 
		 		"													Welcome To\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"											<tr>\n" + 
		 		"												<td style=\"padding: 5px 0 0 0;color: #153643; font-family: sans-serif;font-size: 33px; line-height: 38px; font-weight: bold;\">\n" + 
		 		"													Mint Kart\n" + 
		 		"												</td>\n" + 
		 		"											</tr>\n" + 
		 		"										</table>\n" + 
		 		"										</td>\n" + 
		 		"									</tr>\n" + 
		 		"								</table>\n" + 
		 		"							\n" + 
		 		"					</td>\n" + 
		 		"                </tr>\n" + 
		 		"				<tr bgcolor=\"white\">\n" + 
		 		"					<td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Greetings From Mint Kart\n" + 
		 		"									\n" + 
		 		"								</td>\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Your order #"+orderId+"  Deliverey Failed.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Your Tracking Id  is"+trackingNumber+"\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Total cost of your order is Rs"+totalCost+"\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							<tr>\n" + 
		 		"							  <td style=\"padding: 30px 30px 30px 30px;border-bottom: 1px solid #f2eeed;\">\n" + 
		 		"								\n" + 
		 		"								<!--[if (gte mso 9)|(IE)]>\n" + 
		 		"								  <table width=\"380\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" + 
		 		"									<tr>\n" + 
		 		"									  <td>\n" + 
		 		"								<![endif]-->\n" + 
		 		"								<table  align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 380px;\">  \n" + 
		 		"								  <tr>\n" + 
		 		"									<td>\n" + 
		 		"									  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"										<tr>\n" + 
		 		"										  <td style=\"padding: 20px 0 0 0;\">\n" + 
		 		"											<table  bgcolor=\"#e05443\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + 
		 		"											  <tr>\n" + 
		 		"												<td height=\"45\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\">\n" + 
		 		"												  <a href=\"http://www.mintkart.in/orderDetailsServlet?orderId="+orderId+"\" style=\"color: #ffffff; text-decoration: none;\">View Order</a>\n" + 
		 		"												</td>\n" + 
		 		"											  </tr>\n" + 
		 		"											</table>\n" + 
		 		"										  </td>\n" + 
		 		"										</tr>\n" + 
		 		"									  </table>\n" + 
		 		"									</td>\n" + 
		 		"								  </tr>\n" + 
		 		"								</table>\n" + 
		 		"								\n" + 
		 		"							  </td>\n" + 
		 		"							</tr>\n" + 
		 		"							\n" + 
		 		"							<tr>\n" + 
		 		"								<td style=\"font-size: 16px; line-height: 22px;color: #153643; font-family: sans-serif;\">\n" + 
		 		"									Thanks For Shopping With Us.\n" + 
		 		"								</td>\n" + 
		 		"								\n" + 
		 		"							</tr>\n" + 
		 		"							\n" + 
		 		"						</table>\n" + 
		 		"					</td>\n" + 
		 		"				</tr>\n" + 
		 		"            </table>";
		 htmlEmailMessage += "<b>Wish you a nice day!</b><br>";
		
		 htmlEmailMessage += "<font color=red>Mint Kart</font>";
		 message.setContent(htmlEmailMessage, "text/html");
		//message.setText(messageNew);
		/*message.setText("Greeting from Fashion HUb!.\n"
				+"Your order #"+orderId+"has been ordered sucessfulyy.Total cost of your order is "+totalCost+".\n"
				+"We will keep you updated regarding your order status\n"
				+"Thanks for shopping with us \n"
				+ "Thanks \n"
				+ "Team Fashion Hub");*/
		// 3 - address the message
		// Address fromAddress = new InternetAddress(from);
		// Address toAddress = new InternetAddress(to);
		message.setFrom(new InternetAddress("Sudhanshusingh888@gmail.com"));
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));

		// 4 - send the message
		// Transport.send(message);
		Transport transport = session.getTransport();
		transport.connect("mintkart12@gmail.com", "MintkarT@12");
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();

	}
}