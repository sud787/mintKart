package com.ecommerce.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import com.ecommerce.business.Address;
import com.ecommerce.business.LineItem;
import com.ecommerce.business.Order;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPCell;

public class PdfInvoices {
	Font boldFont;
	Font mediumFont;
	Font semiMediumFont;

	public PdfInvoices() {
		boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
		mediumFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
		semiMediumFont = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);
	}

	public void createPdf(Document document, Image img, Order order)
			throws DocumentException, MalformedURLException, IOException {
		// header
		// Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
		Paragraph p;
		p = new Paragraph("Tax Invoice/Bill of Supply/Cash Memo", boldFont);
		p.setAlignment(Element.ALIGN_RIGHT);
		document.add(p);
		// invoice date,same as order date
		// p = new Paragraph("April 01,2015", boldFont);
		// p.setAlignment(Element.ALIGN_RIGHT);
		// document.add(p);
		// Image img = Image.getInstance("/images/logo20.png");
		img.setAlignment(Element.ALIGN_LEFT);
		document.add(img);
		// Address seller / buyer
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		PdfPCell seller = getSellerAddress();
		table.addCell(seller);
		PdfPCell buyer = getShippingAddress(order);
		table.addCell(buyer);
		document.add(table);
		table = new PdfPTable(2);
		PdfPCell orderInfo = getorderInfo(order);
		table.addCell(orderInfo);
		PdfPCell invoiceInfo = getInvoiceInfo(order);
		table.addCell(invoiceInfo);
		document.add(table);
		table = getLineItemTable(order);
		document.add(table);
		table = getTotalCostTable(order);
		document.add(table);

	}

	public PdfPCell getInvoiceInfo(Order order) {
		PdfPCell cell = new PdfPCell();
		cell.setBorder(PdfPCell.NO_BORDER);
		Paragraph newParagraph = new Paragraph("Invoice-id:" + order.getInvoiceId());
		newParagraph.setAlignment(Element.ALIGN_RIGHT);
		cell.addElement(newParagraph);
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		// cell.addElement(new Paragraph("Invoice-Date:" +
		// formatter.format(order.getOrderDate())));
		newParagraph = new Paragraph("Invoice Date:" + formatter.format(order.getOrderDate()));
		newParagraph.setAlignment(Element.ALIGN_RIGHT);
		cell.addElement(newParagraph);
		return cell;
	}

	public PdfPCell getorderInfo(Order order) {
		PdfPCell cell = new PdfPCell();
		cell.setBorder(PdfPCell.NO_BORDER);
		cell.addElement(new Paragraph("Order-Id:" + order.getOrderId()));
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		cell.addElement(new Paragraph("Order-Date:" + formatter.format(order.getOrderDate())));
		return cell;
	}

	public PdfPCell getSellerAddress() {
		PdfPCell cell = new PdfPCell();
		cell.setBorder(PdfPCell.NO_BORDER);
		cell.addElement(new Paragraph("Sold By", boldFont));
		cell.addElement(new Paragraph("Mint Kart Private Limited"));
		cell.addElement(new Paragraph("Vishrant Wadi"));
		cell.addElement(new Paragraph(String.format("%s-%s %s", "India", "411025", "Pune")));
		return cell;
	}

	public PdfPCell getShippingAddress(Order order) {
		Address address = order.getAddress();
		PdfPCell cell = new PdfPCell();
		cell.setBorder(PdfPCell.NO_BORDER);
		Paragraph newParagraph = new Paragraph("Shipping Address:", boldFont);
		newParagraph.setAlignment(Element.ALIGN_RIGHT);
		cell.addElement(newParagraph);
		newParagraph = new Paragraph(address.getName());
		newParagraph.setAlignment(Element.ALIGN_RIGHT);
		cell.addElement(newParagraph);
		newParagraph = new Paragraph(address.getAddress());
		newParagraph.setAlignment(Element.ALIGN_RIGHT);
		cell.addElement(newParagraph);
		// cell.addElement();
		newParagraph = new Paragraph(
				String.format("%s-%s %s", address.getCity(), address.getState(), address.getPincode()));
		newParagraph.setAlignment(Element.ALIGN_RIGHT);
		cell.addElement(newParagraph);
		newParagraph = new Paragraph("PH. NO " + address.getNumber());
		newParagraph.setAlignment(Element.ALIGN_RIGHT);
		cell.addElement(newParagraph);
		return cell;

	}

	public PdfPTable getLineItemTable(Order order) throws DocumentException {
		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100);
		table.setSpacingBefore(10);
		table.setSpacingAfter(10);
		table.setWidths(new int[] { 1, 7, 2, 1, 2, 2, 2 });
		table.addCell(getHeaderCell("Sl.No:", Element.ALIGN_LEFT, mediumFont));
		table.addCell(getHeaderCell("Description:", Element.ALIGN_LEFT, mediumFont));
		table.addCell(getHeaderCell("Unit Price:", Element.ALIGN_LEFT, mediumFont));
		table.addCell(getHeaderCell("Qunatity:", Element.ALIGN_LEFT, mediumFont));
		table.addCell(getHeaderCell("Net Amount:", Element.ALIGN_LEFT, mediumFont));
		table.addCell(getHeaderCell("Tax Rate:", Element.ALIGN_LEFT, mediumFont));
		table.addCell(getHeaderCell("Total Amount:", Element.ALIGN_LEFT, mediumFont));
		ArrayList<LineItem> items = order.getItems();
		Iterator<LineItem> itrOrderItems = items.iterator();
		int i = 1;
		while (itrOrderItems.hasNext()) {
			LineItem item = itrOrderItems.next();
			if (item.getProduct().getIsOrderCancelled() == null) {
				table.addCell(getCell(String.valueOf(i), Element.ALIGN_CENTER, semiMediumFont));
				table.addCell(getCell(item.getProduct().getName(), Element.ALIGN_CENTER, semiMediumFont));
				table.addCell(getCell(String.valueOf(item.getProduct().getSellingPrice()), Element.ALIGN_CENTER,
						semiMediumFont));
				table.addCell(getCell(String.valueOf(item.getQuantity()), Element.ALIGN_CENTER, semiMediumFont));
				table.addCell(getCell(String.valueOf(item.getQuantity() * item.getProduct().getSellingPrice()),
						Element.ALIGN_CENTER, semiMediumFont));
				table.addCell(getCell("8.5%", Element.ALIGN_CENTER, semiMediumFont));
				table.addCell(getCell(String.valueOf(item.getQuantity() * item.getProduct().getSellingPrice()),
						Element.ALIGN_CENTER, semiMediumFont));
				++i;
			}
		}
		/*
		 * Product product; for (Item item : invoice.getItems()) { product =
		 * item.getProduct(); table.addCell(createCell(product.getName()));
		 * table.addCell(createCell(InvoiceData.format2dec(InvoiceData.round(product.
		 * getPrice()))) .setTextAlignment(TextAlignment.RIGHT));
		 * table.addCell(createCell(String.valueOf(item.getQuantity())).setTextAlignment
		 * (TextAlignment.RIGHT));
		 * table.addCell(createCell(InvoiceData.format2dec(InvoiceData.round(item.
		 * getCost()))) .setTextAlignment(TextAlignment.RIGHT));
		 * table.addCell(createCell(InvoiceData.format2dec(InvoiceData.round(product.
		 * getVat()))) .setTextAlignment(TextAlignment.RIGHT));
		 * table.addCell(createCell(InvoiceData
		 * .format2dec(InvoiceData.round(item.getCost() + ((item.getCost() *
		 * product.getVat()) / 100)))) .setTextAlignment(TextAlignment.RIGHT)); }
		 */
		return table;
	}

	public PdfPCell getHeaderCell(String value, int alignment, Font font) {
		PdfPCell cell = new PdfPCell();
		cell.setUseAscender(true);
		cell.setUseDescender(true);
		Paragraph p = new Paragraph(value, font);
		p.setAlignment(alignment);
		cell.addElement(p);
		cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
		return cell;
	}

	public PdfPCell getCell(String value, int alignment, Font font) {
		PdfPCell cell = new PdfPCell();
		cell.setUseAscender(true);
		cell.setUseDescender(true);
		Paragraph p = new Paragraph(value, font);
		p.setAlignment(alignment);
		cell.addElement(p);
		return cell;
	}

	public PdfPTable getTotalCostTable(Order order) throws DocumentException {
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		// table.setSpacingBefore(1);
		table.setSpacingAfter(10);
		table.setWidths(new int[] { 15, 2 });
		table.addCell(getHeaderCell("TOTAL:", Element.ALIGN_LEFT, boldFont));
		table.addCell(getHeaderCell(String.valueOf(order.getTotalCost() - order.getShippementCharges()),
				Element.ALIGN_LEFT, mediumFont));

		return table;
	}
}
