package com.ecommerce.util;

import java.util.Random;

public class CalculationUtil {

	public static long calculateDiscount(Double costPrice, long sellingPrice) {
		long percentage = Math.round( (((costPrice - sellingPrice) / costPrice) * 100));
		return percentage;
	}

	public static long calculateSelling(Double sellingPrice, Double tax) {
		long finalSelling = Math.round((sellingPrice) + (sellingPrice * tax)/100);
		return finalSelling;
	}

	public static String genrateInvoiceId() {

		Random rn = new Random();
		String str = "INV-MINT-KART" + rn.nextInt();
		return str;

	}
}
