package com.ecommerce.business;

import java.text.NumberFormat;
import java.io.Serializable;

public class LineItem implements Serializable {
	private Product product;
	private int quantity;
	private int size;
	private String message;
	private String errorMessage;
	private String isProductReviewed;

	public String getIsProductReviewed() {
		return isProductReviewed;
	}

	public void setIsProductReviewed(String isProductReviewed) {
		this.isProductReviewed = isProductReviewed;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public LineItem() {
	}

	public void setProduct(Product p) {
		product = p;
	}

	public Product getProduct() {
		return product;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}

	public double getTotal() {
		double total = product.getSellingPrice() * quantity;
		return total;
	}

}