package com.ecommerce.business;

import java.util.*;

import com.ecommerce.data.CartDB;
import com.ecommerce.data.ProductDB;

import java.io.Serializable;

public class Cart implements Serializable {
	private ArrayList<LineItem> items;
	private int CartId;
	private int totalCartCost;

	public int getCartId() {
		return CartId;
	}

	public void setCartId(int cartId) {
		CartId = cartId;
	}

	public Cart() {
		items = new ArrayList<LineItem>();
		CartId = 0;
	}

	// public void setItems(ArrayList<Product> Product) {
	// items = lineItems;
	// }

	public void setItems(ArrayList<LineItem> lineItems) {
		items = lineItems;
	}

	public ArrayList<LineItem> getItems() {
		return items;
	}

	public int getCount() {
		return items.size();
	}

	public void addItem(LineItem item, int cartId) {
		// If the item already exists in the cart, only the quantity is changed.
		String code = item.getProduct().getId();
		int quantity = item.getQuantity();
		int size = item.getSize();
		boolean update = false;
		for (int i = 0; i < items.size(); i++) {
			LineItem lineItem = items.get(i);
			if (lineItem.getProduct().getId().equals(code) && lineItem.getSize() == size) {
				// update the database with increased quantity
				 CartDB.updateQuantityCart(item, cartId,quantity);
				update = true;
				 lineItem.setQuantity(lineItem.getQuantity() + quantity);
				return;
			}
		}
		if (!update) {
			int sizeId = ProductDB.getSizeID(item.getProduct().getId(), item.getSize());
			CartDB.addToCart(item, cartId, sizeId);
			items.add(item);
		}
	}

	// overloaded method
	// when user doesn't have pre existing entry in cart table
	public void addItem(LineItem item, String cookieValue) {
		// If the item already exists in the cart, only the quantity is changed.
		// insert a new entry in cart table
		CartDB.insertIntocart(cookieValue);
		int cartId = CartDB.getCartId(cookieValue);
		this.CartId = cartId;
		int sizeId = ProductDB.getSizeID(item.getProduct().getId(), item.getSize());
		CartDB.addToCart(item, cartId, sizeId);
		items.add(item);
	}

	public double calculateCost() {
		Iterator<LineItem> itr = items.iterator();
		double totalCost = 0;
		while (itr.hasNext()) {
			LineItem item = itr.next();
			double cost = item.getProduct().getSellingPrice();
			totalCost = totalCost + cost * item.getQuantity();
		}
		return totalCost;

	}

	public int quantityCheck() {
		// ArrayList<LineItem> items = cart.getItems();
		Iterator<LineItem> itr = items.iterator();
		boolean isErrorMessage = false;
		while (itr.hasNext()) {
			LineItem item = itr.next();
			Product product = item.getProduct();
			String productId = product.getId();
			int size = item.getSize();
			int quantity = item.getQuantity();
			int dbquanty = CartDB.getQuantity(productId, size);
			if (dbquanty == 0) {
				item.setErrorMessage("Product Is out of stock.Remove this item from cart to proceed.");
				isErrorMessage = true;
			} else if (quantity > dbquanty) {
				item.setQuantity(dbquanty);
				item.setMessage("Only " + dbquanty + " units can be ordered");
				isErrorMessage = true;
			}
		}
		if (isErrorMessage)
			return 1;
		return 0;

	}

}