package com.ecommerce.business;

import java.io.Serializable;
import java.util.ArrayList;

public class Product implements Serializable {
	private String productCode;
	private String id;
	private String name;
//	private double price;
	private String productShortDescription;
	private String productLongDescription;
	private String productAdditionalInformation;
	private int discount;
	private String brand;
	private int quantity;
	private String colour;
	private double sellingPrice;
	private double costPrice;
	private String isOrderCancelled;
	private double tax;
	private String HSNNumber;
	private String subCategoryId;
	

	public String getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public String getHSNNumber() {
		return HSNNumber;
	}

	public void setHSNNumber(String hSNNumber) {
		HSNNumber = hSNNumber;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public String getIsOrderCancelled() {
		return isOrderCancelled;
	}

	public void setIsOrderCancelled(String isOrderCancelled) {
		this.isOrderCancelled = isOrderCancelled;
	}

	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(double costPrice) {
		this.costPrice = costPrice;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductShortDescription() {
		return productShortDescription;
	}

	public void setProductShortDescription(String productShortDescription) {
		this.productShortDescription = productShortDescription;
	}

	public String getProductLongDescription() {
		return productLongDescription;
	}

	public void setProductLongDescription(String productLongDescription) {
		this.productLongDescription = productLongDescription;
	}

	public String getProductAdditionalInformation() {
		return productAdditionalInformation;
	}

	public void setProductAdditionalInformation(
			String productAdditionalInformation) {
		this.productAdditionalInformation = productAdditionalInformation;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImageURL() {
		//"https://s3.ap-south-1.amazonaws.com/mintkart/products/"
		String imageURL = "https://s3.ap-south-1.amazonaws.com/mintkartaws/products/" + productCode + "_" + colour + "_01.PNG";
		return imageURL;
	}

	public ArrayList<String> getImagesURLS() {
		ArrayList<String> imageURL = new ArrayList<>();
		imageURL.add("https://s3.ap-south-1.amazonaws.com/mintkartaws/products/" + productCode + "_" + colour + "_01.PNG");
		imageURL.add("https://s3.ap-south-1.amazonaws.com/mintkartaws/products/" + productCode + "_" + colour + "_02.PNG");
		imageURL.add("https://s3.ap-south-1.amazonaws.com/mintkartaws/products/" + productCode + "_" + colour + "_03.PNG");
		return imageURL;
	}
}