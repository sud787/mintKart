package com.ecommerce.business;

public class Address {
	private String name;
	private String email;
	private String city;
	private String pincode;
	private String state;
	private String address;
	private String number;

	public Address(String name, String email, String number, String city, String pincode, String state,
			String address) {
		super();
		this.name = name;
		this.email = email;
		this.city = city;
		this.pincode = pincode;
		this.state = state;
		this.address = address;
		this.number = number;
	}

	public Address() {

	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
