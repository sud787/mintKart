package com.ecommerce.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.business.Product;
import com.ecommerce.data.ProductDB;

/**
 * Servlet implementation class DisplatProductHandler
 */
@WebServlet("/admin/displayProductHandler")
public class DisplayProductHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DisplayProductHandler() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String productId = request.getParameter("productId");
		String update = (String) request.getAttribute("updateSucess");
		String url = "";
		Product product = ProductDB.selectProduct(productId);

		/* Math.round((sellingprice) + (sellingprice * tax)/100) */;
		/*
		 * fs=sp+(sp*tax)/100 fs*100=100*sp+sp*tax (fs*100)/100+tax
		 * 
		 */

		// return finalSelling;
		if (product != null) {
			double sellingprice = product.getSellingPrice();
			double tax = product.getTax();
			long finalSelling = Math.round((sellingprice * 100) / (100 + tax));
			product.setSellingPrice(finalSelling);
			HashMap<Integer, Integer> sizeQuantityMap = ProductDB.selectSizeQuantity(productId);
			HashMap<String, String> availabeColours = ProductDB.getAvailableColours(product.getProductCode(),
					product.getId());
			if (availabeColours != null && availabeColours.size() > 0) {
				request.setAttribute("availabeColours", availabeColours);
			}
			/*
			 * ArrayList<String> productReview = ProductDB.getProducReviews(productId); if
			 * (productReview != null && productReview.size() > 0) {
			 * request.setAttribute("productReview", productReview); }
			 */
			request.setAttribute("product", product);
			request.setAttribute("sizes", sizeQuantityMap);
			url = "editProduct.jsp";

		} else {
			url = "/index.jsp";
		}
		RequestDispatcher view = request.getRequestDispatcher(url);
		view.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
