package com.ecommerce.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.business.Product;
import com.ecommerce.data.ProductDB;
import com.ecommerce.util.CalculationUtil;

/**
 * Servlet implementation class UpdateProductServlet
 */
@WebServlet("/admin/updateProduct")
public class UpdateProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateProductServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String productId = request.getParameter("productId").trim();
		String subCategoryId = request.getParameter("subCategoryId");
		String productName = request.getParameter("productName").trim();
		String colour = request.getParameter("colour").trim();
		String brand = request.getParameter("brand").trim();
		String[] size = request.getParameterValues("size");
		String[] quantity = request.getParameterValues("quantity");
		double costPrice = Double.parseDouble(request.getParameter("costPrice").trim());
		double sellingPrice = Double.parseDouble(request.getParameter("sellingPrice").trim());
		// sizes and quantityHandling
		double tax = Double.parseDouble(request.getParameter("tax").trim());
		String hsn = request.getParameter("hsn").trim();
		String productDescription = request.getParameter("productDescription").trim();
		String productLongDescription = request.getParameter("productLongDescription").trim();
		String productAdditionalDescription = request.getParameter("productAdditionalDescription").trim();
		long sellingUpdated = CalculationUtil.calculateSelling(sellingPrice, tax);

		long discount = 0;
		if (costPrice > sellingUpdated)
			discount = CalculationUtil.calculateDiscount(costPrice, sellingUpdated);

		Product product = new Product();
		product.setId(productId);
		product.setName(productName);
		product.setColour(colour);
		product.setProductShortDescription(productDescription);
		product.setProductLongDescription(productLongDescription);
		product.setProductAdditionalInformation(productAdditionalDescription);
		product.setCostPrice(costPrice);
		product.setSellingPrice(sellingUpdated);
		product.setDiscount((int) discount);
		product.setHSNNumber(hsn);
		product.setTax(tax);
		int brandId = 0;
		String result = ProductDB.getBrandID(brand);
		if (result == null || result.equals("")) {
			brandId = ProductDB.insertIntoBrandTable(brand);
		} else {
			brandId = Integer.parseInt(result);
		}
		int updateResult = ProductDB.updateProduct(product, brandId);

		// size quantity Update
		/*
		 * check whether the size is new or existing size updation
		 * 
		 */

		if (size.length > 0 && !size[0].equals("")) {
			// size and quantity both are there
			// check whether size is existing or a new one.
			for (int i = 0; i < size.length; i++) {
				int sizeId = ProductDB.getSizeID(productId, Integer.parseInt(size[i]));
				if (sizeId != -1) {
					// update the table;
					ProductDB.updateSizeQuantity(sizeId, Integer.parseInt(quantity[i]));

				} else {
					// insert as it's a new enrty
					ProductDB.insertSizeQuantity(Integer.parseInt(productId), Integer.parseInt(size[i]),
							Integer.parseInt(quantity[i]), Integer.parseInt(subCategoryId));
				}
			}

		} else {
			// getSizeId for the products that doesn't has a size
			int sizeId = ProductDB.getSizeID(productId);
			ProductDB.updateSizeQuantity(sizeId, Integer.parseInt(quantity[0]));

		}
		System.out.println("updated" + updateResult);
		request.setAttribute("updateSucess","Sucessfull");
		RequestDispatcher view = request.getRequestDispatcher("displayProductHandler?productId="+productId);
		view.forward(request, response);

	}

}
