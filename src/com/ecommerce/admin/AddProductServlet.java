package com.ecommerce.admin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.ecommerce.business.Product;
import com.ecommerce.data.MenuUtil;
import com.ecommerce.data.ProductDB;
import com.ecommerce.util.CalculationUtil;

public class AddProductServlet extends HttpServlet {

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		DiskFileItemFactory factory = new DiskFileItemFactory();
		String filePath = getServletContext().getInitParameter("file-upload");
		File file;
		ServletFileUpload upload = new ServletFileUpload(factory);
		List<FileItem> uploadItems = null;
		HashMap<String, String> fieldValueMap = new HashMap<>();
		int imageCount = 1;
		try {
			uploadItems = upload.parseRequest(request);
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (FileItem uploadItem : uploadItems) {
			if (uploadItem.isFormField()) {
				String fieldName = uploadItem.getFieldName();
				String value = uploadItem.getString().trim();
				fieldValueMap.put(fieldName, value);
			} else {
				String fieldName = uploadItem.getFieldName();
				String fileName = uploadItem.getName();
				if (fileName != null && !fileName.equals("")) {
					String contentType = uploadItem.getContentType();
					boolean isInMemory = uploadItem.isInMemory();
					long sizeInBytes = uploadItem.getSize();
					// write the logic to save the image
					file = new File(filePath + fieldValueMap.get("productCode")
							+ "_" + fieldValueMap.get("colour") + "_" + "0"
							+ imageCount + ".PNG");
					++imageCount;
					try {
						uploadItem.write(file);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		double costPrice = Double.parseDouble(fieldValueMap.get("costPrice"));
		double sellingPrice = Double.parseDouble(fieldValueMap
				.get("sellingPrice"));
		// calcultae the discount
		int discount=0;
		if(costPrice>sellingPrice)
		discount= CalculationUtil.calaculateDiscount(costPrice,
				sellingPrice);
		Product product = new Product();
		product.setName(fieldValueMap.get("productName"));
		product.setProductCode(fieldValueMap.get("productCode"));
		product.setColour(fieldValueMap.get("colour"));
		product.setProductShortDescription(fieldValueMap
				.get("productDescription"));
		product.setProductLongDescription(fieldValueMap
				.get("productLongDescription"));
		product.setProductAdditionalInformation(fieldValueMap
				.get("productLongDescription"));
		product.setCostPrice(costPrice);
		product.setSellingPrice(sellingPrice);
		product.setDiscount(discount);
	//	product.setBrand(filedValueMap.get("brand"));
		int subcategoryId = MenuUtil.getSubCategoryId(fieldValueMap
				.get("selectedSubCategory"));
		//insert brand into brand table and fetch the ID.
		String brand=fieldValueMap.get("brand").toLowerCase();
		//check whether brand exist or not
		int brandId=0;
		String result=ProductDB.getBrandID(brand);
		if(result==null || result.equals("") ){
			brandId=ProductDB.insertIntoBrandTable(brand);
		}else{
			brandId=Integer.parseInt(result);
		}
		int productId = ProductDB.addProduct(product, subcategoryId,brandId);
		if (productId > 0) {
			ProductDB.insertSizeQuantity(productId,
					Integer.parseInt(fieldValueMap.get("size")),
					Integer.parseInt(fieldValueMap.get("quantity")));
		}
	}
}
