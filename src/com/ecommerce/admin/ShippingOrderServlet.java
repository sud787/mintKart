package com.ecommerce.admin;

import java.io.IOException;
import java.util.HashMap;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.data.AddressDB;
import com.ecommerce.data.OrderDB;
import com.ecommerce.util.MailUtil;

/**
 * Servlet implementation class ShippingOrderServlet
 */
@WebServlet("/admin/shippingOrder")
public class ShippingOrderServlet extends HttpServlet {

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String orderId = (String) request.getParameter("orderId");
		String trackingNumber = (String) request.getParameter("trackingId");
		// move the state to shipped
		// insert tracking id
		// send mail with tracking id to consumer.
		OrderDB.updateOrderToShipped(orderId, trackingNumber);
		// fetch the email from addressTable and send the mail.
		//String mailingAddress = AddressDB.getMailingAddress(orderId);
		HashMap<String, String> emailCostMap = OrderDB.getMailingDetails(orderId);
		try {
			MailUtil.sendOrderShipmentMailToCustomer(emailCostMap.get("emailAddress"),Integer.parseInt(orderId),
					Double.parseDouble(emailCostMap.get("totalCost")),trackingNumber);
		} catch (MessagingException e) {
			String errorMessage = "ERROR: Unable to send email. " + "Check Tomcat logs for details.<br>"
					+ "NOTE: You may need to configure your system " + "as described in chapter 15.<br>"
					+ "ERROR MESSAGE: " + e.getMessage();
			request.setAttribute("errorMessage", errorMessage);
			this.log("Unable to send email. \n" + "Here is the email you tried to send: \n");
		}
	}

}
