package com.ecommerce.admin;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.data.MenuUtil;

/**
 * Servlet implementation class AddMenuHandlerAjaxServlet
 */
@WebServlet("/admin/addMenu")
public class AddMenuHandlerAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddMenuHandlerAjaxServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String topCategory = request.getParameter("topCategory");
		String category = request.getParameter("category");
		String subCategory = request.getParameter("subCategory");
		if (topCategory != null && !topCategory.equals("") && category == null) {
			// insert into top category
			MenuUtil.addTopCategory(topCategory);
		} else if (category != null && !category.equals("") && subCategory != null && !subCategory.equals("")) {
			int categoryId = MenuUtil.getCategoryId(category);
			MenuUtil.addSubCategory(subCategory, categoryId);
			
		} else {
			int topCategoryId = MenuUtil.getTopCategoryId(topCategory);
			MenuUtil.addCategory(category, topCategoryId);
		}

	}

}
