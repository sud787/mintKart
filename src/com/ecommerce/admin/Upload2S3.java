package com.ecommerce.admin;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.ecommerce.business.Product;
import com.ecommerce.data.MenuUtil;
import com.ecommerce.data.ProductDB;
import com.ecommerce.util.CalculationUtil;

/**
 * Servlet implementation class Upload2S3
 */
@WebServlet("/admin/upload2S3")
public class Upload2S3 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// private static final long serialVersionUID = -7720246048637220075L;
	private static final int THRESHOLD_SIZE = 1024 * 1024 * 3; // 3MB
	private static final int MAX_FILE_SIZE = 1024 * 1024 * 140; // 140MB
	private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 150; // 150MB
	private static final String UUID_STRING = "uuid";
	private static final String AMAZON_ACCESS_KEY = "AKIAJBVIML7BQLOP7Q2Q";
	private static final String AMAZON_SECRET_KEY = "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";
	private static final String S3_BUCKET_NAME = "mintkartaws";

	// private static final Logger LOGGER = Logger.getLogger(Upload2S3.class);

	public Upload2S3() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// needed for cross-domain communication
		ArrayList<String> size = new ArrayList<>();
		ArrayList<String> quantity = new ArrayList<>();
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		if (!ServletFileUpload.isMultipartContent(request)) {
			PrintWriter writer = response.getWriter();
			writer.println("Request does not contain upload data");
			writer.flush();
			return;
		}
		// configures upload settings
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// factory.setSizeThreshold(THRESHOLD_SIZE);

		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setFileSizeMax(MAX_FILE_SIZE);
		upload.setSizeMax(MAX_REQUEST_SIZE);
		List<FileItem> uploadItems = null;
		HashMap<String, String> fieldValueMap = new HashMap<>();
		int imageCount = 1;
		String uuidValue = "";
		FileItem itemFile = null;
		String keyName = "";
		BasicAWSCredentials awsCredentials = new BasicAWSCredentials(AMAZON_ACCESS_KEY, AMAZON_SECRET_KEY);

		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion("ap-south-1")
				.withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).build();
		try {
			// parses the request's content to extract file data

			List formItems = upload.parseRequest(request);
			Iterator iter = formItems.iterator();

			// iterates over form's fields to get UUID Value
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();
				if (item.isFormField()) {
					String fieldName = item.getFieldName();
					if (!fieldName.equals("size") && !fieldName.equals("quantity")) {
						String value = item.getString().trim();
						fieldValueMap.put(fieldName, value);
					} else {
						if (fieldName.equals("size")) {
							if(!item.getString().trim().equals(""))
							size.add(item.getString().trim());
						} else {
							quantity.add(item.getString().trim());
						}
					}
				}
				// processes only fields that are not form fields
				if (!item.isFormField()) {
					String fieldName = item.getFieldName();
					String fileName = item.getName();
					if (fileName != null && !fileName.equals("")) {
						String contentType = item.getContentType();
						boolean isInMemory = item.isInMemory();
						long sizeInBytes = item.getSize();
						// write the logic to save the image
						keyName = "products/" + fieldValueMap.get("productCode") + "_" + fieldValueMap.get("colour")
								+ "_" + "0" + imageCount + ".PNG";
						++imageCount;
						itemFile = item;
					}
				}
				if (itemFile != null) {
					// get item inputstream to upload file into s3 aws

					// AmazonS3 s3client = new AmazonS3Client(awsCredentials);
					try {

						ObjectMetadata om = new ObjectMetadata();
						om.setContentLength(itemFile.getSize());
						String ext = FilenameUtils.getExtension(itemFile.getName());
						// String keyName = uuidValue + '.' + ext;

						s3Client.putObject(
								new PutObjectRequest(S3_BUCKET_NAME, keyName, itemFile.getInputStream(), om));
						s3Client.setObjectAcl(S3_BUCKET_NAME, keyName, CannedAccessControlList.PublicRead);

					} catch (AmazonServiceException ase) {
						ase.printStackTrace();

					} catch (AmazonClientException ace) {
						ace.printStackTrace();
					}

				} else {
					System.out.println(uuidValue + ":error:" + "No Upload file");
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		double costPrice = Double.parseDouble(fieldValueMap.get("costPrice"));
		double sellingPrice = Double.parseDouble(fieldValueMap.get("sellingPrice"));
		double tax = Double.parseDouble(fieldValueMap.get("tax"));
		// calculate the selling price including tax
		long sellingUpdated = CalculationUtil.calculateSelling(sellingPrice, tax);
		long discount = 0;
		if (costPrice > sellingUpdated)
			discount = CalculationUtil.calculateDiscount(costPrice, sellingUpdated);
		Product product = new Product();
		product.setName(fieldValueMap.get("productName"));
		product.setProductCode(fieldValueMap.get("productCode"));
		product.setColour(fieldValueMap.get("colour"));
		product.setProductShortDescription(fieldValueMap.get("productDescription"));
		product.setProductLongDescription(fieldValueMap.get("productLongDescription"));
		product.setProductAdditionalInformation(fieldValueMap.get("productLongDescription"));
		product.setCostPrice(costPrice);
		product.setSellingPrice(sellingUpdated);
		product.setDiscount((int)discount);
		product.setHSNNumber(fieldValueMap.get("hsn"));
		product.setTax(tax);
		// product.setBrand(filedValueMap.get("brand"));
		int subcategoryId = MenuUtil.getSubCategoryId(fieldValueMap.get("subCategory"));
		// insert brand into brand table and fetch the ID.
		String brand = fieldValueMap.get("brand").toLowerCase();
		// check whether brand exist or not
		int brandId = 0;
		String result = ProductDB.getBrandID(brand);
		if (result == null || result.equals("")) {
			brandId = ProductDB.insertIntoBrandTable(brand);
		} else {
			brandId = Integer.parseInt(result);
		}
		int productId = ProductDB.addProduct(product, subcategoryId, brandId);
		if (productId > 0) {

			Map<String, String> sizeQuantityMap = new HashMap();
			if (size.size() > 0) {

				for (int i = 0; i < size.size(); i++) {
					ProductDB.insertSizeQuantity(productId, Integer.parseInt(size.get(i)),
							Integer.parseInt(quantity.get(i)),subcategoryId);
				}
			} else {
				ProductDB.insertSizeQuantity(productId, -1, Integer.parseInt(quantity.get(0)),subcategoryId);
				request.setAttribute("added","Sucessfull");
				RequestDispatcher view = request.getRequestDispatcher("addProduct.jsp");
			}

		}
	}

}
