package com.ecommerce.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.data.MenuUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class MenuCategoryAjaxServlet
 */
@WebServlet("/admin/menuCategory")
public class MenuCategoryAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MenuCategoryAjaxServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		String topCategory = request.getParameter("topCategory");
		String category = request.getParameter("category");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		JsonObject myObj = new JsonObject();
		if (topCategory != null) {
			ArrayList<String> categoryMenu = MenuUtil.getCategory(topCategory);
			JsonElement categoryMenuObj = gson.toJsonTree(categoryMenu);
			myObj.add("categoryMenu", categoryMenuObj);

		} else {
			ArrayList<String> subCategoryMenu=MenuUtil.getSubcategory(category);
			JsonElement subCategoryMenuObj = gson.toJsonTree(subCategoryMenu);
			myObj.add("subCategoryMenu", subCategoryMenuObj);

			
		}

		// JsonObject myObj = new JsonObject();
		
		out.println(myObj.toString());
		out.close();

	}

}
