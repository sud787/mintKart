package com.ecommerce.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.Order;
import com.ecommerce.business.User;

/**
 * Servlet implementation class OrderDetailsServlet
 */
@WebServlet("/admin/viewOrderDetails")
public class OrderDetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		User admin = (User) session.getAttribute("admin");
		if (admin == null) {
			RequestDispatcher view = request.getRequestDispatcher("/admin/login.jsp");
			view.forward(request, response);
		} else {
			String orderId = (String) request.getParameter("orderId");
			Order order = new Order();
			order.setOrderId(Integer.parseInt(orderId));
			order.getOrderInfo();
			request.setAttribute("order", order);
			session.removeAttribute("orderIsplaced");
			RequestDispatcher view = request.getRequestDispatcher("/admin/OrderDetails.jsp");
			view.forward(request, response);
		}
	}

}
