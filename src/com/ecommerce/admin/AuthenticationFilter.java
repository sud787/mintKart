package com.ecommerce.admin;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.User;

public class AuthenticationFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("admin");
		if (user == null) {
			//response.sendRedirect("admin/login.jsp");
			request.getRequestDispatcher("login.jsp").forward(request, response);
		} else {
			//request.getRequestDispatcher("index.jsp").forward(request, response);
			chain.doFilter(req, res);
		}
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
