package com.ecommerce.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.User;
import com.ecommerce.data.UserDB;

/**
 * Servlet implementation class HandleAdminLogin
 */
@WebServlet("/admin/handleAdminLogin")
public class HandleAdminLogin extends HttpServlet {

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String loginId = request.getParameter("userId");
		String password = request.getParameter("password");
		User user = UserDB.authenticateAdmin(loginId, password);
		HttpSession session = request.getSession();
		if (user != null) {
			session.setAttribute("admin", user);
			// request.setAttribute("error", "Incorrcet credentials.Please try again");
			RequestDispatcher view = request.getRequestDispatcher("/admin/index.jsp");
			view.forward(request, response);
		} else {
			request.setAttribute("error", "Incorrcet credentials.Please try again");
			RequestDispatcher view = request.getRequestDispatcher("/admin/login.jsp");
			view.forward(request, response);
		}
	}

}
