package com.ecommerce.admin;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.data.MenuUtil;

/**
 * Servlet implementation class EditMenuHandlerAjaxServlet
 */
@WebServlet("/admin/editMenu")
public class EditMenuHandlerAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditMenuHandlerAjaxServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String topCategory = request.getParameter("topCategory");
		String category = request.getParameter("category");
		String subCategory = request.getParameter("subCategory");
		if (topCategory != null && !topCategory.equals("")) {
			// update top category.
			String updateTopCategoryName = request.getParameter("updatedTopCategory");
			int topCategoryID = MenuUtil.getTopCategoryId(topCategory);
			MenuUtil.updateTopCategory(updateTopCategoryName, topCategoryID);
		}else if(category!=null && !category.equals("")) {
			int categoryId=MenuUtil.getCategoryId(category);
			String updatedCategory=request.getParameter("updatedCategory");
			MenuUtil.updateCategory(updatedCategory, categoryId);
		}else {
			int subCategoryId=MenuUtil.getSubCategoryId(subCategory);
			String updatedSubCategory=request.getParameter("updatedSubCategory");
			MenuUtil.updateSubCategory(updatedSubCategory, subCategoryId);
			
			
		}
	}

}
