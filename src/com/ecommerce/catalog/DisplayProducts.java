package com.ecommerce.catalog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.business.Product;
import com.ecommerce.data.MenuUtil;
import com.ecommerce.data.ProductDB;

/**
 * Servlet implementation class DisplayProducts
 */
public class DisplayProducts extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int page = 1;
		int recordsPerPage = 12;
		if (request.getParameter("page") != null)
			page = Integer.parseInt(request.getParameter("page"));
		int startIndex = (page - 1) * recordsPerPage;
		String type = request.getParameter("type");
		// fetch the subcategoryId
		int subcategoryId = MenuUtil.getSubCategoryId(type);
		ArrayList<String> categoryList = MenuUtil.getCategories(type);
		// ArrayList<String> brandList = MenuUtil.getSubCategoryBrands(type);
		HashSet<String> brandList = ProductDB.getBrands(subcategoryId);
		HashSet<String> colourSet = ProductDB.getColours(subcategoryId);
		HashSet<String> sizeSet = ProductDB.getSizes(subcategoryId);
		int noOfRecords = ProductDB.getProductCount(subcategoryId);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		// fetch the sizes from sizepivot table
		// colourSet, brandList
		ArrayList<Product> productList = ProductDB.selectProducts(type, startIndex, recordsPerPage);
		// if(categoryList!=null)
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("productList", productList);
		// if(brandList!=null)
		request.setAttribute("brandList", brandList);
		request.setAttribute("colourSet", colourSet);
		request.setAttribute("sizeSet", sizeSet);
		request.setAttribute("noOfPages", noOfPages);
		request.setAttribute("currentPage", page);
		RequestDispatcher view = request.getRequestDispatcher("/products/displayProducts.jsp");
		view.forward(request, response);
		// write the model class to fetch the products,categories and size
		// column for filtering along with the products

	}

}
