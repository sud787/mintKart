package com.ecommerce.catalog;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.Cart;
import com.ecommerce.business.User;
import com.ecommerce.data.CartDB;
import com.ecommerce.data.CookieUtil;
import com.ecommerce.data.UserDB;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("Password");
		User user = UserDB.authenticateUser(loginId, password);
		if (user != null) {
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			String source = (String) session.getAttribute("source");
			// check if there is any existing cookie
			String cookieValue = null;
			Cookie[] cookie = request.getCookies();
			cookieValue = CookieUtil.getCookieValue(cookie, "id");
			if (cookieValue != null && !cookieValue.equals("")) {
				// means the user is a guest,create a session and insert this id
				// into logininfo table;
				// fetch the cart info using cookie value
				int guestId = CartDB.getCartId(cookieValue);
				// fetch only cartId
				if (guestId > 0) {
					int loggedInUsercartId = CartDB.getCartId(user.getSessionId());
					if (loggedInUsercartId > 0) {
						// update the guest cartdetails table,change the cart id
						// of guest to logged in user
						// if (CartDB.UpdateGuestToLoginCart(guestId,
						// loggedInUsercartId) != 0) {
						// delete the entries from all the tables
						// delete the entry from cart table and logininfo
						// table
						CartDB.updateGuestToLoginProductHandling(guestId, loggedInUsercartId);
						CartDB.removeCartEntry(guestId);
						UserDB.removeGuestEntry(cookieValue);

						// }
					} else {
						CartDB.insertIntocart(user.getSessionId());
						loggedInUsercartId = CartDB.getCartId(user.getSessionId());
						if (loggedInUsercartId > 0) {
							// update the guest cartdetails table,change the
							// cart id
							// of guest to logged in user
							CartDB.updateGuestToLoginProductHandling(guestId, loggedInUsercartId);
							// if (CartDB.UpdateGuestToLoginCart(guestId,
							// loggedInUsercartId) != 0) {
							CartDB.removeCartEntry(guestId);
							UserDB.removeGuestEntry(cookieValue);

						}
					}

				}
			}
			session.removeAttribute("cart");
			Cart cart = CartDB.getCart(user.getSessionId());
			cart = CartDB.getCart(user.getSessionId());
			cart.setCartId(CartDB.getCartId(user.getSessionId()));
			session.setAttribute("cart", cart);
			Cookie cookieId = new Cookie("id", user.getSessionId());
			cookieId.setMaxAge(60 * 60 * 24 * 365 * 2);
			cookieId.setPath("/");
			//cookieId.setDomain("mintkart.co.in");
			/*Cookie cookieIdSubDomain = new Cookie("id", user.getSessionId());
			cookieIdSubDomain.setMaxAge(60 * 60 * 24 * 365 * 2);
			cookieIdSubDomain.setPath("/");
			cookieIdSubDomain.setDomain("mintkart.in");
			response.addCookie(cookieIdSubDomain);*/
			response.addCookie(cookieId);
			String path = "";
			if (source != null) {
				path = "/cartSummary";
			} else {
				path = "/index.jsp";
			}
			RequestDispatcher view = request.getRequestDispatcher(path);
			view.forward(request, response);
		} else {
			request.setAttribute("error", "Incorrcet credentials.Please try again");
			RequestDispatcher view = request.getRequestDispatcher("/login.jsp");
			view.forward(request, response);
		}
	}

}
