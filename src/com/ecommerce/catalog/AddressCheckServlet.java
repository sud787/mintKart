package com.ecommerce.catalog;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.Address;
import com.ecommerce.business.Cart;
import com.ecommerce.business.User;
import com.ecommerce.data.CartDB;
import com.ecommerce.data.CookieUtil;

/**
 * Servlet implementation class AddressCheckServlet
 */
public class AddressCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// set the address in session variable
		HttpSession session = request.getSession();
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String state = request.getParameter("state");
		String address = request.getParameter("address");
		String number = request.getParameter("number");
		String city = request.getParameter("city");
		String pincode = request.getParameter("pincode");
		Address shippingAddress = new Address(name, email, number, city, pincode, state, address);
		session.setAttribute("shippingAddress", shippingAddress);
		User user = (User) session.getAttribute("user");
		String cookieValue = null;
		Cart cart = (Cart) session.getAttribute("cart");
		if (user == null) {
			Cookie[] cookie = request.getCookies();
			cookieValue = CookieUtil.getCookieValue(cookie, "id");
			if (cookieValue == null || cookieValue.equals("")) {

			} else {
				if (cart == null) {
					cart = CartDB.getCart(cookieValue);
					cart.setCartId(CartDB.getCartId(cookieValue));
					session.setAttribute("cart", cart);
				}
			}
		} else {
			if (cart == null) {
				cart = CartDB.getCart(user.getSessionId());
				cart = CartDB.getCart(user.getSessionId());
				cart.setCartId(CartDB.getCartId(user.getSessionId()));
				session.setAttribute("cart", cart);
			}
		}

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cart/payement.jsp");
		dispatcher.forward(request, response);
	}

}
