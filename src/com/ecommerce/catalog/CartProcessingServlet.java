package com.ecommerce.catalog;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.Cart;
import com.ecommerce.business.LineItem;
import com.ecommerce.business.Product;
import com.ecommerce.business.User;
import com.ecommerce.data.CartDB;
import com.ecommerce.data.CookieUtil;
import com.ecommerce.data.ProductDB;
import com.ecommerce.data.UserDB;

public class CartProcessingServlet extends HttpServlet {
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// only when user is logged in
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ")
				.append(request.getContextPath());
		String productId = request.getParameter("productId");
		String selectedSize = request.getParameter("selectedSize");
		if(selectedSize==null) {
			selectedSize="0";
		}
		String selectedQuantity = request.getParameter("selectedQuantity");
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		String noRefesh = (String) session.getAttribute("noRefesh");
		String cookieValue = null;
		if (user == null) {
			// check for guest user
			Cookie[] cookie = request.getCookies();
			cookieValue = CookieUtil.getCookieValue(cookie, "id");
			if (cookieValue == null || cookieValue.equals("")) {
				// means the user is a guest,create a session and insert this id
				// into logininfo table;
				String sessionId = session.getId();
				UserDB.insertGuestInfo(sessionId);
				Cookie cookieId = new Cookie("id", session.getId());
				cookieValue = session.getId();
				cookieId.setMaxAge(60 * 60 * 24 * 365 * 2);
				cookieId.setPath("/");
				response.addCookie(cookieId);
			}
			// guest user processing done
			Cart cart = (Cart) session.getAttribute("cart");
			cart = CartDB.getCart(cookieValue);
			cart.setCartId(CartDB.getCartId(cookieValue));
			cart.quantityCheck();
			// check whether cart id is set or not
			if (productId != null && !productId.equals("") && noRefesh != null) {
				Product product = ProductDB.selectProduct(productId);
				LineItem lineItem = new LineItem();
				lineItem.setProduct(product);
				lineItem.setQuantity(Integer.parseInt(selectedQuantity));
				lineItem.setSize(Integer.parseInt(selectedSize));
				if (cart.getCartId() > 0)
					cart.addItem(lineItem, cart.getCartId());
				else {
					cart.addItem(lineItem, cookieValue);
				}
				
			}
			session.setAttribute("cart", cart);
		} else {
			Cart cart = (Cart) session.getAttribute("cart");
			cart = CartDB.getCart(user.getSessionId());
			cart.setCartId(CartDB.getCartId(user.getSessionId()));
			cart.quantityCheck();

			if (productId != null && !productId.equals("") && noRefesh != null) {
				Product product = ProductDB.selectProduct(productId);
				LineItem lineItem = new LineItem();
				lineItem.setProduct(product);
				lineItem.setQuantity(Integer.parseInt(selectedQuantity));
				lineItem.setSize(Integer.parseInt(selectedSize));
				if (cart.getCartId() > 0)
					cart.addItem(lineItem, cart.getCartId());
				else {
					cart.addItem(lineItem, user.getSessionId());
				}

			}
			session.setAttribute("cart", cart);
		}
		// user.setSessionId(rs.getString("loginId"));
		session.removeAttribute("noRefesh");
		RequestDispatcher view = request
				.getRequestDispatcher("/displayCheckout.jsp");
		view.forward(request, response);
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

}
