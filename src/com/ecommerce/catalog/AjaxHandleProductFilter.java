package com.ecommerce.catalog;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.business.Product;
import com.ecommerce.data.MenuUtil;
import com.ecommerce.data.ProductDB;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class AjaxHandleProductFilter
 */

public class AjaxHandleProductFilter extends HttpServlet {
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int page = 1;
		int recordsPerPage = 12;
		if (request.getParameter("page") != null)
			page = Integer.parseInt(request.getParameter("page"));
		int startIndex = (page - 1) * recordsPerPage;
		String brandFilter = request.getParameter("brandFilter");
		String sizeFilter = request.getParameter("sizeFilter");
		String colorFilter = request.getParameter("colorFilter");
		List<String> brandIDList = null;
		List<String> colorList = null;
		List<String> sizeFilterProductIdsList = null;
		String subtype = request.getParameter("subtype");
		int subCategoryId = MenuUtil.getSubCategoryId(subtype);
		if (brandFilter != null && !brandFilter.equals("")) {
			List<String> brandList = Arrays.asList(brandFilter.split(","));
			String brandIds = ProductDB.getBrandIDS(brandList);
			brandIDList = Arrays.asList(brandIds.split(","));
		}
		if (colorFilter != null && !colorFilter.equals("")) {
			colorList = Arrays.asList(colorFilter.split(","));
		}
		if (sizeFilter != null && !sizeFilter.equals("")) {

			List<String> sizeList = Arrays.asList(sizeFilter.split(","));
			String sizeFilterProductIds = ProductDB
					.getProductIdsIDsForSizeFiltering(sizeList, subCategoryId);
			sizeFilterProductIdsList = Arrays.asList(sizeFilterProductIds
					.split(","));
		}
		// StringBuffer queryString=new StringBuffer();
		ArrayList<Product> productList = ProductDB.selectProductsFiltering(
				subCategoryId, brandIDList, colorList,
				sizeFilterProductIdsList, startIndex, recordsPerPage);
		int noOfRecords = ProductDB
				.selectProductsFilteringRowCount(subCategoryId, brandIDList,
						colorList, sizeFilterProductIdsList);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		response.setContentType("application/json");
		// OutputStream outputStream = response.getOutputStream();
		PrintWriter out = response.getWriter();
		// Gson gson = new Gson();
		Gson gson = new Gson();
		JsonObject myObj = new JsonObject();
		JsonElement countryObj = gson.toJsonTree(productList);
		myObj.addProperty("noOfPages", noOfPages);
		myObj.add("productInfo", countryObj);
		out.println(myObj.toString());

		out.close();
	}
}
