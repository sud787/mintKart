package com.ecommerce.catalog;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.Order;

/**
 * Servlet implementation class OrderDetailsServlet
 */
public class OrderDetailsServlet extends HttpServlet {
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		String orderId = (String) request.getParameter("orderId");

		if (orderId == null) {
			orderId = (String) session.getAttribute("orderId");
		}
		String isOrderConfirmMail = (String) session.getAttribute("orderIsplaced");
		if (isOrderConfirmMail != null && !isOrderConfirmMail.equals(" ")) {
			session.setAttribute("sendOrderMail", "true");
		}
		session.removeAttribute("orderIsplaced");
		/*
		 * if(isOrderConfirmMail!=null && !isOrderConfirmMail.equals("")){
		 * request.setAttribute("sendOrderConfirmationMail", "true"); }
		 */
		Order order = new Order();
		order.setOrderId(Integer.parseInt(orderId));
		order.getOrderInfo();
		request.setAttribute("order", order);
		session.removeAttribute("orderIsplaced");
		RequestDispatcher view = request.getRequestDispatcher("/Order/OrderDetails.jsp");
		view.forward(request, response);
		// fetch the order details on the basis of use id and dispaly the
		// contents
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
