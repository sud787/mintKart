package com.ecommerce.catalog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.Address;
import com.ecommerce.business.Cart;
import com.ecommerce.business.LineItem;
import com.ecommerce.business.User;
import com.ecommerce.data.AddressDB;
import com.ecommerce.data.CartDB;
import com.ecommerce.data.CookieUtil;
import com.ecommerce.data.OrderDB;
import com.ecommerce.data.PayuMoneyUtil;
import com.ecommerce.data.ProductDB;
import com.ecommerce.util.CalculationUtil;

/**
 * Servlet implementation class PaymnetFailureHandler
 */
@WebServlet("/paymnetFailureHandler")
public class PaymnetFailureHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PaymnetFailureHandler() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String url = (String) request.getAttribute("url");
		request.getRequestDispatcher(url).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String status = request.getParameter("status");
		String payuMoneyId = request.getParameter("payuMoneyId");
		// now generate the hash keys for the same
		HashMap<String, String> params = new HashMap();
		PayuMoneyUtil payuAccess = new PayuMoneyUtil();
		params.put("salt", payuAccess.getSalt());
		params.put("status", status);
		params.put("key", PayuMoneyUtil.getMerchantKey());
		params.put("txnid", request.getParameter("txnid"));
		params.put("amount", request.getParameter("amount"));
		params.put("productinfo", request.getParameter("productinfo"));
		params.put("firstname", request.getParameter("firstname"));
		params.put("email", request.getParameter("email"));
		String hashSequence = "salt|status||||||udf5|udf4|udf3|udf2|udf1|email|firstname|productinfo|amount|txnid|key";
		// String hashSequence =
		// "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		String[] hashVarSeq = hashSequence.split("\\|");
		String hashString = "";
		for (String part : hashVarSeq) {
			hashString = (empty(params.get(part))) ? hashString.concat("") : hashString.concat(params.get(part));
			hashString = hashString.concat("|");
		}
		hashString.trim();
		hashString = hashString.substring(0, hashString.lastIndexOf("|"));
		// int lastIndex=hashString.lastIndexOf("|");
		// String hashStringUpdated=hashStrin
		// hashString=hashString.concat(payuAccess.getSalt());
		String hashReceived = request.getParameter("hash");
		String hashGenrerated = payuAccess.hashCal("SHA-512", hashString);
		if (hashReceived.equals(hashGenrerated)) {
			// this status should be inserted into orderdetails and no mail and no deduction
			// from cart.
		//	System.out.println(status);
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("user");
			Address address = (Address) session.getAttribute("shippingAddress");
			String payementMethod = (String) session.getAttribute("payementMethod");
			// String isSendConfirmationMailSet = (String)
			// session.getAttribute("sendOrderConfirmationMail");
			Cart cart = (Cart) session.getAttribute("cart");
			Date orderdate = new Date();
			String orderStatus = status;
			String url = "orderDetailsServlet";
			if (user == null) {
				// first check the payemnt,if it's sucessful then insert the
				// address,move the cart item into order table and redirect the
				// user to orderPlaced page
				String cookieValue = null;
				// check for guest user
				Cookie[] cookie = request.getCookies();
				cookieValue = CookieUtil.getCookieValue(cookie, "id");
				if (cookieValue == null || cookieValue.equals("")) {
					// redirect to index page with error message
				} else {
					session.removeAttribute("sendOrderConfirmationMail");
					if (cart != null) {
						double productCost = cart.calculateCost();
						double shippingCharges = 15;
					//	double tax = 125.0;
						double totalCost = productCost  + shippingCharges;
						String invoiceId = CalculationUtil.genrateInvoiceId();
						int insertedid = OrderDB.insertIntoOrder(cookieValue, orderdate, orderStatus, payementMethod,
								0, shippingCharges, productCost, totalCost, invoiceId, payuMoneyId);
						if (insertedid > 0) {
							ArrayList<LineItem> items = cart.getItems();
							Iterator<LineItem> itr = items.iterator();
							while (itr.hasNext()) {
								LineItem item = itr.next();
								String productId = item.getProduct().getId();
								int sizeid = ProductDB.getSizeID(productId, item.getSize());
								OrderDB.addOrder(insertedid, productId, sizeid, item.getQuantity(),
										item.getProduct().getSellingPrice());
								// ProductDB.reduceQuantity(sizeid, item.getQuantity());
							}
							CartDB.clearCart(cookieValue);
							AddressDB.insertIntoAddress(cookieValue, address, insertedid);
							session.setAttribute("orderId", String.valueOf(insertedid));
						}
					}

				}
			} else {
				session.removeAttribute("sendOrderConfirmationMail");
				if (cart != null) {
					double productCost = cart.calculateCost();
					double shippingCharges = 15.0;
				//	double tax = 125.0;
					double totalCost = productCost  + shippingCharges;
					String invoiceId = CalculationUtil.genrateInvoiceId();
					int insertedid = OrderDB.insertIntoOrder(user.getSessionId(), orderdate, orderStatus,
							payementMethod, 0, shippingCharges, productCost, totalCost, invoiceId, payuMoneyId);
					if (insertedid > 0) {
						ArrayList<LineItem> items = cart.getItems();
						Iterator<LineItem> itr = items.iterator();
						while (itr.hasNext()) {
							LineItem item = itr.next();
							String productId = item.getProduct().getId();
							int sizeid = ProductDB.getSizeID(productId, item.getSize());
							// reduce the quantity
							OrderDB.addOrder(insertedid, productId, sizeid, item.getQuantity(),
									item.getProduct().getSellingPrice() * item.getQuantity());
							// ProductDB.reduceQuantity(sizeid, item.getQuantity());
						}
						// delete entries from cart
						CartDB.clearCart(user.getSessionId());
						AddressDB.insertIntoAddress(user.getSessionId(), address, insertedid);
						session.setAttribute("orderId", String.valueOf(insertedid));
					}
				}
			}
			request.setAttribute("url", url);
			doGet(request, response);
		}else {
			response.getWriter().println("Invalid Transaction");
		}
	}

	public boolean empty(String s) {
		if (s == null || s.trim().equals(""))
			return true;
		else
			return false;
	}

}
