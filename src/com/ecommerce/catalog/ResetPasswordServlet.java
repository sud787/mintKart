package com.ecommerce.catalog;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.data.UserDB;

/**
 * Servlet implementation class ResetPasswordServlet
 */
public class ResetPasswordServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String tokenId = request.getParameter("tokenId");
		// check the time intervel(qat max 30 minutes.)
		long timeInterval = 900000;
		Timestamp timestamp = new Timestamp(new Date().getTime() - timeInterval);
		if (UserDB.validateToken(timestamp, tokenId) != null) {
			request.setAttribute("token", tokenId);
			RequestDispatcher view = request
					.getRequestDispatcher("/resetPassword.jsp");
			view.forward(request, response);
		} else {
			response.getWriter().write("The Link has expired");

		}

	}

}
