package com.ecommerce.catalog;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.Address;
import com.ecommerce.data.OrderDB;
import com.ecommerce.util.MailUtil;
import com.ecommerce.util.TextMessageNotifcationSNS;

/**
 * Servlet implementation class HandleMailServlet
 */

public class HandleMailServlet extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// emailCostMap.put("totalCost", totalCost);
		// emailCostMap.put("emailAddress", emailAddress);
		String orderId = (String) request.getParameter("orderId");
		String mailType = (String) request.getParameter("mailType");
		HashMap<String, String> emailCostMap = OrderDB.getMailingDetails(orderId);
		TextMessageNotifcationSNS textSNS = new TextMessageNotifcationSNS();
	//	TextMessageNotifcationSNS.sendSMSMessage();
		if (mailType!=null && mailType.equals("orderCancelled")) {
			try {
				MailUtil.sendCancellationMailToAdmin(Integer.parseInt(orderId),
						Double.parseDouble(emailCostMap.get("totalCost")));
			} catch (MessagingException e) {
				String errorMessage = "ERROR: Unable to send email. " + "Check Tomcat logs for details.<br>"
						+ "NOTE: You may need to configure your system " + "as described in chapter 15.<br>"
						+ "ERROR MESSAGE: " + e.getMessage();
				request.setAttribute("errorMessage", errorMessage);
				this.log("Unable to send email. \n" + "Here is the email you tried to send: \n");
			}

		} else if(mailType!=null && mailType.equals("orderPlaced")) {
			// fetch the data from map and populate it to the send mail API
			if (orderId != null && !orderId.equals("")) {

				try {
					MailUtil.sendOrderMailToCustomer(emailCostMap.get("emailAddress"), Integer.parseInt(orderId),
							Double.parseDouble(emailCostMap.get("totalCost")));
					
				} catch (MessagingException e) {
					String errorMessage = "ERROR: Unable to send email. " + "Check Tomcat logs for details.<br>"
							+ "NOTE: You may need to configure your system " + "as described in chapter 15.<br>"
							+ "ERROR MESSAGE: " + e.getMessage();
					request.setAttribute("errorMessage", errorMessage);
					this.log("Unable to send email. \n" + "Here is the email you tried to send: \n");
				}
				try {
					MailUtil.sendOrderMailToAdmin( Integer.parseInt(orderId),
							Double.parseDouble(emailCostMap.get("totalCost")));
					
				} catch (MessagingException e) {
					String errorMessage = "ERROR: Unable to send email. " + "Check Tomcat logs for details.<br>"
							+ "NOTE: You may need to configure your system " + "as described in chapter 15.<br>"
							+ "ERROR MESSAGE: " + e.getMessage();
					request.setAttribute("errorMessage", errorMessage);
					this.log("Unable to send email. \n" + "Here is the email you tried to send: \n");
				}
			}

			// retrieve the orderId from the request and fetch the data from DB for
			// mailing address.

		}
	}

}
