package com.ecommerce.catalog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.business.Product;
import com.ecommerce.data.ProductDB;

/**
 * Servlet implementation class DisplayProducts
 */
public class DisplaySingleProducts extends HttpServlet {
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ")
				.append(request.getContextPath());
		String productId = request.getParameter("productId");
		String url = "";
		Product product = ProductDB.selectProduct(productId);
		if (product != null) {
			HashMap<Integer, Integer> sizeQuantityMap = ProductDB
					.selectSizeQuantity(productId);
			HashMap<String, String> availabeColours = ProductDB
					.getAvailableColours(product.getProductCode(),
							product.getId());
			if (availabeColours != null && availabeColours.size() > 0) {
				request.setAttribute("availabeColours", availabeColours);
			}
			ArrayList<String> productReview = ProductDB
					.getProducReviews(productId);
			if (productReview != null && productReview.size() > 0) {
				request.setAttribute("productReview", productReview);
			}
			request.setAttribute("product", product);
			request.setAttribute("sizes", sizeQuantityMap);
			url = "/displayProduct.jsp";

		} else {
			url = "/index.jsp";
		}
		RequestDispatcher view = request.getRequestDispatcher(url);
		view.forward(request, response);

	}
}
