package com.ecommerce.catalog;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.data.OrderDB;

/**
 * Servlet implementation class AjaxProductReviewSubmitServlet
 */

public class AjaxProductReviewSubmitServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String orderId = request.getParameter("orderId");
		String productId = request.getParameter("productId");
		String reviewMessage = request.getParameter("reviewMessage");
		// insert into productReview Table
		OrderDB.addProductReview(productId, orderId, reviewMessage);

	}

}
