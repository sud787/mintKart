package com.ecommerce.catalog;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.User;
import com.ecommerce.data.CartDB;
import com.ecommerce.data.CookieUtil;
import com.ecommerce.data.ProductDB;

/**
 * Servlet implementation class AjaxHandleServlet
 */
public class AjaxHandleServlet extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String productId = request.getParameter("productId");
		String quantity = request.getParameter("quantity");
		String size = request.getParameter("size");
		if(size.equals("-"))
			size="0";
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		// remove button
		if ((productId != null && !productId.equals("")) && quantity == null) {
			if (user == null) {
				Cookie[] cookie = request.getCookies();
				String cookieValue = CookieUtil.getCookieValue(cookie, "id");
				if (cookieValue != null && !cookieValue.equals("")) {
					int cartId = CartDB.getCartId(cookieValue);
					CartDB.removeFromCart(cartId, productId, ProductDB.getSizeID(productId, Integer.parseInt(size)));
					int cartItemCount = CartDB.isCartEmpty(cartId);
					if (cartItemCount == 0) {
						response.setContentType("text/plain");
						response.setCharacterEncoding("UTF-8");
						response.getWriter().write("empty");
					}
				}
			} else {
				int cartId = CartDB.getCartId(user.getSessionId());
				CartDB.removeFromCart(cartId, productId, ProductDB.getSizeID(productId, Integer.parseInt(size)));
				int cartItemCount = CartDB.isCartEmpty(cartId);
				if (cartItemCount == 0) {
					response.setContentType("text/plain");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write("empty");
				}
			}

		} else {
			int DBQuantity = CartDB.getQuantity(productId, Integer.parseInt(size));
			if (user == null) {
				Cookie[] cookie = request.getCookies();
				String cookieValue = CookieUtil.getCookieValue(cookie, "id");
				// check for the quantity of that product and if exceeds show
				// message
				if (DBQuantity >= Integer.parseInt(quantity)) {
					if (cookieValue != null && !cookieValue.equals("")) {
						CartDB.UpdateQuantityCart(quantity, CartDB.getCartId(cookieValue), productId,
								ProductDB.getSizeID(productId, Integer.parseInt(size)));
					}
				} else {
					if (cookieValue != null && !cookieValue.equals("")) {
						CartDB.UpdateQuantityCart(String.valueOf(DBQuantity), CartDB.getCartId(cookieValue), productId,
								ProductDB.getSizeID(productId, Integer.parseInt(size)));
					}
					String data = String.valueOf(DBQuantity);
					response.setContentType("text/plain");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write(data);
				}
			} else {
				if (DBQuantity >= Integer.parseInt(quantity)) {
					CartDB.UpdateQuantityCart(quantity, CartDB.getCartId(user.getSessionId()), productId,
							ProductDB.getSizeID(productId, Integer.parseInt(size)));
				} else {
					CartDB.UpdateQuantityCart(String.valueOf(DBQuantity), CartDB.getCartId(user.getSessionId()),
							productId, ProductDB.getSizeID(productId, Integer.parseInt(size)));
					String data = String.valueOf(DBQuantity);
					response.setContentType("text/plain");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write(data);
				}

			}

		}

	}
}
