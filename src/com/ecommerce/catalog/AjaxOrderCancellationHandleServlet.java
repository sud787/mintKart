package com.ecommerce.catalog;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.User;
import com.ecommerce.data.OrderDB;
import com.ecommerce.data.ProductDB;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class AjaxOrderCancellationHandleServlet
 */
public class AjaxOrderCancellationHandleServlet extends HttpServlet {
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String orderId = request.getParameter("orderId").trim();
		String productId = request.getParameter("productId").trim();
		String price = request.getParameter("price");
		String size = request.getParameter("size");
		String paymentMethod=OrderDB.getPaymentMethod(orderId);
		if(size.equals("-"))
			size="0";
		String quantity = request.getParameter("quantity");
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		// String data = String.valueOf(DBQuantity);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		//Gson gson = new Gson();
		JsonObject myObj = new JsonObject();
		// response.getWriter().write(data);
		if (user != null) {
			OrderDB.cancelOrder(orderId, productId,
					ProductDB.getSizeID(productId, Integer.parseInt(size)));
			HashMap<String, Double> costMap = OrderDB.getOrderCost(Integer
					.parseInt(orderId));
			Double productCost = costMap.get("productCost");
			Double totalCost = costMap.get("totalCost");
			Double updatedProductCost = productCost - Double.parseDouble(price);
			Double updatedTotalCost = totalCost - Double.parseDouble(price);
			if (updatedProductCost == 0) {
				OrderDB.cancelFullOrder(orderId);
			//	response.getWriter().write(String.valueOf(0));
				// update table with each cost entry to zero and set status as
				// cancelled
				if(paymentMethod.equals("payumoney")) {
					//raise the refund request:
					String paymentId=OrderDB.getPaymentId(orderId);
					//if the full order is cancelled then refund the total amount
					
				}
				
				myObj.addProperty("totalOrderCost", 0);
				myObj.addProperty("ORDERSTATUS", "Cancelled");
			} else {
				// update the price in order table
				OrderDB.updateOrderPrice(orderId, updatedTotalCost,
						updatedProductCost);
				if(paymentMethod.equals("payumoney")) {
					//raise the refund request:
				}
				
				myObj.addProperty("totalOrderCost", updatedTotalCost);
			}
			
			
			// send the mail to user and admin
			// increase the quantity again.
			ProductDB.updateQuantity(
					ProductDB.getSizeID(productId, Integer.parseInt(size)),
					Integer.parseInt(quantity));
			
			//get the payementMethod from orderId
			
			
			// update the price in order table
			out.println(myObj.toString());

			out.close();
		}

	}
}
