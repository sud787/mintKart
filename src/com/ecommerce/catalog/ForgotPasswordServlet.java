package com.ecommerce.catalog;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.data.UserDB;
import com.ecommerce.util.MailUtil;

/**
 * Servlet implementation class ForgotPasswordServlet
 */
public class ForgotPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String emailId = request.getParameter("email").trim();

		if (UserDB.userRegistered(emailId)) {
			// check in password_change_requests table
			Date date = new Date();
			String token = UUID.randomUUID().toString();
			if (UserDB.userAlreadyRequestedPassword(emailId)) {
				// update the time and UUId

				UserDB.updateTokenInfo(emailId, date, token);
			} else {
				UserDB.insertTokenInfo(emailId, date, token);

			}
			try {
				MailUtil.sendResetPasswordMail(emailId, token);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			request.setAttribute("error", "Reset password link sent to "
					+ emailId);
			RequestDispatcher view = request
					.getRequestDispatcher("/forgotPassword.jsp");
			view.forward(request, response);
			// redirect the user to register page with message that user already
			// registered,please login
		} else {
			request.setAttribute("error", "Email is not registered.");
			RequestDispatcher view = request
					.getRequestDispatcher("/forgotPassword.jsp");
			view.forward(request, response);
		}
	}

}
