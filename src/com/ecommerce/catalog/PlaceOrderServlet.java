package com.ecommerce.catalog;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.Address;
import com.ecommerce.business.Cart;
import com.ecommerce.business.LineItem;
import com.ecommerce.business.Product;
import com.ecommerce.business.User;
import com.ecommerce.data.AddressDB;
import com.ecommerce.data.CartDB;
import com.ecommerce.data.CookieUtil;
import com.ecommerce.data.OrderDB;
import com.ecommerce.data.ProductDB;
import com.ecommerce.data.UserDB;
import com.ecommerce.util.CalculationUtil;
import com.ecommerce.util.MailUtil;

public class PlaceOrderServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		Address address = (Address) session.getAttribute("shippingAddress");
		String payementMethod = (String) session.getAttribute("payementMethod");
		String isSendConfirmationMailSet = (String) session.getAttribute("sendOrderConfirmationMail");
		Cart cart = (Cart) session.getAttribute("cart");
		// add those elemnts which are not out of stock
		String sessionId = "";
		Date orderdate = new Date();
		String orderStatus = "Order Placed";
		String url = "orderDetailsServlet";

		if (user == null) {
			// first check the payemnt,if it's sucessful then insert the
			// address,move the cart item into order table and redirect the
			// user to orderPlaced page
			String cookieValue = null;
			// check for guest user
			Cookie[] cookie = request.getCookies();
			cookieValue = CookieUtil.getCookieValue(cookie, "id");
			if (cookieValue == null || cookieValue.equals("")) {
				// redirect to index page with error message
			} else {
				if (isSendConfirmationMailSet != null && !isSendConfirmationMailSet.equals("")) {
					session.removeAttribute("sendOrderConfirmationMail");
					if (cart.quantityCheck() == 0) {
						session.setAttribute("orderIsplaced", "yes");
						if (payementMethod.equalsIgnoreCase("CashOndelivery")) {
							if (cart != null) {
								double productCost = cart.calculateCost();
								double shippingCharges = 15.0;
								//double tax = 125.0;
								double totalCost = productCost  + shippingCharges;
								String invoiceId = CalculationUtil.genrateInvoiceId();
								int insertedid = OrderDB.insertIntoOrder(cookieValue, orderdate, orderStatus,
										payementMethod, 0, shippingCharges, productCost, totalCost,invoiceId);
								if (insertedid > 0) {
									ArrayList<LineItem> items = cart.getItems();
									Iterator<LineItem> itr = items.iterator();
									while (itr.hasNext()) {
										LineItem item = itr.next();
										String productId = item.getProduct().getId();
										int sizeid = ProductDB.getSizeID(productId, item.getSize());
										OrderDB.addOrder(insertedid, productId, sizeid, item.getQuantity(),
												item.getProduct().getSellingPrice());
										ProductDB.reduceQuantity(sizeid, item.getQuantity());
									}
									CartDB.clearCart(cookieValue);
									AddressDB.insertIntoAddress(cookieValue, address, insertedid);
									session.setAttribute("orderId", String.valueOf(insertedid));
								}
							}
						} else {

						}
					} else {
						url = "/cartSummary";
					}
				}
			}
		} else {
			if (isSendConfirmationMailSet != null && !isSendConfirmationMailSet.equals("")) {
				session.removeAttribute("sendOrderConfirmationMail");
				if (cart.quantityCheck() == 0) {
					session.setAttribute("orderIsplaced", "yes");
					if (payementMethod.equalsIgnoreCase("CashOndelivery")) {
						if (cart != null) {
							double productCost = cart.calculateCost();
							double shippingCharges = 15.0;
							//double tax = 125.0;
							double totalCost = productCost + shippingCharges;
							String invoiceId = CalculationUtil.genrateInvoiceId();
							int insertedid = OrderDB.insertIntoOrder(user.getSessionId(), orderdate, orderStatus,
									payementMethod, 0, shippingCharges, productCost, totalCost,invoiceId);
							if (insertedid > 0) {
								ArrayList<LineItem> items = cart.getItems();
								Iterator<LineItem> itr = items.iterator();
								while (itr.hasNext()) {
									LineItem item = itr.next();
									String productId = item.getProduct().getId();
									int sizeid = ProductDB.getSizeID(productId, item.getSize());
									// reduce the quantity
									OrderDB.addOrder(insertedid, productId, sizeid, item.getQuantity(),
											item.getProduct().getSellingPrice() * item.getQuantity());
									ProductDB.reduceQuantity(sizeid, item.getQuantity());
								}
								// delete entries from cart
								CartDB.clearCart(user.getSessionId());
								AddressDB.insertIntoAddress(user.getSessionId(), address, insertedid);
								session.setAttribute("orderId", String.valueOf(insertedid));
							}
						}
					}
				} else {
					url = "/cartSummary";
				}
			}
		}
		request.setAttribute("url", url);
		doGet(request, response);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// doGet(request,response);
		String url = (String) request.getAttribute("url");
		request.getRequestDispatcher(url).forward(request, response);

	}

}
