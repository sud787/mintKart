package com.ecommerce.catalog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.map.HashedMap;

import com.ecommerce.business.Address;
import com.ecommerce.business.Cart;
import com.ecommerce.business.LineItem;
import com.ecommerce.business.User;
import com.ecommerce.data.CartDB;
import com.ecommerce.data.CookieUtil;
import com.ecommerce.data.PayuMoneyUtil;

/**
 * Servlet implementation class PaymentInfoServlet
 */
public class FinalizeOrderServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Cart cart = (Cart) session.getAttribute("cart");
		String payementMethod = request.getParameter("payementMethod");

		session.setAttribute("payementMethod", payementMethod);
		String url = "";
		User user = (User) session.getAttribute("user");
		String cookieValue = null;
		if (user == null) {
			Cookie[] cookie = request.getCookies();
			cookieValue = CookieUtil.getCookieValue(cookie, "id");
			if (cookieValue == null || cookieValue.equals("")) {

			} else {
				if (cart == null) {
					cart = CartDB.getCart(cookieValue);
					cart.setCartId(CartDB.getCartId(cookieValue));
					session.setAttribute("cart", cart);
				}
			}
		} else {
			if (cart == null) {
				cart = CartDB.getCart(user.getSessionId());
				cart = CartDB.getCart(user.getSessionId());
				cart.setCartId(CartDB.getCartId(user.getSessionId()));
				session.setAttribute("cart", cart);
			}
		}
		// check for out of stock,if it is redirect it to cart page with error
		// message!!
		if (cart.quantityCheck() == 1) {
			url = "/cartSummary";

		} else {
			response.getWriter().append("Served at: ").append(request.getContextPath());
			url = "/cart/finalizeOrder.jsp";

		}
		// handling for online payement.
		if (payementMethod.equals("payumoney")) {

			PayuMoneyUtil payuAccess = new PayuMoneyUtil();
			Random rand = new Random();
			String rndm = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
			String txnid = payuAccess.hashCal("SHA-256", rndm).substring(0, 20);
			payuAccess.setTxnid(txnid);
			Address address = (Address) session.getAttribute("shippingAddress");
			String name = address.getName();
			String email = address.getEmail();
			String phone = address.getNumber();
			float totalCost =(float) cart.calculateCost();
			// shipping charges needs to be added here
			float shippingCharges = 15;
			totalCost = shippingCharges + totalCost;
			String productInfo = "online payment";
			String hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
			HashMap<String, String> params = new HashMap();
			params.put("key", PayuMoneyUtil.getMerchantKey());
			params.put("txnid", payuAccess.getTxnid());
			params.put("amount",String.valueOf(totalCost));
			params.put("productinfo", productInfo);
			params.put("firstname", name);
			params.put("email", email);
			String[] hashVarSeq = hashSequence.split("\\|");
			String hashString = "";
			for (String part : hashVarSeq) {
				hashString = (empty(params.get(part))) ? hashString.concat("") : hashString.concat(params.get(part));
				hashString = hashString.concat("|");
			}
			hashString=hashString.concat(payuAccess.getSalt());
			payuAccess.setHashString(payuAccess.hashCal("SHA-512", hashString));
			payuAccess.setAction1(payuAccess.getBase_url().concat("/_payment"));
			payuAccess.setAmount(totalCost);
			request.setAttribute("payuMoney", payuAccess);
			// "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		}

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

	public boolean empty(String s) {
		if (s == null || s.trim().equals(""))
			return true;
		else
			return false;
	}
}
