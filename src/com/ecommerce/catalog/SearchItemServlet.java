package com.ecommerce.catalog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.business.Product;
import com.ecommerce.data.ProductDB;
import com.ecommerce.data.QueryUtil;
import com.ecommerce.util.Combinations;

/**
 * Servlet implementation class SearchItemServlet
 */
public class SearchItemServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ")
				.append(request.getContextPath());
		String searchtext = request.getParameter("Search").trim().toLowerCase();
		String[] words = searchtext.split("\\s+");
		ArrayList<String> brandList = ProductDB.getAllBranads();
		ArrayList<String> brandFilter = new ArrayList<>();
		StringBuilder whereClause = new StringBuilder();
		whereClause.append("where (");
		StringBuilder brandCaluse = new StringBuilder();
		brandCaluse.append(" product.brandId IN (");
		// whereCaluse.append("where ");
		for (int i = 0; i < words.length; i++) {
			HashSet<String> set = Combinations.formWord(words[i]);
			// Combinations searchText = new Combinations(searchtext);
			// HashSet<String> set = searchText.combine();
			// now iterate from bardlist and findout if there is any match with
			// brands.
			for (int j = 0; j < brandList.size(); j++) {
				String brandName = brandList.get(j);
				if (set.contains(brandName.toLowerCase())) {
					brandFilter.add(brandName);
				}
			}
			// form the query with wilcards

			QueryUtil.generateWhereClauseForProduct(set, whereClause);
			System.out.println("-whereCaluse" + whereClause);
			QueryUtil.generateWhereClauseForBrand(brandFilter, brandCaluse);

		}
		whereClause.delete(whereClause.lastIndexOf("OR"),
				whereClause.lastIndexOf("OR") + 2);
		whereClause.append(")");
		if (brandFilter.size() > 0) {
			brandCaluse.deleteCharAt(brandCaluse.lastIndexOf(","));
			brandCaluse.append(")");
			whereClause.append(" AND ");
			whereClause.append(brandCaluse);
		}
		HashSet<String> colourSet = new HashSet<String>();
		HashSet<String> productBrandSet = new HashSet<String>();
		ArrayList<Product> productList = ProductDB.selectProductsForSearch(
				whereClause.toString(), colourSet, productBrandSet);
		request.setAttribute("productList", productList);
		request.setAttribute("brandList", productBrandSet);
		request.setAttribute("colourSet", colourSet);
		RequestDispatcher view = request
				.getRequestDispatcher("/displaySearchResult.jsp");
		view.forward(request, response);

	}
}
