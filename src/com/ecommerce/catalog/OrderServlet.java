package com.ecommerce.catalog;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.Order;
import com.ecommerce.business.User;
import com.ecommerce.data.CookieUtil;
import com.ecommerce.data.OrderDB;
import com.ecommerce.data.UserDB;

/**
 * Servlet implementation class OrderServlet
 */
public class OrderServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		if (user == null) {
			Cookie[] cookie = request.getCookies();
			String cookieValue = CookieUtil.getCookieValue(cookie, "id");
			if (cookieValue == null || cookieValue.equals("")) {
			} else {
				user = UserDB.selectUser(cookieValue);
				if (user == null) {

				} else {
					session.setAttribute("user", user);
				}
			}
		}
		ArrayList<Order> orderList = OrderDB.getOrderList(user.getSessionId());
		for (int i = 0; i < orderList.size(); i++) {
			orderList.get(i).setItems(OrderDB.getOrderItemDetails(orderList.get(i).getOrderId()));
		}
		request.setAttribute("orderList", orderList);
		request.getRequestDispatcher("Order/OrderTable.jsp").forward(request, response);
	}

}
