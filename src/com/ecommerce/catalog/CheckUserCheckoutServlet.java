package com.ecommerce.catalog;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.Cart;
import com.ecommerce.business.User;
import com.ecommerce.data.CartDB;
import com.ecommerce.data.CookieUtil;

/**
 * Servlet implementation class CheckUserCheckoutServlet
 */
public class CheckUserCheckoutServlet extends HttpServlet {
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		String cookieValue = null;
		String url = "/cart/checkoutLogin.jsp";
		Cart cart = (Cart) session.getAttribute("cart");
		if (user == null) {
			Cookie[] cookie = request.getCookies();
			cookieValue = CookieUtil.getCookieValue(cookie, "id");
			if (cookieValue == null || cookieValue.equals("")) {

			} else {
				if (cart == null) {
					cart = CartDB.getCart(cookieValue);
					cart.setCartId(CartDB.getCartId(cookieValue));
					session.setAttribute("cart", cart);
				}
			}
		} else {
			if (cart == null) {
				cart = CartDB.getCart(user.getSessionId());
				cart = CartDB.getCart(user.getSessionId());
				cart.setCartId(CartDB.getCartId(user.getSessionId()));
				session.setAttribute("cart", cart);
			}
			url = "/cart/myBilling.jsp";
		}

		int cartId = cart.getCartId();
		cart = CartDB.getCart(cart.getCartId());
		cart.setCartId(cartId);
		session.setAttribute("cart", cart);
		// check for session cart objects again
		// check only for quantity thing from db

		if (user != null) {
			url = "/cart/myBilling.jsp";
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

}
