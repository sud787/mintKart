package com.ecommerce.catalog;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ecommerce.business.Cart;
import com.ecommerce.business.User;
import com.ecommerce.data.CartDB;
import com.ecommerce.data.CookieUtil;
import com.ecommerce.data.UserDB;

/**
 * Servlet implementation class RegisterUserServlet
 */
@WebServlet("/RegisterUserServlet")
public class RegisterUserServlet extends HttpServlet {
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String source = (String) session.getAttribute("source");
		String firstName = request.getParameter("FirstName");
		String lastName = request.getParameter("LastName");
		String emailAddress = request.getParameter("Email");
		long mobileNumber = Long.valueOf(request.getParameter("PhoneNumber"));
		String password = request.getParameter("Password");
		// check whether user is already registered or not
		if (UserDB.userExists(emailAddress, mobileNumber)) {
			request.setAttribute("error", "User is already registered.Please login.");
			RequestDispatcher view = request.getRequestDispatcher("/register.jsp");
			view.forward(request, response);
			// redirect the user to register page with message that user already
			// registered,please login
		} else {
			User user = new User();
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmailAddress(emailAddress);
			user.setMobileNumber(mobileNumber);
			String sessionId = session.getId();
			user.setSessionId(sessionId);
			String cookieValue = null;
			Cookie[] cookie = request.getCookies();
			cookieValue = CookieUtil.getCookieValue(cookie, "id");
			if (sessionId.equals(cookieValue)) {
				// update the logininfo and insert into registerInfo
				// no cart updation required
				if (UserDB.updateLoginInfo(user, password, sessionId) > 0) {
					UserDB.insertUserRegisterInfo(user, sessionId);
					session.setAttribute("user", user);
					session.removeAttribute("cart");
					Cart cart = CartDB.getCart(user.getSessionId());
					cart = CartDB.getCart(user.getSessionId());
					cart.setCartId(CartDB.getCartId(user.getSessionId()));
					session.setAttribute("cart", cart);
					Cookie cookieId = new Cookie("id", session.getId());
					cookieId.setMaxAge(60 * 60 * 24 * 365 * 2);
					cookieId.setPath("/");
					response.addCookie(cookieId);
					String path = "";
					if (source != null) {
						path = "/cartSummary";
					} else {
						path = "/index.jsp";
					}
					RequestDispatcher view = request.getRequestDispatcher(path);
					view.forward(request, response);
				} else {
					request.setAttribute("error", "Please try sign up again.");
					RequestDispatcher view = request.getRequestDispatcher("/register.jsp");
					view.forward(request, response);
				}

			} else {
				// encryption of sessionid is yet to be done
				if (UserDB.insertLoginInfo(user, password, sessionId) > 0) {
					UserDB.insertUserRegisterInfo(user, sessionId);
					// check for guestUser
					if (cookieValue != null && !cookieValue.equals("")) {
						// update the logged in user db with guest cart
						int guestId = CartDB.getCartId(cookieValue);
						if (guestId > 0) {
							CartDB.insertIntocart(user.getSessionId());
							int loggedInUsercartId = CartDB.getCartId(user.getSessionId());
							if (loggedInUsercartId > 0) {
								// update the guest cartdetails table,change the
								// cart id
								// of guest to logged in user
								CartDB.updateGuestToLoginCart(guestId, loggedInUsercartId);
								// insert into cart and then updatte

							}

						}
					}

					session.setAttribute("user", user);
					session.removeAttribute("cart");
					Cart cart = CartDB.getCart(user.getSessionId());
					cart = CartDB.getCart(user.getSessionId());
					cart.setCartId(CartDB.getCartId(user.getSessionId()));
					session.setAttribute("cart", cart);
					Cookie cookieId = new Cookie("id", session.getId());
					cookieId.setMaxAge(60 * 60 * 24 * 365 * 2);
					cookieId.setPath("/");
					response.addCookie(cookieId);
					String path = "";
					if (source != null) {
						path = "/cartSummary";
					} else {
						path = "/index.jsp";
					}
					RequestDispatcher view = request.getRequestDispatcher(path);
					view.forward(request, response);
				} else {
					request.setAttribute("error", "Please try sign up again.");
					RequestDispatcher view = request.getRequestDispatcher("/register.jsp");
					view.forward(request, response);
				}
			}

		}
	}

}
