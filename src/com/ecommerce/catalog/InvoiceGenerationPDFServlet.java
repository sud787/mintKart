package com.ecommerce.catalog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.business.Order;
import com.ecommerce.data.OrderDB;
import com.ecommerce.util.PdfInvoices;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.util.*;

/**
 * Servlet implementation class InvoiceGenerationPDFServlet
 */
public class InvoiceGenerationPDFServlet extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String orderId = request.getParameter("orderId");
		Order order = new Order();
		order.setOrderId(Integer.parseInt(orderId));
		order.getOrderInfo();
		try {
			Image img = Image.getInstance("https://s3.ap-south-1.amazonaws.com/mintkart/Util/logo1.jpg");
			img.setAlignment(Element.ALIGN_LEFT);
			Document document = new Document();
			// step 2
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PdfWriter.getInstance(document, baos);
			// step 3
			document.open();
			// step 4
			PdfInvoices invoice = new PdfInvoices();
			invoice.createPdf(document, img,order);
			/*
			 * document.add(new Paragraph( String.format(
			 * "You have submitted the following text using the %s method:",
			 * request.getMethod()))); document.add(new Paragraph(text));
			 */
			// step 5
			document.close();

			// setting some response headers
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");
			// setting the content type
			response.setContentType("application/pdf");
			// the contentlength
			response.setContentLength(baos.size());
			// write ByteArrayOutputStream to the ServletOutputStream
			OutputStream os = response.getOutputStream();
			baos.writeTo(os);
			os.flush();
			os.close();
		} catch (DocumentException e) {
			throw new IOException(e.getMessage());
		}
	}

}
